-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Mar 23, 2017 alle 16:24
-- Versione del server: 10.1.21-MariaDB
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `al.jo.db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `collezione`
--

CREATE TABLE `collezione` (
  `Nome` varchar(20) NOT NULL,
  `Esclusiva` tinyint(1) DEFAULT '0',
  `Descrizione` text NOT NULL,
  `Immagini` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `collezione`
--

INSERT INTO `collezione` (`Nome`, `Esclusiva`, `Descrizione`, `Immagini`) VALUES
('Estate2017', 1, 'Collezione ispirata all\'estate. La collezione prende spunto dalle grandi mete turistiche come il mare, la montagna e anche le città.', 'img/collezioni/Estate2017.jpg'),
('Primavera2017', 0, 'Collezione ispirata ai profumati fiori di primavera. Gioielli dalla grande capacitÃ  espressiva che ricordano il ritorno delle belle giornate. ', 'img/collezioni/Primavera2017.jpg');

--
-- Trigger `collezione`
--
DELIMITER $$
CREATE TRIGGER `Collezione_Privata_Assegnamento_Gioielli` AFTER UPDATE ON `collezione` FOR EACH ROW BEGIN
DECLARE I varchar(20);
DECLARE U varchar(50);
if new.Esclusiva != old.Esclusiva
then 
if new.Esclusiva = 1
THEN
INSERT INTO esclusivagioiello(Gioiello,Utente)
SELECT gioielli.Id, esclusivacollezione.Utente
FROM gioielli, esclusivacollezione
WHERE
gioielli.Collezione = new.Nome and gioielli.Collezione=esclusivacollezione.Collezione AND esclusivacollezione.Collezione =new.Nome;
UPDATE gioielli
SET gioielli.Esclusiva = 1
where gioielli.Collezione = new.Nome And gioielli.Esclusiva = 0;
ELSE
DELETE FROM esclusivacollezione
WHERE esclusivacollezione.Collezione = new.Nome;
END IF;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `dimensione`
--

CREATE TABLE `dimensione` (
  `Codice` varchar(20) NOT NULL,
  `Tipologia` varchar(20) NOT NULL,
  `Grandezza` decimal(6,3) NOT NULL,
  `UnitaDiMisura` varchar(20) DEFAULT 'Centimetri'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `dimensione`
--

INSERT INTO `dimensione` (`Codice`, `Tipologia`, `Grandezza`, `UnitaDiMisura`) VALUES
('ANA', 'Anello', '36.250', 'Millimetri'),
('ANB', 'Anello', '38.640', 'Millimetri'),
('ANC', 'Anello', '39.900', 'Millimetri'),
('AND', 'Anello', '41.150', 'Millimetri'),
('ANE', 'Anello', '42.410', 'Millimetri'),
('ANF', 'Anello', '43.670', 'Millimetri'),
('ANG', 'Anello', '44.920', 'Millimetri'),
('BRSmallDonna', 'Bracciale', '17.000', 'Centimetri'),
('BRMedioDonna', 'Bracciale', '19.000', 'Centimetri'),
('BRUomo', 'Bracciale', '20.000', 'Centimetri'),
('CLDonna35', 'Collana', '35.000', 'Centimetri'),
('CLDonna40', 'Collana', '40.000', 'Centimetri'),
('CLDonna45', 'Collana', '45.000', 'Centimetri'),
('CLDonna50', 'Collana', '50.000', 'Centimetri'),
('CLDonna61', 'Collana', '61.000', 'Centimetri'),
('ORDonna1', 'Orecchino', '1.000', 'Centimetri'),
('ORDonna3', 'Orecchino', '3.000', 'Centimetri'),
('ORDonna5', 'Orecchino', '5.000', 'Centimetri'),
('Dm/Za/Ru2.5', 'Pietra', '2.500', 'Millimetri'),
('Dm/Za/Ru2.7', 'Pietra', '2.700', 'Millimetri'),
('Dm/Za/Ru3', 'Pietra', '3.000', 'Millimetri'),
('Dm/Za/Ru3.1', 'Pietra', '3.100', 'Millimetri'),
('Dm/Za/Ru3.4', 'Pietra', '3.400', 'Millimetri');

-- --------------------------------------------------------

--
-- Struttura della tabella `esclusivacollezione`
--

CREATE TABLE `esclusivacollezione` (
  `Collezione` varchar(20) NOT NULL,
  `Utente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `esclusivacollezione`
--

INSERT INTO `esclusivacollezione` (`Collezione`, `Utente`) VALUES
('Estate2017', 'company@company.com'),
('Estate2017', 'user@user.com');

--
-- Trigger `esclusivacollezione`
--
DELIMITER $$
CREATE TRIGGER `GestioneCollezioniInEsclusivaEGioielli` AFTER INSERT ON `esclusivacollezione` FOR EACH ROW BEGIN
INSERT INTO esclusivagioiello(Gioiello,Utente)
SELECT gioielli.Id, esclusivacollezione.Utente
FROM gioielli, esclusivacollezione 
WHERE
gioielli.Collezione = new.Collezione and gioielli.Collezione=esclusivacollezione.Collezione AND esclusivacollezione.Collezione =new.Collezione AND gioielli.Id NOT IN (SELECT esclusivagioiello.Gioiello FROM esclusivagioiello WHERE esclusivagioiello.Utente = esclusivacollezione.Utente);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `esclusivagioiello`
--

CREATE TABLE `esclusivagioiello` (
  `Gioiello` varchar(20) NOT NULL,
  `Utente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `esclusivagioiello`
--

INSERT INTO `esclusivagioiello` (`Gioiello`, `Utente`) VALUES
('AN3C', 'company@company.com'),
('AN3C', 'user@user.com'),
('ANDM', 'user@user.com'),
('BRSRP', 'company@company.com'),
('BRSRP', 'user@user.com'),
('OCPO', 'user@user.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `gioielli`
--

CREATE TABLE `gioielli` (
  `Id` varchar(20) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Peso` decimal(5,2) DEFAULT NULL,
  `Parour` varchar(20) DEFAULT NULL,
  `Descrizione` text,
  `Sesso` varchar(10) DEFAULT 'Unisex',
  `Esclusiva` tinyint(1) DEFAULT '0',
  `Collezione` varchar(20) DEFAULT NULL,
  `Immagini` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `gioielli`
--

INSERT INTO `gioielli` (`Id`, `Nome`, `Peso`, `Parour`, `Descrizione`, `Sesso`, `Esclusiva`, `Collezione`, `Immagini`) VALUES
('8888sdvsj', 'Anello Zaffiro', '10.50', NULL, 'Anelli con zaffiri naturali da Ceylon, Birmania, Brasile e Thailandia taglio ovale o ottagonale. Lo zaffiro &egrave; la pietra preziosa del momento che proponiamo a prezzi fortemente scontati rispetto alla gioielleria. Su richiesta anche carature, colori e tagli particolari.', 'Femminile', 0, NULL, 'img/gioielli/8888sdvsj.jpg'),
('abisvihv8895', 'Orecchini Avorio', '15.00', 'AvorioBianco', 'Orecchini realizzati con anelli di alluminio color canna di fucile, quarzo grigio e cerchio in avorio.', 'Femminile', 0, NULL, 'img/gioielli/abisvihv8895.jpg'),
('afafjasfjsaf', 'Cuore Zaffiro', '15.60', NULL, 'Collana con ciondolo forma di cuore in oro bianco zaffiro.', 'Femminile', 0, 'Primavera2017', 'img/gioielli/afafjasfjsaf.jpg'),
('Ajvsdbu9823', 'Anello Ametista', '5.00', 'StyleAmetista', 'L&rsquo;anello icona si espande. I suoi colori diventano ancora pi&ugrave; intensi e luminosi, nell&rsquo;immutata purezza del design.\r\n', 'Femminile', 0, NULL, 'img/gioielli/Ajvsdbu9823.jpg'),
('AN3C', '3Colori', '10.60', 'Discoteca', 'Anello in bronzo colorato con nuove tecniche all&#39;avanguardia nel campo della gioielleria. Questo permette di essere usato dove si vuole e quando si vuole.Ricordano molto i fiori', 'Femminile', 1, 'Estate2017', 'img/gioielli/AN3C.jpg'),
('ANDM', 'Diamond', '100.90', NULL, 'Anello di notevoli dimensioni con sopra incastonato un diamante anche esso di notevoli dimensioni', 'Unisex', 1, NULL, 'img/gioielli/ANDM.jpg'),
('BRSRP', 'Serpente', '76.65', 'Discoteca', 'Bracciale con ricami in argento che ricorda un piccolo serpente.', 'Unisex', 1, 'Estate2017', 'img/gioielli/BRSRP.jpg'),
('buvdbo8985', 'Bracciale Avorio D', '25.00', 'AvorioBianco', 'Bei braccialetti elasticizzati in rilievo con diaspro fossile opaco, oro placcato rondelles cristallo Swarovski &amp; placcato oro martellata anelli.Il loro colore crema avorio opaco &egrave; abbastanza neutro allo stack con altri braccialetti di divertimento e anche eleganti nella loro semplicit&agrave; impilati per conto proprio.', 'Unisex', 0, NULL, 'img/gioielli/buvdbo8985.jpg'),
('chsauhavbuo888', 'Bracciale Pugnale', '18.50', NULL, 'Bracciale rosario con 2 pugnali in argento che danno un tocco di mascolinit&agrave; al gioiello.', 'Maschile', 0, NULL, 'img/gioielli/chsauhavbuo888.jpg'),
('f515sdfds4', 'Cuore Argento', '20.00', NULL, 'Collana argento con pendente a forma di cuore piatto.', 'Femminile', 0, NULL, 'img/gioielli/f515sdfds4.jpg'),
('fafds451fa5', 'Collana Oro Rosa', '21.00', NULL, 'Collana bellissima che riempie di armonia e serenit&agrave; chi la guarda.', 'Unisex', 0, 'Primavera2017', 'img/gioielli/fafds451fa5.jpg'),
('javbpa856', 'Bracciale Avorio', '22.20', 'AvorioBianco', 'Questo gioiello &egrave; un pezzo unico, fatto a mano in Italia, con materiali di alta qualit&agrave;: \r\n- sfere di osso, in due diverse dimensioni\r\n- colore avorio\r\n- pesciolino in pietra dura (onice nero), realizzato su nostro disegno esclusivo', 'Unisex', 0, NULL, 'img/gioielli/javbpa856.jpg'),
('nhbddfb856', 'Anello Opale Nero', '5.00', NULL, 'Anello in oro 18k e grande cabochon di opale nero dai colori intensi che variano dal verde smeraldo al blu cobalto. ', 'Femminile', 0, NULL, 'img/gioielli/nhbddfb856.jpg'),
('OCPO', 'Palla Oro', '52.19', 'Discoteca', 'Orecchino di medie dimensioni adatto alla discoteca, perch&eacute; se lo perdi lo noti.', 'Femminile', 1, NULL, 'img/gioielli/OCPO.jpg'),
('sadsa8565', 'Orecchini Corniola', '12.00', NULL, 'Orecchini con elementi in bronzo placcato rutenio impreziositi con pietre in corniola rinforzata rossa ed elementi in bronzo.', 'Femminile', 0, NULL, 'img/gioielli/sadsa8565.jpg'),
('safddfs899', 'Anello triplo Zaffir', '12.50', NULL, 'Anelli con zaffiri naturali da Ceylon, Birmania, Brasile e Thailandia taglio ovale o ottagonale. Lo zaffiro &egrave; la pietra preziosa del momento che proponiamo a prezzi fortemente scontati rispetto alla gioielleria. Su richiesta anche carature, colori e tagli particolari.', 'Unisex', 0, NULL, 'img/gioielli/safddfs899.jpg'),
('sdsadsda99998', 'Orecchini Turchese', '5.50', 'Turchese', 'Gioiello di alta qualit&agrave;, fornito con un certificato di autenticit&agrave; e pietre naturali selezionate con la massima cura, attestato francese.', 'Femminile', 0, NULL, 'img/gioielli/sdsadsda99998.jpg'),
('sgfdngpbf895', 'Collana Avorio', '30.50', 'AvorioBianco', 'Avorio e osso sfaccettato Beaded Bracciale Stretch con strass Swarovski cristallo opale Crescent Moon fascino\r\nPerfetto per l&amp;#39;impilamento!', 'Unisex', 0, NULL, 'img/gioielli/sgfdngpbf895.jpg');

-- --------------------------------------------------------

--
-- Struttura della tabella `materiale`
--

CREATE TABLE `materiale` (
  `Nome` varchar(30) NOT NULL,
  `Colore` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `materiale`
--

INSERT INTO `materiale` (`Nome`, `Colore`) VALUES
('Acciaio', 'Grigio'),
('Argento', 'Argento'),
('Argento', 'Oro'),
('Avorio', 'Bianco'),
('Bronzo', 'Bronzo'),
('Bronzo', 'Oro'),
('Bronzo', 'Rosa'),
('Oro', 'Bianco'),
('Oro', 'Oro'),
('Oro', 'Rosa'),
('Pelle', 'Marrone');

-- --------------------------------------------------------

--
-- Struttura della tabella `ornamento`
--

CREATE TABLE `ornamento` (
  `Gioiello` varchar(20) NOT NULL,
  `Nome_Pietra` varchar(30) NOT NULL,
  `Colore_Pietra` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `ornamento`
--

INSERT INTO `ornamento` (`Gioiello`, `Nome_Pietra`, `Colore_Pietra`) VALUES
('8888sdvsj', 'Zaffiro', 'Blu'),
('abisvihv8895', 'Quarzo', 'Grigio'),
('afafjasfjsaf', 'Zaffiro', 'Blu'),
('Ajvsdbu9823', 'Ametista', 'Viola'),
('AN3C', 'Acquamarina', 'Azzurro'),
('ANDM', 'Alessandrite', 'Rossa'),
('BRSRP', 'Acquamarina', 'Azzurro'),
('buvdbo8985', 'Diamante', 'Bianco'),
('chsauhavbuo888', 'Onice', 'Nero'),
('javbpa856', 'Onice', 'Nero'),
('nhbddfb856', 'Opale', 'Nero'),
('OCPO', 'Alessandrite', 'Rossa'),
('sadsa8565', 'Corniola', 'RossoArancio'),
('safddfs899', 'Zaffiro', 'Blu'),
('sdsadsda99998', 'Turchese', 'Azzurro'),
('sgfdngpbf895', 'Opale', 'Nero');

-- --------------------------------------------------------

--
-- Struttura della tabella `pietra`
--

CREATE TABLE `pietra` (
  `Nome` varchar(30) NOT NULL,
  `Dimensione` varchar(20) NOT NULL,
  `Colore` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pietra`
--

INSERT INTO `pietra` (`Nome`, `Dimensione`, `Colore`) VALUES
('Acquamarina', 'Dm/Za/Ru3.1', 'Azzurro'),
('Alessandrite', 'Dm/Za/Ru3', 'Rossa'),
('Ambra', 'Dm/Za/Ru3.1', 'Giallo'),
('Ametista', 'Dm/Za/Ru3.1', 'Viola'),
('Corniola', 'Dm/Za/Ru2.5', 'RossoArancio'),
('Diamante', 'Dm/Za/Ru2.7', 'Bianco'),
('Onice', 'Dm/Za/Ru2.5', 'Nero'),
('Opale', 'Dm/Za/Ru3.4', 'Nero'),
('Opale', 'Dm/Za/Ru3.4', 'Rosso'),
('Opale', 'Dm/Za/Ru3.4', 'Verde'),
('Quarzo', 'Dm/Za/Ru2.5', 'Grigio'),
('Rubino', 'Dm/Za/Ru3', 'Rosso'),
('Turchese', 'Dm/Za/Ru2.7', 'Azzurro'),
('Zaffiro', 'Dm/Za/Ru3', 'Blu');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `Gioiello` varchar(20) NOT NULL,
  `Nome_Materiale` varchar(30) NOT NULL,
  `Colore_Materiale` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`Gioiello`, `Nome_Materiale`, `Colore_Materiale`) VALUES
('8888sdvsj', 'Argento', 'Argento'),
('abisvihv8895', 'Acciaio', 'Grigio'),
('afafjasfjsaf', 'Oro', 'Bianco'),
('Ajvsdbu9823', 'Oro', 'Bianco'),
('AN3C', 'Argento', 'Argento'),
('ANDM', 'Oro', 'Bianco'),
('BRSRP', 'Oro', 'Oro'),
('buvdbo8985', 'Avorio', 'Bianco'),
('chsauhavbuo888', 'Argento', 'Argento'),
('f515sdfds4', 'Argento', 'Argento'),
('fafds451fa5', 'Oro', 'Rosa'),
('javbpa856', 'Avorio', 'Bianco'),
('nhbddfb856', 'Acciaio', 'Grigio'),
('OCPO', 'Oro', 'Rosa'),
('sadsa8565', 'Bronzo', 'Oro'),
('safddfs899', 'Argento', 'Argento'),
('sdsadsda99998', 'Argento', 'Argento'),
('sgfdngpbf895', 'Avorio', 'Bianco');

-- --------------------------------------------------------

--
-- Struttura della tabella `taglia`
--

CREATE TABLE `taglia` (
  `Gioiello` varchar(20) NOT NULL,
  `Dimensione` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taglia`
--

INSERT INTO `taglia` (`Gioiello`, `Dimensione`) VALUES
('8888sdvsj', 'ANC'),
('abisvihv8895', 'ORDonna3'),
('afafjasfjsaf', 'CLDonna35'),
('Ajvsdbu9823', 'ANF'),
('AN3C', 'ANC'),
('ANDM', 'ANE'),
('BRSRP', 'BRUomo'),
('buvdbo8985', 'BRUomo'),
('chsauhavbuo888', 'BRSmallDonna'),
('f515sdfds4', 'CLDonna40'),
('fafds451fa5', 'CLDonna40'),
('javbpa856', 'BRMedioDonna'),
('nhbddfb856', 'ANA'),
('OCPO', 'ORDonna1'),
('sadsa8565', 'ORDonna1'),
('safddfs899', 'ANG'),
('sdsadsda99998', 'ORDonna1'),
('sgfdngpbf895', 'CLDonna35');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `Email` varchar(50) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`Email`, `Password`, `Nome`, `Admin`) VALUES
('admin@admin.com', 'Admin1234', 'Admin', 1),
('company@company.com', 'Company1234', 'Company', 0),
('user@user.com', 'User1234', 'User', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `collezione`
--
ALTER TABLE `collezione`
  ADD PRIMARY KEY (`Nome`);

--
-- Indici per le tabelle `dimensione`
--
ALTER TABLE `dimensione`
  ADD PRIMARY KEY (`Codice`),
  ADD UNIQUE KEY `Tipologia` (`Tipologia`,`Grandezza`,`UnitaDiMisura`);

--
-- Indici per le tabelle `esclusivacollezione`
--
ALTER TABLE `esclusivacollezione`
  ADD PRIMARY KEY (`Collezione`,`Utente`),
  ADD KEY `Utente` (`Utente`);

--
-- Indici per le tabelle `esclusivagioiello`
--
ALTER TABLE `esclusivagioiello`
  ADD PRIMARY KEY (`Gioiello`,`Utente`),
  ADD KEY `Utente` (`Utente`);

--
-- Indici per le tabelle `gioielli`
--
ALTER TABLE `gioielli`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Collezione` (`Collezione`);

--
-- Indici per le tabelle `materiale`
--
ALTER TABLE `materiale`
  ADD PRIMARY KEY (`Nome`,`Colore`);

--
-- Indici per le tabelle `ornamento`
--
ALTER TABLE `ornamento`
  ADD PRIMARY KEY (`Gioiello`,`Nome_Pietra`,`Colore_Pietra`),
  ADD KEY `Nome_Pietra` (`Nome_Pietra`,`Colore_Pietra`);

--
-- Indici per le tabelle `pietra`
--
ALTER TABLE `pietra`
  ADD PRIMARY KEY (`Nome`,`Colore`,`Dimensione`),
  ADD KEY `Dimensione` (`Dimensione`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`Gioiello`,`Nome_Materiale`,`Colore_Materiale`),
  ADD KEY `Nome_Materiale` (`Nome_Materiale`,`Colore_Materiale`);

--
-- Indici per le tabelle `taglia`
--
ALTER TABLE `taglia`
  ADD PRIMARY KEY (`Gioiello`,`Dimensione`),
  ADD KEY `Dimensione` (`Dimensione`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`Email`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `esclusivacollezione`
--
ALTER TABLE `esclusivacollezione`
  ADD CONSTRAINT `EsclusivaCollezione_ibfk_1` FOREIGN KEY (`Collezione`) REFERENCES `collezione` (`Nome`) ON DELETE CASCADE,
  ADD CONSTRAINT `EsclusivaCollezione_ibfk_2` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`) ON DELETE CASCADE;

--
-- Limiti per la tabella `esclusivagioiello`
--
ALTER TABLE `esclusivagioiello`
  ADD CONSTRAINT `EsclusivaGioiello_ibfk_1` FOREIGN KEY (`Gioiello`) REFERENCES `gioielli` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EsclusivaGioiello_ibfk_2` FOREIGN KEY (`Utente`) REFERENCES `utente` (`Email`) ON DELETE CASCADE;

--
-- Limiti per la tabella `gioielli`
--
ALTER TABLE `gioielli`
  ADD CONSTRAINT `Gioielli_ibfk_1` FOREIGN KEY (`Collezione`) REFERENCES `collezione` (`Nome`) ON DELETE SET NULL;

--
-- Limiti per la tabella `ornamento`
--
ALTER TABLE `ornamento`
  ADD CONSTRAINT `Ornamento_ibfk_1` FOREIGN KEY (`Gioiello`) REFERENCES `gioielli` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `Ornamento_ibfk_2` FOREIGN KEY (`Nome_Pietra`,`Colore_Pietra`) REFERENCES `pietra` (`Nome`, `Colore`) ON DELETE CASCADE;

--
-- Limiti per la tabella `pietra`
--
ALTER TABLE `pietra`
  ADD CONSTRAINT `Pietra_ibfk_1` FOREIGN KEY (`Dimensione`) REFERENCES `dimensione` (`Codice`) ON DELETE CASCADE;

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `Prodotto_ibfk_1` FOREIGN KEY (`Gioiello`) REFERENCES `gioielli` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `Prodotto_ibfk_2` FOREIGN KEY (`Nome_Materiale`,`Colore_Materiale`) REFERENCES `materiale` (`Nome`, `Colore`) ON DELETE CASCADE;

--
-- Limiti per la tabella `taglia`
--
ALTER TABLE `taglia`
  ADD CONSTRAINT `Taglia_ibfk_1` FOREIGN KEY (`Dimensione`) REFERENCES `dimensione` (`Codice`) ON DELETE CASCADE,
  ADD CONSTRAINT `Taglia_ibfk_2` FOREIGN KEY (`Gioiello`) REFERENCES `gioielli` (`Id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
