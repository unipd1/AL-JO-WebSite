<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/UpdateInterrogation.php');

    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="lista_gioielli.php">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;
    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    $sessioneAperta = true;

    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true){

        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;
        <a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a>&gt; Aggiornamento&nbsp;Esclusiva</p>';
    }
    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
    try{

    if($sessioneAperta== true){

        if(!empty($_POST['utentiEs'])&&!empty($_POST['id'])){
        $utente = $_POST['utentiEs'];
        $evento = $_POST['id'];
        $s1 = array("COUNT(*) as CONTEGGIO");
        $s2 = "";
        $f1 = "";
        $w1 = "";
        $t1 = "";
        $boolC = false;
        if($_POST['id']=='eliminaGioielloEsclusiva'){
            $s2 = array("gioielli.Nome","gioielli.Id","gioielli.Immagini","gioielli.Descrizione");
            $f1 = array("esclusivagioiello", "gioielli");
            $w1 = array("esclusivagioiello.Utente ='".$utente."'",
         "AND","esclusivagioiello.Gioiello =  gioielli.Id");
            $t1 = "Elimina Gioiello";
            $boolC = true;
        }
        if($_POST['id']=='eliminaCollezioneEsclusiva'){
            $s2 = array("esclusivacollezione.Collezione as Nome");
            $f1 = array("esclusivacollezione");
            $w1 = array("esclusivacollezione.Utente ='".$utente."'");
            $t1 = "Elimina Collezione";
        }
        if($_POST['id']=='aggiungiGioielloEsclusiva'){
            $s2 = array("Nome","Id","Immagini","Descrizione");
            $f1 = array("gioielli");
            $w1 = array("gioielli.Esclusiva = 1","AND","gioielli.Id not in (
                SELECT esclusivagioiello.Gioiello FROM esclusivagioiello
                WHERE esclusivagioiello.Utente ='".$utente."')");
            $t1 = "Aggiungi Gioiello";
            $boolC = true;
        }
        if($_POST['id']=='aggiungiCollezioneEsclusiva'){
            $s2 = array("collezione.Nome");
            $f1 = array("collezione");
            $w1 = array("collezione.Esclusiva = 1","AND","collezione.Nome not in (
                SELECT esclusivacollezione.Collezione FROM esclusivacollezione
                 WHERE esclusivacollezione.Utente = '".$utente."')");
            $t1 = "Aggiungi Collezione";
        }

        $mInterrogation = new SelectInterrogation($s1,$f1,$w1);
        try {
        $arrayInterrogationm = $mInterrogation->interrogation();
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }

        $risultato = 0;
        if($arrayInterrogationm->num_rows > 0){
            while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                $risultato+=$row['CONTEGGIO'];
            }
        }
        $arrayInterrogationm->free();

        $limite1 = 0;
        $limite2 = 30;
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            if(!empty($_GET["numero"])){
                    $limite1 = ($_GET["numero"]-1)*30;

            }
        }
        array_push($w1,"LIMIT ".$limite1.",".$limite2);
        $mInterrogation = new SelectInterrogation($s2,$f1,$w1);
        try {
        $arrayInterrogationm = $mInterrogation->interrogation();
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
        if($arrayInterrogationm->num_rows > 0){

            echo "<div id='esecuzioneModificaEsclusive'>
            <a href='#divNumeriFondoPagina' class='salto'>Salta la lista di selezione e vai ai numeri di cambio pagina</a>
            <a href='#menu' class='salto'>Ritorna al menu</a>
            <a href='#menuSD' class='salto'>Ritorna al menu di sezione dedicata</a>
            <form action='esecuzioneModificaEsclusive.php' method='post'>


           <ul id ='listaElementi'>
            ";
              while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
               if($boolC==true){
               echo "
               <li class='elementoGioielli'>
              <h2><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h2>
              <a href=\"gioiello.php?id=".$row['Id']."\" class='linkDisabilitato'><img src=\"".$row['Immagini']."\" alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/></a>
              <div>
              <input type='checkbox' name='elem[]' value='".$row['Id']."'/>Seleziona ".$row['Id']."
              </div>

              </li>";
              }
              else {
                  echo "<li class='elementoCollezione'>

                  <h2><a href=\"collezione.php?name=".$row['Nome']."\">".$row['Nome']."</a></h2>

                  <input type='checkbox' name='elem[]' value='".$row['Nome']."'/>Seleziona ".$row['Nome']."

                  </li>";
              }
          }
          echo "</ul>
          <div id='eliminaDiv'>
          <label for='eliminaLabelButton' id='eliminaLabel'>
          <button type='submit' name='id' id='eliminaLabelButton' value='".$evento."&amp;".$utente."'>".$t1."</button>
          </label>
          </div>
          </form></div>";
        }

        $arrayInterrogationm->free();

        echo "<div id='divNumeriFondoPagina'>

        <a href='#footer' class='salto'>Salta i numeri di cambio pagina e vai al <span xml:lang='en'>footer</span></a>
        <a href='#menu' class='salto'>Ritorna al menu</a>
        <a href='#menuSD' class='salto'>Ritorna al menu di sezione dedicata</a>
        <ul id='numeriFondoPagina'>";
        if(intval($risultato/30) == 0) $ri = 1;
        else $ri=$risultato/30;
        for ($i=0; $i < intval($ri); $i++) {
          if(!isset($_GET["numero"])){
            if($i+1==1){
            echo"<li class='elementoNumeroGioiello active'>
            <span class='active'>".($i+1)."</span>
            </li>";
            }
            else{
              echo"<li class='elementoNumeroGioiello'>
                <a href=\"aggiornaEsclusive.php?numero=".($i+1)."\">".($i+1)."</a>
                </li>";
            }

          }
          else{
            if($_GET["numero"]==($i+1)){

              echo"<li class='elementoNumeroGioiello active'>
              <span class='active'>".($i+1)."</span>
              </li>";
            }
            else{
              echo"<li class='elementoNumeroGioiello'>
                <a href=\"aggiornaEsclusive.php?numero=".($i+1)."\">".($i+1)."</a>
                </li>";
            }
          }


        }
        echo"</ul></div>";
    }
    else {
        echo "<p>Errore, bisogna selezionare almeno un utente per poter avanzare.</p>";
    }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
    require_once("Parti/footer.php");
    echo printfooter($sessioneAperta);
?>
