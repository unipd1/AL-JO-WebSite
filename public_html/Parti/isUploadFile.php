<?php
function isUploadFile($uploaddir,$nomeImm){
// per prima cosa verifico che il file sia stato effettivamente caricato
if (!isset($_FILES[$nomeImm]) || !is_uploaded_file($_FILES[$nomeImm]['tmp_name'])){
    return 'IMGNOTINSERT';
}

//Recupero il percorso temporaneo del file
$userfile_tmp = $_FILES[$nomeImm]['tmp_name'];

//recupero il nome originale del file caricato
$userfile_name = $_FILES[$nomeImm]['name'];

$ext_ok = array('png','jpg','jpeg');
$temp = explode('.', $_FILES[$nomeImm]['name']);
$ext = end($temp);
if (!in_array($ext, $ext_ok)) {
  return 'Il file ha un estensione non ammessa!';
}
$target_file = $uploaddir.".".$ext;

if (file_exists($target_file)) {
  return 'Id già presente';
}

$is_img = getimagesize($_FILES[$nomeImm]['tmp_name']);
if (!$is_img) {
  return 'Puoi inviare solo immagini';
}

//copio il file dalla sua posizione temporanea alla mia cartella upload
if (move_uploaded_file($userfile_tmp, $target_file)) {
  //Se l'operazione è andata a buon fine...
  return $ext;
}else{
  //Se l'operazione è fallta...
  return 'Upload NON valido!';
}
}
?>
