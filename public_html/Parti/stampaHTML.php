<?php
function printHeadHTML($mTitle,$mDescription,$mKeywords,$mRobots){
  return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

  <head>
      <meta http-equiv="Content-type" content="application/xhtml+xml; charset=utf-8" />
      <title>'.$mTitle.'</title>
      <meta name="title" content="'.$mTitle.'" />
      <meta name="description" content="'.$mDescription.'" />
      <meta name="keywords" content="'.$mKeywords.'" />
      <meta name="robots" content="'.$mRobots.'" />
      <meta name="author" content="Marco Berselli, Sebastiano Zamberlan" />
      <link rel="shortcut icon" href="img/shortcutIcon.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link href="css/stile.css" rel="stylesheet" type="text/css" media="screen" />
      <link href="css/stile_print.css" rel="stylesheet" type="text/css" media="print" />
      <script type="text/javascript" src="javascript/index.js"></script>
  </head>';
}
function printHTML($menu,$sessioneAperta,$sezioneLogin,$sezioneRicerca,$breadCrumb){
  $parte1 = '<body>
      <div id="areaFissata">
          <div id="rigaUnica">
              <div id="header">
                  <a href="index.php">
                      <img src="img/logo.png" alt="AL.JO. Gioielli Center" id="logo" width="100%" height="100%" />
                  </a>
                  <h1>AL.JO. Gioielli<span xml:lang="en"> Center </span></h1>
              </div>
              <div id="menu">

                  <ul>
                      <li><a href="#saltaLogin" class="salto">Salta il menu e vai all\'area di login</a></li>
                      <li><a href="#body" class="salto">Salta il menu e vai al corpo della pagina</a></li>
                      '.$menu.'
                  </ul>
              </div>
              <a href="#breadcrumb" id="saltaLogin" class="salto">Salta la form di <span xml:lang="en">login</span> e vai al <span xml:lang="en">breadcrumb</span></a>
              <a href=\'#body\' class=\'salto\'>Salta la form di <span xml:lang="en">login</span> e vai al corpo della pagina</a>
              <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
                      ';
                    if($sessioneAperta==true)
                    {
                      $parte2 = '
                      <div id="areaLoginRegistrazione">
                      <div id ="logout">

                      <form action="logout.php" id="ancoraLogout">
                          <fieldset><button id="bottoneLogin">Logout</button></fieldset>
                      </form>
                      </div>


                    </div>
                    </div>';
                  }
                    else {
                      $parte2 = '
                      <div id="areaLoginRegistrazione">
                          <div id="login">
                              <form action="login.html" id="ancoraLogin">
                                  <fieldset><button id="bottoneLogin">Login</button></fieldset>
                              </form>
                              <div id="contenitoreLogin">
                          <form action="login.php" method="post" id="formLogin">
                              <fieldset>
                                <legend>Accedi o Registrati</legend>
                                  <span title="chiusura login" id="chiusuraLogin">&times;</span>

                                  <div>
                                      <label for="emailL">E-mail:
                                      <input type="text" name="emailL" id="emailL" class="defaultInputForm" />
                                      </label>
                                      <span></span>
                                  </div>
                                  <div>
                                      <label for="password">Password:
                                      <input type="password" name="password" id="password" maxlength="20" class="defaultInputForm" />
                                      </label>
                                      <span></span>
                                  </div>
                                  <input type="submit" id="loginSubmit" value="Accedi" />
                                  <p> Non sei ancora iscritto ? <a href="registrazione.html">Registrati</a></p>
                              </fieldset>
                          </form>
                      </div>
                  </div>
                  <div id="registrazione">
                      <form action="registrazione.html" id="ancoraRegistrazione">
                          <fieldset><button id="bottoneRegistrazione">Registrazione</button></fieldset>
                      </form>
                  </div>
              </div>
          </div>';
          }
          if($sezioneRicerca){
            $parte3 = "<div id ='menuBR'>
                          <div id='breadcrumb'>
                          <a href='#body' class='salto'>Salta il <span xml:lang='en'>breadcrumb</span> e vai al corpo della pagina</a>
                          <a href='#menu' class='salto'>Ritorna al menu</a>
                          ";
                          if($sessioneAperta == true)
                            $parte3 = $parte3."Bentornato<p>&nbsp;".$sezioneLogin.".&nbsp;</p>".$breadCrumb;
                            else
                            $parte3 = $parte3."".$breadCrumb;

                            $parte3 = $parte3."
                          </div></div>
                    </div>
                    <div id='body'>
                    <a href='#saltaRicerca' class='salto'>Salta l'area di ricerca</a>
                    <a href='#menu' class='salto'>Ritorna al menu</a>
                    <div id='areaRicerca'>
                    <div id = 'spazioDiRicerca'>
                          <form action='ricerca.php' method='get'>

                                <fieldset>

                                <legend>
                                Ricerca:<span></span>
                                </legend>
                                    <label for='ricercaForm'>Ricerca
                                    <input type ='text' name='ricercaForm'
                                    id='ricercaForm' maxlength='20'/>
                                    </label>


                                      <input type ='submit' id='subRicerca' value='Ricerca'/>
                                </fieldset>
                          </form>
                          </div>
                          <span id='saltaRicerca'  class='salto'></span>
                    </div>";
          }
          else {
            $parte3 = "<div id ='menuBR'>
                          <div id='breadcrumb'>

                          <a href='#body' class='salto'>Salta il <span xml:lang='en'>breadcrumb</span> e vai al corpo della pagina</a>
                          <a href='#menu' class='salto'>Ritorna al menu</a>
                            ";
                            if($sessioneAperta == true)
                              $parte3 = $parte3."Bentornato<p>&nbsp;".$sezioneLogin.".&nbsp;</p>".$breadCrumb;
                              else
                              $parte3 = $parte3."".$breadCrumb;
                          $parte3 = $parte3."
                          </div></div>
                    </div>
                    <div id='body'>";
          }
          return $parte1.$parte2.$parte3;
}
 ?>
