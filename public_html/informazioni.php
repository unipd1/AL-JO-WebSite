<?php
session_start();
require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "Informazioni - AL.JO. Gioielli Center";
$a2 = "Pagina contenente la storia della azienda
      e le informazioni di contatto";
$a3 = "Al.jo., Al.jo. Gioielli, oreficeria bronzo vicenza,
      oreficeria bronzo, contatti Al.jo.,
      informazioni Al.jo., oreficeria artigianale,
      oreficeria bronzo artigianale, oreficeria artigianale vicenza, vicenza";

$a4 = "index,follow";
echo printHeadHTML($a1,$a2,$a3,$a4);


$menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li class="active">Informazioni</li>';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;
if(isset($_SESSION['username'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}

$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Informazioni</p>';

echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

echo file_get_contents('Parti/informazioniBody.html');

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);
 ?>
