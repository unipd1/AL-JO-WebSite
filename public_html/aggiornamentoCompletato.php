<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('Parti/isUploadFile.php');
    require_once('../php/SelectInterrogation.php');
    require_once('../php/UpdateInterrogation.php');
    require_once('../php/DeleteInterrogation.php');
    require_once('../php/InsertInterrogation.php');

    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;

    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; <a href="sezioneDedicata.php">Sezione Dedicata</a> &gt; Modifica/Rimuovi</p>';
    }


    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

    try{

    if($sessioneAperta== true){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          if(!empty($_POST["submitModG"])){
             $insertInt = "";
             $table = array();
             $values = array("Id = '".$_POST['id']."'");
             $strErr = "";
             $errImm = "";
             $selectImg = new SelectInterrogation(array("Immagini, Descrizione, Nome, Peso"),array("gioielli"),$values);
             $img = "";
             $nomeG ="";
             $pesoG="";
             $descG="";
             $upd = false;
             try {
                 $risul = $selectImg->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }

             if($risul->num_rows > 0){
                   while($row = $risul->fetch_array(MYSQLI_ASSOC)){
                        $img = $row["Immagini"];
                        $nomeG =$row["Nome"];
                        $pesoG=$row["Peso"];
                        $descG=$row["Descrizione"];
                   }
               }
               $risul->free();


             if(!empty($_POST["Materiale"])&&
             !empty($_POST["Dimensione"])){

              if(isset($_POST["nome"])){
                if($nomeG!=$_POST["nome"]){
                      if(ctype_alnum(trim(str_replace(' ','',$_POST["nome"])))){
                             $nme = htmlentities($_POST["nome"]);
                             array_push($table,"Nome = '".$nme."'");
                        }
                        else {
                            $strErr=$strErr.'<p>Nome deve contenere solo caratteri alfanumerici e niente caratteri accentati</p>';
                        }
                    }
              }
              if(isset($_POST["PesoInsG"])){
                  if($_POST["PesoInsG"]!=$pesoG){
                      if(!preg_match("/[0.01-999.99]/",$_POST["PesoInsG"])){
                          $strErr=$strErr.'<p>Peso deve essere compreso tra 0.01 e 999.999</p>';
                      }
                      else
                        array_push($table,"Peso = ".$_POST["PesoInsG"]);
                    }
              }
              if(isset($_POST["descrizione"])){
                  if($descG!=$_POST["descrizione"]){
                    $es = htmlentities($_POST["descrizione"]);
                    $ass = preg_replace("/'/","&#39;",$es);
                    array_push($table,"Descrizione = '".$ass."'");
                }
              }

              if(!empty($_POST["sesso"])){
                  if($_POST["sesso"]!="default")
                    array_push($table,"Sesso = '".$_POST["sesso"]."'");
              }
              if(isset($_POST["esclusiva"])){
                  if($_POST["esclusiva"]!="default")
                    array_push($table,"Esclusiva = '".$_POST["esclusiva"]."'");
              }

              if(strlen($strErr)==0){
                  if (isset($_FILES["immagine"]) && is_uploaded_file($_FILES["immagine"]['tmp_name'])){
                      if(strlen($img)>0){
                          if(file_exists($img)){
                              unlink($img);
                          }
                      }
                      $errImm = isUploadFile('img/gioielli/'.$_POST["id"],"immagine");
                      if(($errImm=='png')||($errImm=='jpeg')||($errImm=='jpg')){
                            array_push($table," Immagini = 'img/gioielli/".$_POST["id"].".".$errImm."'");
                        }
                        else {
                                if($errImm!="IMGNOTINSERT")
                                    $strErr=$strErr.'<p>'.$errImm.'</p>';
                        }
                    }
              }
              if(!empty($_POST["Collezione"])){
                  if($_POST["Collezione"]!="default"){
                      if($_POST["Collezione"]=="NULL"){
                          array_push($table,"Collezione = NULL ");
                      }else {
                           array_push($table,"Collezione= '".$_POST["Collezione"]."'");
                      }

              }
          }
              if(empty($_POST["parour"])){
                  if($_POST["Parour"]!="default"){
                      if($_POST["Parour"]=="NULL"){
                          array_push($table,"Parour = NULL ");
                      }else {
                           array_push($table,"Parour= '".$_POST["Parour"]."'");
                      }
              }
          }else {
             array_push($table,"Parour= '".$_POST["parour"]."'");
          }
              if(strlen($strErr)==0){
                  if(count($table)>0){
              $insertInt = new UpdateInterrogation(array("gioielli"),$table,$values);
              try{
                  $insertInt->interrogation();
              }
              catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }
          }
              if(!empty($_POST["Materiale"])){
                  if($_POST["Materiale"]!="default"){
                      $upd = true;
                      $piecesMat = explode("@", $_POST['Materiale']);
                      $insertMat = new UpdateInterrogation(array("prodotto"),
                        array("Nome_Materiale = '".$piecesMat[0]."'","Colore_Materiale = '".$piecesMat[1]."'"),
                        array("Gioiello = '".$_POST['id']."'"));
                        try {
                            $insertMat->interrogation();
                        } catch (Exception $e) {
                            throw new Exception($e->getMessage());
                        }
                        }
                    }
                if(!empty($_POST["Pietra"])){
                    if($_POST["Pietra"]!="default"){
                        $upd = true;
                         if($_POST["Pietra"]=="NULL"){
                             $insertPietr = new DeleteInterrogation("ornamento",
                             array("Gioiello = '".$_POST['id']."'"));
                         }else {
                             $selInt = new SelectInterrogation(array("Nome_Pietra, Colore_Pietra, Gioiello"),
                             array("ornamento"),array("Gioiello = '".$_POST['id']."'"));

                             try {
                                 $arrayRisultato = $selInt->interrogation();
                             } catch (Exception $e) {
                                 throw new Exception($e->getMessage());
                             }

                             $risultato = $arrayRisultato->num_rows;
                             $arrayRisultato->free();

                             $piecesPietr = explode("@", $_POST['Pietra']);

                             if($risultato > 0){
                                 $insertPietr = new UpdateInterrogation(array("ornamento"),
                                 array("Nome_Pietra = '".$piecesPietr[0]."'","Colore_Pietra = '".$piecesPietr[1]."'"),
                                 array("Gioiello = '".$_POST['id']."'"));
                            }else {
                                $insertPietr = new InsertInterrogation(array("ornamento"),
                                array("Gioiello","Nome_Pietra","Colore_Pietra"),
                                array(array($_POST['id'],$piecesPietr[0],$piecesPietr[1])));
                            }
                         }
                         try{
                             $insertPietr->interrogation();
                         } catch (Exception $e) {
                             throw new Exception($e->getMessage());
                         }
                }
            }
            if(!empty($_POST["Dimensione"])){
                if($_POST["Dimensione"]!="default"){
                    $upd = true;
                    $insertIntDim = new UpdateInterrogation(array("taglia"),
                    array("Dimensione ='".$_POST['Dimensione']."'"),
                    array("Gioiello = '".$_POST['id']."'"));
                    try {
                        $insertIntDim->interrogation();
                    } catch (Exception $e) {
                        throw new Exception($e->getMessage());
                    }
                }
            }
            if(count($table)>0||$upd){
                    echo '<p>Aggiornamento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
                }else {
                    if($errImm=="")
                        echo '<p>Aggiornamento non riuscito in quanto nulla &egrave; stato cambiato rispetto a prima torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
                        else {
                            echo '<p>Aggiornamento  riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
                        }
                }
                }
                else{
                    echo '<p>Errore Compilazione Form, ritorna alla  <a href="lista_gioielli.php">lista gioielli</a>  da modificare .</p>';
                }
            }else {
                echo '<p>Errore Compilazione Form, ritorna alla  <a href="lista_gioielli.php">lista gioielli</a>  da modificare .</p>';
            }
          }
            elseif(!empty($_POST["submitModC"])){
                $insertInt = "";
                $table = array();
                $values = array("Nome = '".$_POST["nomeCollezione"]."'");

                $selectImg = new SelectInterrogation(array("Immagini, Descrizione"),array("collezione"),$values);
                $img = "";
                $descr = "";
                $strErr="";
                try {
                    $risul = $selectImg->interrogation();
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }
                if($risul->num_rows > 0){
                      while($row = $risul->fetch_array(MYSQLI_ASSOC)){
                          $img = $row["Immagini"];
                          $descr = $row["Descrizione"];
                      }
                  }


                if(isset($_POST["descrizioneCollezioneMod"])){
                    if($_POST["descrizioneCollezioneMod"]!=$descr){
                      $es = htmlentities($_POST["descrizioneCollezioneMod"]);
                      $ass = preg_replace("/'/","&#39;",$es);
                      array_push($table,"Descrizione = '".$_POST["descrizioneCollezioneMod"]."'");
                  }
                }


                if(isset($_POST["esclusivaCollezione"])){
                    if($_POST["esclusivaCollezione"]!="default")
                        array_push($table,"Esclusiva ='".$_POST["esclusivaCollezione"]."'");
                }

                if(strlen($strErr)==0){
                    if (isset($_FILES["immagineCollezione"]) && is_uploaded_file($_FILES["immagineCollezione"]['tmp_name'])){
                        if(strlen($img)>0){
                            if(file_exists($img)){
                                unlink($img);
                            }
                        }
                        $errImm = isUploadFile('img/collezioni/'.$_POST["nomeCollezione"],"immagineCollezione");
                        if(($errImm=='png')||($errImm=='jpeg')||($errImm=='jpg')){
                              array_push($table," Immagini = 'img/collezioni/".$_POST["nomeCollezione"].".".$errImm."'");
                          }
                          else {
                                if($errImm!="IMGNOTINSERT")
                                  $strErr=$strErr.'<p>'.$errImm.'</p>';
                              }
                    }
                }

                if(strlen($strErr)==0&&count($table)>0){

                $insertInt = new UpdateInterrogation(array("collezione"),$table,$values);
                try{
                        $insertInt->interrogation();
                    } catch (Exception $e) {
                        throw new Exception($e->getMessage());
                    }


                echo '<p>Aggiornamento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
            }
            else if(strlen($strErr)>0){
                echo '<p>Errore Compilazione Form,</p>'.$strErr.'<p> ritorna alla <a href="lista_collezioni">lista collezioni</a>da modificare.</p>';
    }else {
        echo '<p>Nessun aggiornamento, i valori sono rimasti quelli di default, torna alla <a href="sezioneDedicata.php">Sezione dedicata</a>.</p>';
    }
}
}
}
} catch (Exception $e) {
    echo $e->getMessage();
}
    require_once("Parti/footer.php");
    echo printfooter($sessioneAperta);
?>
