<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/DeleteInterrogation.php');

    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;
    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    $sessioneAperta = true;

    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }
    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; <a href="sezioneDedicata.php">Sezione Dedicata</a> &gt; Modifica/Rimuovi</p>';



    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);


    if($sessioneAperta== true){

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          if(!empty($_POST["modificaGiol"])){
                $valore = $_POST["modificaGiol"];

                $InterrogationInitial = new SelectInterrogation(
                    array("Id","Nome","Peso","Parour","Descrizione","Sesso","Esclusiva","Collezione","Immagini"),
                    array("gioielli"),
                    array("Id ='".$valore."'"));

                $arrayResult = $InterrogationInitial->interrogation();
                if($arrayResult->num_rows > 0){
                      while($row = $arrayResult->fetch_array(MYSQLI_ASSOC)){
                          $colVal = $row['Collezione'];
                          $parourVal = $row['Parour'];
                          echo '<div id="Ins">

                          <a href="#footer" class="salto">Salta la form di modifica e vai al <span xml:lang="en">footer</span></a>
                          <a href="#menu" class="salto">Ritorna al menu</a>
                          <a href="#menuSD" class="salto">Ritorna al menu di sezione dedicata</a>
                          <form enctype="multipart/form-data" action="aggiornamentoCompletato.php" method="post">
                          <fieldset>
                            <legend>Aggiorna Gioiello</legend>
                            <div>
                             <label for="IdModG">ID
                              <input type ="text" name="id" id="IdModG" maxlength="20" value = "'.$valore.'" readonly="readonly"/>
                             </label>
                             <span></span>
                            </div>
                            <div>
                            <label for="NomeModG">Nome
                              <input type="text" name="nome" value="'.$row['Nome'].'" id= "NomeModG" maxlength="20" ></input>
                              </label>
                              <span></span>
                            </div>
                             <div>
                             <label for="MassaModG">Peso
                               <input type="text" name ="PesoInsG" value="'.$row['Peso'].'" id="MassaModG"/><abbr title="grammi">gr</abbr>
                               </label>
                               <span></span>
                             </div>
                              <div>
                              <label for="DescrizioneModG">Descrizione
                                <textarea name="descrizione" id ="DescrizioneModG" rows="5" cols="40">'.$row['Descrizione'].'</textarea>
                                </label>
                                <span></span>
                              </div>';

                              if($row['Sesso']=="Maschile"){
                                  echo '
                                  <div>
                                  Sesso
                                    <label for="SessoModGUnisex"><span><input type="radio" name="sesso" id="SessoModGUnisex" value="Unisex"  />Unisex</span></label>
                                    <label for="SessoModGM"><span><input type="radio" name="sesso" id="SessoModGM" value="default" checked="checked" />Maschile</span></label>
                                    <label for="SessoModGF"><span><input type="radio" name="sesso" id="SessoModGF" value="Femminile" />Femminile</span></label>

                                    <span></span></div>';
                              }elseif ($row['Sesso']=="Femminile") {
                                  echo '
                                  <div>
                                  Sesso
                                    <label for="SessoModGUnisex"><span><input type="radio" name="sesso" id="SessoModGUnisex" value="Unisex"  />Unisex</span></label>
                                    <label for="SessoModGM"><span><input type="radio" name="sesso" id="SessoModGM" value="Maschile" />Maschile</span></label>
                                    <label for="SessoModGF"><span><input type="radio" name="sesso" id="SessoModGF" value="default" checked="checked" />Femminile</span></label>

                                    <span></span></div>';
                              }else {
                                  echo '
                                  <div>
                                  Sesso
                                    <label for="SessoModGUnisex"><span><input type="radio" name="sesso" id="SessoModGUnisex" value="default" checked="checked" />Unisex</span></label>
                                    <label for="SessoModGM"><span><input type="radio" name="sesso" id="SessoModGM" value="Maschile" />Maschile</span></label>
                                    <label for="SessoModGF"><span><input type="radio" name="sesso" id="SessoModGF" value="Femminile" />Femminile</span></label>

                                    <span></span></div>';
                              }
                              if($row['Esclusiva']==1){
                              echo '
                              <div>
                              Esclusiva
                                <label for="EsclusivaModGNo"><span><input type="radio" id="EsclusivaModGNo" name="esclusiva" value="0"  />No</span></label>
                                <label for="EsclusivaModGSi"><span><input type="radio" id="EsclusivaModGSi" name="esclusiva" value="default" checked="checked" />Si</span></label>
                                <span></span>
                              </div>';
                          }else {
                              echo '
                              <div>
                              Esclusiva
                                <label for="EsclusivaModGNo"><span><input type="radio" id="EsclusivaModGNo" name="esclusiva" value="default" checked="checked" />No</span></label>
                                <label for="EsclusivaModGSi"><span><input type="radio" id="EsclusivaModGSi" name="esclusiva" value="1" />Si</span></label>
                                <span></span>
                              </div>';
                          }

                              echo '
                              <div>
                              <label for="ImmagineModG">Immagine
                                <input type="file" name="immagine" id="ImmagineModG" />
                                </label>
                                <span></span>
                                </div>
                                 <div>
                                 <label for="MaterialeModG">Seleziona materiale
                                <select name="Materiale" id="MaterialeModG">';

                                $matVal = "";
                                $matNm = "";
                                $matCol = "";

                                $mInterrogation = new SelectInterrogation(array("Nome","Colore"),
                                array("materiale","prodotto"),array("prodotto.Colore_Materiale = Colore","AND",
                                "prodotto.Nome_Materiale = Nome","AND","prodotto.Gioiello = '".$valore."'"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          $matVal='<option value="default" selected="selected">'.$row['Nome'].' '.$row['Colore'].' default</option>';
                                          $matNm = $row['Nome'];
                                          $matCol = $row['Colore'];
                                      }
                                }
                                $arrayInterrogationm->free();

                                if(strlen($matVal)>0){
                                    echo $matVal;
                                }

                                $mInterrogation = new SelectInterrogation(array("Nome","Colore"),
                                array("materiale"),array("Nome <> '".$matNm."'","AND","Colore <> '".$matCol."'"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          echo '<option value="'.$row['Nome'].'@'.$row['Colore'].'">'.$row['Nome'].' '.$row['Colore'].'</option>';
                                      }
                                }

                                $arrayInterrogationm->free();

                                $mInterrogation = new SelectInterrogation(array("Codice","Tipologia","Grandezza","UnitaDiMisura"),
                                array("dimensione"),array("dimensione.Codice in (SELECT taglia.Dimensione FROM taglia WHERE taglia.Gioiello = '".$valore."' )"));

                                $dimVal = "";
                                $dimCod = "";

                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                         $dimVal = '<option value="default" selected="selected">'.$row['Tipologia'].' '.$row['Grandezza'].$row['UnitaDiMisura'].' default</option>';
                                         $dimCod = $row['Codice'];
                                      }
                                }

                                $arrayInterrogationm->free();


                             echo '
                                </select></label><span></span>oppure
                                <a href="inserimenti.php?ins=mat">Inserisci Materiale</a></div>
                                <div><label for="DimensioneModG">Seleziona dimensione
                                <select name="Dimensione" id="DimensioneModG">';
                                if(strlen($dimVal)>0){
                                    echo $dimVal;
                                }

                                $mInterrogation = new SelectInterrogation(array("Codice","Tipologia","Grandezza","UnitaDiMisura"),
                                array("dimensione"),array("dimensione.Codice not in (SELECT pietra.Dimensione FROM pietra )","AND","Codice <> '".$dimCod."'"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          echo '<option value="'.$row['Codice'].'">'.$row['Tipologia'].' '.$row['Grandezza'].$row['UnitaDiMisura'].'</option>';
                                      }
                                }

                                $ptrVal = "";
                                $ptrNm = "";
                                $ptrCol = "";

                                $mInterrogation = new SelectInterrogation(array("Nome","Colore","Tipologia","Grandezza","UnitaDiMisura"),
                                array("pietra","dimensione","ornamento"),array("pietra.Dimensione = dimensione.Codice","AND","Colore = ornamento.Colore_Pietra","AND","Nome = ornamento.Nome_Pietra","AND","ornamento.Gioiello = '".$valore."'"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          $ptrVal ='<option value="default" selected="selected">'.$row['Nome'].' '.$row['Colore'].' default</option>';
                                          $ptrNm = $row['Nome'];
                                          $ptrCol = $row['Colore'];
                                      }
                                }
                                $arrayInterrogationm->free();

                                echo '</select></label><span></span>oppure
                                <a href="inserimenti.php?ins=dim">Inserisci Dimensione</a></div><div>
                                <label for="PietraModG">Seleziona pietra
                                <select name="Pietra" id="PietraModG">';
                                if(strlen($ptrVal)>0){
                                    echo $ptrVal;
                                    echo'<option value="NULL" class="italic">NULL</option>';
                                }
                                else {
                                    echo'<option value="default" selected="selected">default NULL</option>';
                                }

                                $mInterrogation = new SelectInterrogation(array("Nome","Colore","Tipologia","Grandezza","UnitaDiMisura"),
                                array("pietra","dimensione"),array("pietra.Dimensione = dimensione.Codice","AND","Nome <> '".$ptrNm."'","AND","Colore <> '".$ptrCol."'"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          echo '<option value="'.$row['Nome'].'@'.$row['Colore'].'">'.$row['Nome'].' '.$row['Colore'].' </option>';
                                      }
                                }
                                $arrayInterrogationm->free();
                                echo '</select></label><span></span>oppure
                                <a href="inserimenti.php?ins=ptr">Inserisci Pietra</a></div><div>
                                <label for="CollezioneModG">Collezione
                                <select name="Collezione" id="CollezioneModG">';
                                    if(strlen($colVal)>0){
                                        echo '<option value="default" selected="selected">'.$colVal.' default</option>';
                                        echo '<option value="NULL" class="italic">NULL</option>';
                                    }
                                    else {
                                        echo '<option value="default" selected="selected">default NULL</option>';
                                    }

                                $mInterrogation = new SelectInterrogation(array("Nome","Esclusiva"),
                                array("collezione"),array("1"));
                                $arrayInterrogationm = $mInterrogation->interrogation();
                                if($arrayInterrogationm->num_rows > 0){
                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                          echo '<option value="'.$row['Nome'].'">'.$row['Nome'].' Esclusiva '.$row['Esclusiva'].'</option>';
                                      }
                                }
                                $arrayInterrogationm->free();

                                 echo '
                                </select></label><span></span>oppure
                                <a href="inserimenti.php?ins=collezioni">Inserisci Collezione</a></div><div>
                                <label for="ParourModG"><span xml:lang="fr">Parure</span>
                                Modifica
                                <input type="text" name="parour" id="ParourModG"/>
                                <span id="break">oppure seleziona</span>
                                <select name="Parour" id="parourSelezione">';
                                if(strlen($parourVal)>0){
                                    echo '<option value="default" selected="selected">'.$parourVal.' default</option>';
                                    echo '<option value="NULL" class="italic">NULL</option>';
                                }
                                else {
                                    echo '<option value="default" selected="selected">default NULL</option>';
                                }

                                $mInterrogation = new SelectInterrogation(array("DISTINCT Parour"),
                                array("gioielli"),array("1"));

                                $arrayInterrogationm = $mInterrogation->interrogation();

                                if($arrayInterrogationm->num_rows > 0){

                                      while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                        if($row['Parour']!=$parourVal)
                                          echo '<option value="'.$row['Parour'].'">'.$row['Parour'].'</option>';
                                      }
                                }

                                $arrayInterrogationm->free();

                                echo '</select></label></div><span></span>
                                <input type="submit" value="Aggiorna" name = "submitModG" id="submitModG" />
                                <input type="reset" id="resetModG"/>
                          </fieldset>
                        </form></div>';
                      }
                }

                $arrayResult->free();


             }
             elseif (!empty($_POST['modificaColl'])) {
                 $valore = $_POST['modificaColl'];
                 $InterrogationInitial = new SelectInterrogation(
                     array("Nome","Descrizione","Esclusiva","Immagini"),
                     array("collezione"),
                     array("Nome ='".$valore."'"));


                 $arrayResult = $InterrogationInitial->interrogation();
                 if($arrayResult->num_rows > 0){
                       while($row = $arrayResult->fetch_array(MYSQLI_ASSOC)){
                 echo '
                 <div id="Ins">


                 <a href="#footer" class="salto">Salta la form di modifica e vai al <span xml:lang="en">footer</span></a>
                 <a href="#menu" class="salto">Ritorna al menu</a>
                 <a href="#menuSD" class="salto">Ritorna al menu di sezione dedicata</a>
                 <form enctype="multipart/form-data" action="aggiornamentoCompletato.php" method="post">
                 <fieldset>

                   <legend>Aggiorna Collezione</legend>';
                   echo '
                   <div>
                   <label for="nomeC">Nome
                     <input type ="text" id="nomeC" name="nomeCollezione" value ="'.$row['Nome'].'"  maxlength="20" readonly="readonly" />
                     </label><span></span>
                   </div>';
                   if($row['Esclusiva']==0){
                      echo '
                     <div>Esclusiva
                       <label for="esclusivaCollezioneNo"><span><input type="radio" name="esclusivaCollezione" id="esclusivaCollezioneNo" value="default" checked="checked" />No</span></label>
                       <label for="esclusivaCollezioneSi"><span><input type="radio" name="esclusivaCollezione" id="esclusivaCollezioneSi" value="1" />Si</span></label>
                      </div>';
                  }else {
                      echo '
                      <div>Esclusiva
                        <label for="esclusivaCollezioneNo"><span><input type="radio" name="esclusivaCollezione" id="esclusivaCollezioneNo" value="0"  />No</span></label>
                        <label for="esclusivaCollezioneSi"><span><input type="radio" name="esclusivaCollezione" id="esclusivaCollezioneSi" value="default" checked="checked" />Si</span></label>
                       </div>';
                  }

                      echo '
                     <div>
                     <label for="descrizioneCollezioneMod">Descrizione
                       <textarea name="descrizioneCollezioneMod" id="descrizioneCollezioneMod" rows="5" cols="40">'.$row['Descrizione'].'</textarea>
                       </label>
                       <span></span>
                     </div>
                     <div>
                     <label for="immagineCollezione">Immagine
                       <input type="file" name="immagineCollezione" id="immagineCollezione" />
                       </label>
                       </div>
                       <input type="submit" value="Aggiorna" name="submitModC" id="submitModC"/>
                       <input type="reset" id="resetModC" />
                 </fieldset>
               </form></div>';

             }
      }
    }
    elseif(!empty($_POST['eliminaG'])) {
        if(!empty($_POST['eliminaGiol'])){

        $where = array();
        $where1 = array();
        $values = isset($_POST['eliminaGiol']) ? $_POST['eliminaGiol'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
            <a href="lista_gioielli.php">lista di gioielli</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    array_push($where,"Id ='".$values[$i]."'");
                    array_push($where1,"Gioiello ='".$values[$i]."'");
                }else {
                    array_push($where,"OR","Id ='".$values[$i]."'");
                    array_push($where1,"OR","Gioiello ='".$values[$i]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("esclusivagioiello",$where1);
            $deleteInt->interrogation();
            $deleteInt = new DeleteInterrogation("prodotto",$where1);
            $deleteInt->interrogation();
            $deleteInt = new DeleteInterrogation("ornamento",$where1);
            $deleteInt->interrogation();
            $deleteInt = new DeleteInterrogation("taglia",$where1);
            $deleteInt->interrogation();
            $selectImage = new SelectInterrogation(array("Immagini"),array("gioielli"),$where);
            $arrayRisultato = $selectImage->interrogation();
            if($arrayRisultato->num_rows > 0){
                  while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
                    if(!($row["Immagini"])=="")
                      if(file_exists($row["Immagini"]))
                        unlink($row["Immagini"]);
                  }
             }
            $deleteInt = new DeleteInterrogation("gioielli",$where);
            $deleteInt->interrogation();
            echo '<p>Eliminazione eseguita con successo ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
        <a href="lista_gioielli.php">lista di gioielli</a>.</p>';
    }
    }
    elseif (!empty($_POST['eliminaC'])) {
        if(!empty($_POST['eliminaColl'])){

        $where = array();
        $where1 = array();
        $immaginiEl = array();
        $values = isset($_POST['eliminaColl']) ? $_POST['eliminaColl'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno una collezione, torna alla
            <a href="modifica.php?del=collezioni">lista di collezioni</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    array_push($where,"Nome ='".$values[$i]."'");
                    array_push($where1,"Collezione ='".$values[$i]."'");

                }else {
                    array_push($where,"OR","Nome ='".$values[$i]."'");
                    array_push($where1,"OR","Collezione ='".$values[$i]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("esclusivacollezione",$where1);
            $deleteInt->interrogation();
            $selectImage = new SelectInterrogation(array("Immagini"),array("collezione"),$where);
            $arrayRisultato = $selectImage->interrogation();
            if($arrayRisultato->num_rows > 0){
                  while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
                      if(!($row["Immagini"])=="")
                       if(file_exists($row["Immagini"]))
                        unlink($row["Immagini"]);
                  }
             }
             $arrayRisultato->free();
            $deleteInt = new DeleteInterrogation("collezione",$where);
            $deleteInt->interrogation();

            echo '<p>Eliminazione eseguita con successio ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno una collezione, torna alla
        <a href="lista_collezioni.php">lista di collezioni</a>.</p>';
    }
    }
    elseif (!empty($_POST['eliminaM'])) {
        if(!empty($_POST['eliminaMat'])){

        $where = array();
        $where1 = array();
        $values = isset($_POST['eliminaMat']) ? $_POST['eliminaMat'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno un materiale, torna alla
            <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    $piecesMat = explode("@", $values[$i]);
                    array_push($where,"Nome ='".$piecesMat[0]."' AND Colore ='".$piecesMat[1]."'");
                }else {
                    array_push($where,"OR","Nome ='".$piecesMat[0]."' AND Colore ='".$piecesMat[1]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("materiale",$where);
            $deleteInt->interrogation();
            echo '<p>Eliminazione eseguita con successio ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno un materiale, torna alla
        <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
    }
    }
    elseif (!empty($_POST['eliminaD'])) {
        if(!empty($_POST['eliminaDim'])){

        $where = array();
        $where1 = array();
        $values = isset($_POST['eliminaDim']) ? $_POST['eliminaDim'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno una dimensione, torna alla
            <a href="modifica.php?del=dim">lista di dimensioni</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    array_push($where,"Codice ='".$values[$i]."'");
                }else {
                    array_push($where,"OR","Codice ='".$values[$i]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("dimensione",$where);
            $deleteInt->interrogation();
            echo '<p>Eliminazione eseguita con successio ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno una dimensione, torna alla
        <a href="modifica.php?del=dim">lista di dimensioni</a>.</p>';
    }
    }
    elseif (!empty($_POST['eliminaP'])) {
        if(!empty($_POST['eliminaPtr'])){

        $where = array();
        $where1 = array();
        $values = isset($_POST['eliminaPtr']) ? $_POST['eliminaPtr'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno una pietre, torna alla
            <a href="modifica.php?del=ptr">lista di pietre</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    $piecesMat = explode("@", $values[$i]);
                    array_push($where,"Nome ='".$piecesMat[0]."' AND Colore ='".$piecesMat[1]."'");
                }else {
                    array_push($where,"OR","Nome ='".$piecesMat[0]."' AND Colore ='".$piecesMat[1]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("pietra",$where);
            $deleteInt->interrogation();
            echo '<p>Eliminazione eseguita con successio ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno una pietra, torna alla
        <a href="modifica.php?del=ptr">lista di pietre</a>.</p>';
    }
    }
    elseif (!empty($_POST['eliminaU'])) {
        if(!empty($_POST['eliminaUtente'])){

        $where = array();
        $where1 = array();
        $values = isset($_POST['eliminaUtente']) ? $_POST['eliminaUtente'] : array();

        if (!count($values)){
            echo '<p>Errore! Devi selezionare almeno un utente, torna alla
            <a href="modifica.php?del=ute">lista di utenti</a>.</p>';
        }
        else {
            for ($i=0; $i <count($values) ; $i++) {
                if($i==0){
                    array_push($where,"Email ='".$values[$i]."'");
                    array_push($where1,"Utente ='".$values[$i]."'");
                }else {
                    array_push($where,"OR","Email ='".$values[$i]."'");
                    array_push($where1,"OR","Utente ='".$values[$i]."'");
                }

            }
            $deleteInt = new DeleteInterrogation("esclusivacollezione",$where1);
            $deleteInt->interrogation();
            $deleteInt = new DeleteInterrogation("esclusivagioiello",$where1);
            $deleteInt->interrogation();
            $deleteInt = new DeleteInterrogation("utente",$where);
            $deleteInt->interrogation();
            echo '<p>Eliminazione eseguita con successio ! Torna alla
            <a href="sezioneDedicata.php">sezione dedicata</a>.</p>';
        }

    }else {
        echo '<p>Errore! Devi selezionare almeno un utente, torna alla
        <a href="modifica.php?del=ute">lista di utenti</a>.</p>';
    }
    }
}

}
require_once("Parti/footer.php");
echo printfooter($sessioneAperta);
?>
