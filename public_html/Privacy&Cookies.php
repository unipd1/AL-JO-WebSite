<?php
session_start();
require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "Privacy&amp;Cookies - AL.JO. Gioielli Center";
$a2 = "Pagina in cui è presente la legge di utilizzo dei cookies";
$a3 = "Privacy, Cookies, AL.JO., Vicenza";

$a4 = "index,follow";
echo printHeadHTML($a1,$a2,$a3,$a4);


$menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>
         ';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;
if(isset($_SESSION['username'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}

$breadCrumb ='<p>Ti trovi in :<a href="index.php" xml:lang="en">Home</a> &gt; <span xml:lang="en">Privacy&amp;Cookies</span></p>';
echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

echo file_get_contents('Parti/PrivacyBody.html');

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);
 ?>
