<?php
session_start();
require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "SiteMap - AL.JO. Gioielli Center";
$a2 = "Mappa del sito contente i link della pagina";
$a3 = "SiteMap, AL.JO., Collane, Bracciali, Anelli";
$a4 = "index,follow";
echo printHeadHTML($a1,$a2,$a3,$a4);

         $sessioneAperta = false;
         $sezioneLogin = "";
         $admin = 0;
         if(isset($_SESSION['username'])){
               $sessioneAperta = true;
               $sezioneLogin =$_SESSION['username'];
               $email = $_SESSION['email'];
               $admin = $_SESSION['admin'];
         }

         $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
                  <li><a href="lista_gioielli.php">Gioielli</a></li>
                  <li><a href="lista_collezioni.php">Collezioni</a></li>
                  <li><a href="informazioni.php">Informazioni</a></li>';

          if($sessioneAperta== true){
              if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
          }

         $breadCrumb = '<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; <span xml:lang="en">SiteMap</span></p>';

         echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
         echo "<a href='#menu' class='salto'>Ritorna al menu</a>
         <a href='#footer' class='salto'>Vai al <span xml:lang='en'>footer</span></a>";

         try {

         echo '<div id ="siteMap">
         <a href="#saltaMenuSezDed" class="salto">Salta il menu di sezione dedicata</a>
         <a href="#menu" class="salto">Ritorna al menu</a>
         <ul id="ulMap">';
         //echo file_get_contents('');/*DA METTERE A POSTO (pARTE DI BODY)*/
         if($sessioneAperta==true){
                  echo'<li><a href="logout.php">Logout</a></li>';
         }else {
                  echo'<li><a href="login.html">Login</a></li>';
                  echo'<li><a href="registrazione.html">Registrazione</a></li>';
         }

         $select = array("DISTINCT Id, Nome");
         $from = array("gioielli","taglia","dimensione", "prodotto");
         $where = array("prodotto.Gioiello=gioielli.Id",
                  "AND", "gioielli.Id = taglia.Gioiello", "AND",
                  "taglia.Dimensione = dimensione.Codice");
         if($sessioneAperta==true){
                  if($admin==1)
                           $where = array("1");
                  else{
                           $where = array("Esclusiva = 0");
                           array_push($where," UNION SELECT gioielli.Id,gioielli.Nome as Nm
                           FROM gioielli,esclusivagioiello,
                           taglia, dimensione, prodotto
                           WHERE esclusivagioiello.Utente = '".$email."' AND
                           esclusivagioiello.Gioiello = gioielli.Id AND gioielli.Esclusiva = 1
                           AND prodotto.Gioiello=gioielli.Id AND gioielli.Id = taglia.Gioiello
                           AND taglia.Dimensione = dimensione.Codice");
                  }
         }
         else {
                  $where = array("Esclusiva = 0");
         }
         $interrogation = new SelectInterrogation($select,$from,$where);
         try {
                  $arrayRisultato = $interrogation->interrogation();
         } catch (Exception $e) {
                  throw new Exception($e->getMessage());
         }
         echo'
           <li>
             <a href="index.php">Home</a>
           </li>';
         echo '
           <li>
             <a href="lista_gioielli.php">Gioielli</a>';
             $risultato1 = $arrayRisultato->num_rows;
             if($risultato1>0){
                  echo'<ul>';
                  while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
                           echo '<li><a href="gioiello.php?id='.$row['Id'].'">'.$row['Nome'].'</a></li>';
                  }
                  echo'</ul>';
         }
          echo '
           </li>';

           $select = array("Nome");
           $from = array("collezione");
           if($sessioneAperta==true){
                    if($admin==1)
                             $where = array("1");
                    else{
                             $where = array("Esclusiva = 0");
                             array_push($where, " UNION  SELECT Nome
                             FROM collezione JOIN esclusivacollezione ON
                             collezione.Nome = esclusivacollezione.Collezione
                             WHERE Esclusiva = 1 AND esclusivacollezione.Utente ='".$email."'");
                    }
           }else {
                    $where = array("Esclusiva = 0");
           }
           $interrogation = new SelectInterrogation($select,$from,$where);
           try {
                    $arrayRisultato = $interrogation->interrogation();
           } catch (Exception $e) {
                    throw new Exception($e->getMessage());
           }
           $risultato2 = $arrayRisultato->num_rows;

         echo '
           <li>
             <a href="lista_collezioni.php">Collezioni</a>';
             if($risultato2>0){
                  echo'<ul>';
                  while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
                           echo '<li><a href="collezione.php?name='.$row['Nome'].'">'.$row['Nome'].'</a></li>';

                  }
                  echo'</ul>';
         }
          echo '
           </li>';
           echo '
           <li>
             <a href="informazioni.php">Infomazioni</a>
           </li>
           <li>
             <a href="ricerca.php">Ricerca</a>
           </li>';
           if($sessioneAperta==true&&$admin==1){
                  echo '
                  <li>
                   <a href="sezioneDedicata.php">Sezione Dedicata</a>
                   <ul>
                   <li>
                   <a href="inserimenti.php?ins=gioielli">Inserimento Gioiello</a>
                   </li>
                   <li>
                   <a href="inserimenti.php?ins=collezioni">Inserimento Collezione</a>
                   </li>
                   <li>
                   <a href="inserimenti.php?ins=dim">Inserimento Dimensione</a>
                   </li>
                   <li>
                   <a href="inserimenti.php?ins=mat">Inserimento Materiale</a>
                   </li>
                   <li>
                   <a href="inserimenti.php?ins=ptr">Inserimento Pietre</a>
                   </li>
                   <li>
                   <a href="lista_gioielli.php">Modifica/Rimuovi Gioielli</a>
                   </li>
                   <li>
                   <a href="lista_collezioni.php">Modifica/Rimuovi Collezioni</a>
                   </li>
                   <li>
                   <a href="modifica.php?del=dim">Rimuovi Dimensione</a>
                   </li>
                   <li>
                   <a href="modifica.php?del=mat">Rimuovi Materiale</a>
                   </li>
                   <li>
                   <a href="modifica.php?del=ptr">Rimuovi Pietra</a>
                   </li>
                   <li>
                   <a href="modifica.php?del=ute">Rimuovi Utenti</a>
                   </li>
                   <li>
                   <a href="aggiornaEsclusive.php">Aggiorna Esclusive</a>
                   </li>
                   </ul>
                 </li>';
           }
         echo'
         </ul>
         <span id="saltaMenuSezDed" class="salto"></span>
         </div>';
         } catch (Exception $e) {
                  echo $e->getMessage();
         }

         require_once('Parti/footer.php');
         echo printfooter($sessioneAperta);
?>
