<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');

    $a1 = "Lista Gioielli - AL.JO. Gioielli Center";

    if ($_SERVER["REQUEST_METHOD"] == "GET") {
      if(!empty($_GET["tipo"])){
        if(!empty($_GET["ord"])){
          $a1 = "Lista Gioielli ".$_GET["tipo"]." ".$_GET["ord"]." - AL.JO. Gioielli Center";
        }
        else{
          $a1 = "Lista Gioielli ".$_GET["tipo"]." - AL.JO. Gioielli Center";
        }
      }
      else{
        if(!empty($_GET["ord"])){
          $a1 = "Lista Gioielli ".$_GET["ord"]." - AL.JO. Gioielli Center";
        }
      }
    }

    $a2 = "Pagina in cui c'è una lista di gioielli
        in cui se ne può selezionare uno per andare nel dettaglio";
    $a3 = "Gioielli, AL.JO., Collane, Bracciali, Anelli, Vicenza";
    $a4 = "index,follow";
    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li class="active">Gioielli</li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>
    ';
    $admin=0;
    $sessioneAperta = false;
    $sezioneLogin = "";

    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Lista Gioielli</p>';

    echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);

    try {

    $stringaDiQuery = $_SERVER['QUERY_STRING'];
    $stringaOrder = "";
    $stringaOrdURL = "";
    $risultato=0;

    $countMatchAsc = preg_match('/(asc)/',$stringaDiQuery);
    $countMatchDesc = preg_match('/(desc)/',$stringaDiQuery);
    $countMatchNm = preg_match('/(Nm)/',$stringaDiQuery);
    $countMatchDim = preg_match('/(Dim)/',$stringaDiQuery);
    $countMatchMat = preg_match('/(Mat)/',$stringaDiQuery);
    $countMatchNum = preg_match('/numero/',$stringaDiQuery);
    $countMatchTipo = preg_match('/tipo/',$stringaDiQuery);

    if($countMatchNm>0){
        $stringaOrder = "Nm";
    }elseif ($countMatchDim>0) {
        $stringaOrder = "Dim";
    }elseif ($countMatchMat>0) {
        $stringaOrder = "Mat";
    }
    if(strlen($stringaOrder)>0){
        if($countMatchDesc>0){
            $stringaOrdURL = "ord=desc".$stringaOrder."&";
            $stringaOrder = "ORDER BY ".$stringaOrder." DESC";
        }
        elseif ($countMatchAsc>0) {
            $stringaOrdURL = "ord=asc".$stringaOrder."&";
            $stringaOrder = "ORDER BY ".$stringaOrder." ASC";

        }
    }
    else {
        $stringaOrder = "";
    }

    if($countMatchAsc>0||$countMatchDesc>0||$countMatchNum>0){
        $pieces = explode("&", $stringaDiQuery);
        if(count($pieces)<=1){
            $stringaDiURL = "lista_gioielli.php?";
        }
        else{
            if($countMatchTipo>0)
                $stringaDiURL = "lista_gioielli.php?".$pieces[0]."&amp;";
            else {
                $stringaDiURL = "lista_gioielli.php?";
            }
        }

    }
    else {
        if($stringaDiQuery==""){
            $stringaDiURL = "lista_gioielli.php?";
        }
        else{
            $stringaDiURL = "lista_gioielli.php?".$stringaDiQuery."&amp;";
        }
    }
    echo '<div id="cacciatore" class="divZoom">
      <span id="chiusura">&times;</span>
      <img src="img/broken.jpg" class="contenutoImmagine" id="immagineCacciata" alt="Immagine di default"/>
      <div id="altImmagine"></div>
    </div>';
    echo "<div id ='menuListaGioielli'><a href='#menuOrdinamentoGioielli' class='salto'>Salta il menu di selezione tipo gioiello e vai al menu d'ordinamento</a>
    <a href='#menu' class='salto'>Ritorna al menu</a>
            <ul id='menuGioielli'>";
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
              if(empty($_GET["tipo"])){
                echo"
                <li class='ElementoMenuGioiello active'><a href=\"lista_gioielli.php\" class=\"active\">Tutti</a></li>
                <li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=anello\">Anelli</a></li>
                <li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=bracciale\">Bracciali</a></li>
                <li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=collana\">Collane</a></li>
                <li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=orecchino\">Orecchini</a></li>";
                if($sessioneAperta==true)
                    echo "<li class='ElementoMenuGioiello'><a href='lista_gioielli.php?tipo=esclusive'>Esclusive</a></li>";
              }
              else{
                echo"<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php\">Tutti</a></li>";
                if($_GET["tipo"] == "anello"){
                  echo "<li class='ElementoMenuGioiello active' ><span class=\"active\">Anelli</span></li>";
                }
                else{
                  echo "<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=anello\">Anelli</a></li>";
                }
                if($_GET["tipo"] == "bracciale"){
                  echo"<li class='ElementoMenuGioiello active'><span class=\"active\">Bracciali</span></li>";
                }
                else{
                  echo"<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=bracciale\">Bracciali</a></li>";
                }
                if($_GET["tipo"] == "collana"){
                  echo"<li class='ElementoMenuGioiello active'><span class=\"active\">Collane</span></li>";
                }
                else{
                  echo"<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=collana\">Collane</a></li>";
                }
                if($_GET["tipo"] == "orecchino"){
                  echo"<li class='ElementoMenuGioiello  active'><span class=\"active\">Orecchini</span></li>";
                }
                else{
                  echo"<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=orecchino\" >Orecchini</a></li>";
                }
                if($sessioneAperta==true){
                    if($_GET["tipo"] == "esclusive"){
                      echo"<li class='ElementoMenuGioiello active'><span class=\"active\">Esclusive</span></li>";
                    }
                    else{
                      echo"<li class='ElementoMenuGioiello'><a href=\"lista_gioielli.php?tipo=esclusive\" >Esclusive</a></li>";
                    }
                }

              }
            }
            echo"</ul>
            </div>
            <div id ='menuOrdinamentoGioielli'>
            <a href='#listaPaginaGioielli' class='salto'>Salta il menu d'ordinamento e vai alla lista dei gioielli</a>
            <a href='#menu' class='salto'>Ritorna al menu</a>
            <ul id='menuOrdinamento'>
            <li class='ElementoOrdinamentoGioiello'>
            Nome <a href='".$stringaDiURL."ord=ascNm'><abbr title='ordina dalla A alla Z'>A-Z</abbr></a><a href='".$stringaDiURL."ord=descNm'><abbr title='ordina dalla Z alla A'>Z-A</abbr></a>
            </li><li class='ElementoOrdinamentoGioiello'>
            Dimensione <a href='".$stringaDiURL."ord=ascDim'><abbr title='ordina dal più grande al più piccolo'>+ &#8594; -</abbr></a><a href='".$stringaDiURL."ord=descDim'><abbr title='ordina dal più piccolo al più grande'>- &#8594; +</abbr></a>
            </li><li class='ElementoOrdinamentoGioiello'>
            Materiale <a href='".$stringaDiURL."ord=ascMat'><abbr title='ordina i gioielli in base al materiale dalla A alla Z'>A&#8594;Z</abbr></a><a href='".$stringaDiURL."ord=descMat'><abbr title='ordina i gioielli in base al materiale dalla Z alla A'>Z&#8594;A</abbr></a>
            </li>
            </ul>
            </div>";

            $selectI = array("gioielli.Id","gioielli.Nome as Nm",
            "gioielli.Immagini","gioielli.Descrizione",
            "dimensione.Grandezza as Dim","prodotto.Nome_Materiale as Mat, gioielli.Esclusiva as GES");

            $fromI = array("gioielli","taglia","dimensione", "prodotto");

            $whereI = array("prodotto.Gioiello=gioielli.Id",
            "AND", "gioielli.Id = taglia.Gioiello", "AND",
            "taglia.Dimensione = dimensione.Codice");

    $tipo = "";
    $limite1 = 0;
    $limite2 = 30;
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if(!empty($_GET["numero"])){
                $limite1 = ($_GET["numero"]-1)*30;
        }

        if (!empty($_GET["tipo"])) {
            $stringaOrdURL= $tipo = "tipo=".$_GET["tipo"]."&".$stringaOrdURL;

            if($_GET["tipo"]=='esclusive'){
                if($sessioneAperta==true)
                    if($admin==0){
                        array_push($fromI,"esclusivagioiello");
                        array_push($whereI,"AND","esclusivagioiello.Utente = '".$email."'", "AND",
                        "esclusivagioiello.Gioiello = gioielli.Id", "AND", "gioielli.Esclusiva = 1");
                    }
                    array_push($whereI,"AND","gioielli.Esclusiva = 1");
            }
            else{
                if($sessioneAperta==true)
                if($admin==0){
                    array_push($whereI,"AND","Esclusiva = 0");
                }
                array_push($whereI,"AND","dimensione.Tipologia ="."'".$_GET["tipo"]."' ");
            }

            $Select = new SelectInterrogation($selectI,$fromI,$whereI);
        }
        else {
            if($sessioneAperta==true){
                if(strlen($stringaOrder)==0)
                    $stringaOrder = " ORDER BY GES DESC";
                if($admin==0){
                        array_push($whereI,"AND","Esclusiva = 0");
                        array_push($whereI," UNION SELECT gioielli.Id,gioielli.Nome as Nm,
                        gioielli.Immagini,gioielli.Descrizione,
                        dimensione.Grandezza as Dim,prodotto.Nome_Materiale as Mat
                        , gioielli.Esclusiva as GES
                        FROM gioielli,esclusivagioiello,
                        taglia, dimensione, prodotto
                        WHERE esclusivagioiello.Utente = '".$email."' AND
                        esclusivagioiello.Gioiello = gioielli.Id AND gioielli.Esclusiva = 1
                        AND prodotto.Gioiello=gioielli.Id AND gioielli.Id = taglia.Gioiello
                        AND taglia.Dimensione = dimensione.Codice");
                    }
                }
                else {
                    array_push($whereI,"AND","Esclusiva = 0");
                }
            }


            $Select = new SelectInterrogation($selectI,$fromI,$whereI);

        }

        try {
            $arrayRisultato = $Select->interrogation();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    $risultato = $arrayRisultato->num_rows;
    $arrayRisultato->free();

    array_push($whereI,$stringaOrder." LIMIT ".$limite1.",".$limite2);
    $Select = new SelectInterrogation($selectI,$fromI,$whereI);
    try {
        $arrayRisultato = $Select->interrogation();
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }



    echo '<div id="listaPaginaGioielli">
    <a href=\'#divNFP\' class=\'salto\'>Salta la lista di gioielli e vai ai numeri di cambio pagina</a>
    <a href="#menu" class="salto">Ritorna al menu</a>
      ';

      $adminString1 = "";
      $adminString2 = "";

    if($arrayRisultato->num_rows > 0){

        if($sessioneAperta==true&&$admin==1){
            echo "<form action='modificaEffettuata.php' method='post'>";
        }
        echo'
        <ul id="listaGioielli">';
        while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){

            if($sessioneAperta==true){
                if($admin==1){
                    $adminString1 = "
                    <div>
                        <button type='submit' name='modificaGiol' class='modificaGiol' value='".$row['Id']."'>Modifica</button>
                        <label for='eliminaGiol".$row['Id']."'><input id='eliminaGiol".$row['Id']."' type='checkbox' name='eliminaGiol[]' value='".$row['Id']."'/>Elimina ".$row['Id']."</label>
                    </div>";
                    $adminString2 = "
                    <div id='eliminaDiv' >
                        <label for='eliminaG' id='eliminaLabel'>
                        <button type='submit' name='eliminaG' id='eliminaG' value='elimina'>Elimina</button></label>
                    </div>
                    </form>";
                }
                if($row['GES']==1)
                    echo "<li class='elementoGioielli Esclusiva'><h2><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nm']."</a></h2>
                    <a href=\"gioiello.php?id=".$row['Id']."\" class='linkDisabilitato'><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/><span class='nascosto'>Esclusiva</span></a>
                    ".$adminString1."
                    </li>";
                else
                    echo "<li class='elementoGioielli'><h2><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nm']."</a></h2>
                    <a href=\"gioiello.php?id=".$row['Id']."\" class='linkDisabilitato'><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/></a>
                    ".$adminString1."
                    </li>";
            }
            else{
                echo "<li class='elementoGioielli'>";

            echo "
            <h2><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nm']."</a></h2>
            <a href=\"gioiello.php?id=".$row['Id']."\" class='linkDisabilitato'><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/></a>
            </li>";
            }
        }
        echo "</ul>".$adminString2;

    }

    $arrayRisultato->free();
    if(intval($risultato/30) == 0)
        $ri = 1;
    else
        if($risultato%30>0){
            $ri = intval($risultato/30) +1;
        }
        else {
            $ri = $risutato/30;
        }
    echo "<div id='divNFP'>
      <a href='#footer' class='salto'>Salta la lista di gioielli e vai al <span xml:lang='en'>footer</span></a>
      <a href='#menu' class='salto'>Ritorna al menu</a>
    <ul id='numeriFondoPagina'>";
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
    for ($i=0; $i < intval($ri); $i++) {
        if(empty($_GET["numero"])){
          if($i+1==1){
              echo"<li class='elementoNumeroGioiello active'>
              <span class='active'>".($i+1)."</span>
              </li>";
          }
          else{
              echo"<li class='elementoNumeroGioiello'>
              <a href=\"lista_gioielli.php?".$stringaOrdURL."numero=".($i+1)."\">".($i+1)."</a>
              </li>";
          }
        }
        else{
          if($_GET["numero"]==($i+1)){
            echo"<li class='elementoNumeroGioiello active'>
            <span class='active'>".($i+1)."</span>
            </li>";
          }else{
            echo"<li class='elementoNumeroGioiello'>
            <a href=\"lista_gioielli.php?".$stringaOrdURL."numero=".($i+1)."\">".($i+1)."</a>
            </li>";
          }
        }

        }

    }
    echo"</ul></div></div>";

    } catch (Exception $e) {
            echo $e->getMessage();
    }

    require_once('Parti/footer.php');
    echo printfooter($sessioneAperta);
    ?>
