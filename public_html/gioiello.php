<?php

session_start();

  require_once("Parti/stampaHTML.php");
  require_once('../php/SelectInterrogation.php');
  $a1 = "Gioiello - AL.JO. Gioielli Center";

  if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if(!empty($_GET["id"])){
          $ar1 = array("gioielli.Nome as GN");
          $ar2 = array("gioielli");
          $ar3 = array("gioielli.Id ='".$_GET['id']."'");
          $Select = new SelectInterrogation($ar1,$ar2,$ar3);
          $risultato = "";

          try {
            $arrayR = $Select->interrogation();
          } catch (Exception $e) {
            throw new Exception($e->getMessage());

          }

          if($arrayR->num_rows > 0){
           while($row = $arrayR->fetch_array(MYSQLI_ASSOC)){
                $risultato= $row['GN'];
           }
          }

         $a1 = "Gioiello ".$risultato." - AL.JO. Gioielli Center";
        }
  }
          $a2 = "Pagina che contiene un gioiello in dettaglio";
          $a3 = "Gioiello, AL.JO., Collana, Bracciale, Anello, Vicenza";
          $a4 = "index,follow";
  echo printHeadHTML($a1,$a2,$a3,$a4);


  $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
         <li class="active superActive"><a href="lista_gioielli.php" class="active">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>';

  $sessioneAperta = false;
  $sezioneLogin = "";
  $admin =0;


  if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
        $sessioneAperta = true;
        $sezioneLogin =$_SESSION['username'];
        $email = $_SESSION['email'];
        $admin = $_SESSION['admin'];
  }

  if($sessioneAperta== true){
      if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
  }

  $getId = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
      if(empty($_GET["id"])){
        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt;
        <a href="lista_gioielli.php">Lista Gioielli</a> &gt; Gioiello </p>';
        echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);
        echo "<div id='contenitoreGioiello'>
        <div id='titoloGioiello'>
                   <h1>Gioiello non trovato</h1>
                   <p>Il Gioiello ricercato non è stato trovato</p>
             </div>
        </div>";
      }
      else{

try {

$a1 = array("gioielli.Id as GI","gioielli.Nome as GN","gioielli.Peso as GP",
"gioielli.Descrizione as GD","gioielli.Sesso ","gioielli.Parour",
"gioielli.Immagini","gioielli.Collezione","pietra.Nome as PN",
"pietra.Colore as PC", "materiale.Nome as MN", "materiale.Colore as MC",
"dimensione.Tipologia as DT", "dimensione.Grandezza as DG",
"dimensione.UnitaDiMisura as DU, gioielli.Esclusiva as GES");

$a2 = array("gioielli","pietra","ornamento","prodotto","materiale","dimensione","taglia");

$a3 = array("gioielli.Id ="."'".$_GET['id']."'",
"AND","pietra.Nome = ornamento.Nome_Pietra ","AND", "pietra.Colore = ornamento.Colore_Pietra",
"AND", "ornamento.Gioiello = gioielli.Id", "AND", "taglia.Gioiello = gioielli.Id", "AND",
"taglia.Dimensione =dimensione.Codice", "AND",  "materiale.Nome= prodotto.Nome_Materiale",
"AND", "materiale.Colore = prodotto.Colore_Materiale", "AND", "prodotto.Gioiello = gioielli.Id");




$CountPtr = 0;

$question = new SelectInterrogation(array("Esclusiva"),$a2,$a3);
try{
  $ARisultato = $question->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
$risultatoEsclusiva = 0;
if($ARisultato->num_rows > 0){
  $CountPtr = 1;
 while($row = $ARisultato->fetch_array(MYSQLI_ASSOC)){
      $risultatoEsclusiva= $row['Esclusiva'];
 }
}



if($CountPtr==0){
  $a1 = array("gioielli.Id as GI","gioielli.Nome as GN","gioielli.Peso as GP",
  "gioielli.Descrizione as GD","gioielli.Sesso ","gioielli.Parour",
  "gioielli.Immagini","gioielli.Collezione", "materiale.Nome as MN", "materiale.Colore as MC",
  "dimensione.Tipologia as DT", "dimensione.Grandezza as DG",
  "dimensione.UnitaDiMisura as DU, gioielli.Esclusiva as GES");

  $a2 = array("gioielli","prodotto","materiale","dimensione","taglia");

  $a3 = array("gioielli.Id ="."'".$_GET['id']."'",
  "AND", "taglia.Gioiello = gioielli.Id", "AND",
  "taglia.Dimensione =dimensione.Codice", "AND",  "materiale.Nome= prodotto.Nome_Materiale",
  "AND", "materiale.Colore = prodotto.Colore_Materiale", "AND", "prodotto.Gioiello = gioielli.Id");

  $question = new SelectInterrogation(array("Esclusiva"),$a2,$a3);
}


$vincolo = "";

if($sessioneAperta==true){
  array_push($a3,"AND","gioielli.Esclusiva =".$risultatoEsclusiva);
  if($risultatoEsclusiva==1){
  if($admin==0){
    array_push($a2,"esclusivagioiello");
    array_push($a3,"AND","gioielli.Id = esclusivagioiello.Gioiello");
    array_push($a3,"AND","esclusivagioiello.Utente ='".$email."'");

  }
  }
  if($admin==0){
    $vincolo = "AND (gioielli.Esclusiva = 0 OR gioielli.Id  in (SELECT Gioiello FROM esclusivagioiello  WHERE esclusivagioiello.Utente = '".$email."')) ";
  }
}else {
  array_push($a3,"AND","gioielli.Esclusiva = 0");
}

$Select = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato = $Select->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
$collection = "";
$parour = "";
$tipology = "";
$material = "";
$pietra = "";
$countG=0;


$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt;
<a href="lista_gioielli.php">Lista gioielli</a> &gt; Gioiello ';
if($arrayRisultato->num_rows > 0){
$countG=1;
 while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
        $breadCrumb = $breadCrumb.$row['GN'].'</p>';
        echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);
      echo '<div id="cacciatore" class="divZoom">
        <span id="chiusura">&times;</span>
        <img src="img/broken.jpg" class="contenutoImmagine" id="immagineCacciata" alt="Immagine di default"/>
        <div id="altImmagine"></div>
      </div>';
      echo "<div id='titolo'>
      <h1>".$row['GN']." - ".$row['DT']."</h1>
      </div>
      <div id='immagineCaratteristiche'>
      <a href='#descrizione' class='salto'>Salta l'immagine e le caratteristiche del gioiello e vai alla descrizione</a>
      <a href='#menu' class='salto'>Ritorna al menu</a>
      <div id='immagine'>
      <img src='".$row['Immagini']."' alt=\"".$row['GD']."\" height='260px' width='260px' class='immaginePreda'/>
      </div>
      <div id ='caratteristiche'>
      <h2>Caratteristiche</h2>
      <ul>
      <li>Materiale : ".$row['MN']."</li>
      <li>Colore : ".$row['MC']."</li>
      <li>Dimensione : ".$row['DG']."".$row['DU']." </li>
      <li>Peso : ".$row['GP']." <abbr title='grammi'>gr</abbr> </li>";
      if($CountPtr>0){
        echo"<li>Pietre : ".$row['PN']." ".$row['PC']."</li>";
      }
      echo"<li>Sesso : ".$row['Sesso']." </li>
      </ul>
      </div>
      </div>
      <div id='descrizione'>
      <a href='#gioielloCollezione' class='salto'>Salta la descrizione del gioiello e vai alla lista dei gioielli della stessa collezione</a>
      <a href='#menu' class='salto'>Ritorna al menu</a>
      <h2>Descrizione</h2>
      <p>".$row['GD']."</p>
      </div>";
      $collection = $row['Collezione'];
      $parour = $row['Parour'];
      $tipology = $row['DT'];
      $material = $row['MN'];
      if($CountPtr>0)
        $pietra = $row['PN'];
 }
}
else {
  $breadCrumb = $breadCrumb."</p>";
    echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);
      echo "<div id='contenitoreGioiello'>
      <div id='titoloGioiello'>
                 <h1>Gioiello non trovato</h1>
                 <p>Il Gioiello ricercato non è stato trovato</p>
           </div>
      </div>";
}
echo "";
$arrayRisultato->free();

if($countG>0){
if($sessioneAperta == true)
  $Select2 = new SelectInterrogation(array("Id","Nome","Immagini","collezione","Descrizione","Esclusiva"),array("gioielli"),array("gioielli.Id <> "."'".$_GET['id']."'"."","AND","collezione IN (SELECT gioielli.Collezione FROM gioielli WHERE gioielli.Id ="."'".$_GET['id']."'".") ".$vincolo." ORDER BY RAND() LIMIT 5") );
else
  $Select2 = new SelectInterrogation(array("Id","Nome","Immagini","collezione","Descrizione","Esclusiva"),array("gioielli"),array("Esclusiva=0","AND","gioielli.Id <> "."'".$_GET['id']."'"."","AND","collezione IN (SELECT gioielli.Collezione FROM gioielli WHERE gioielli.Id ="."'".$_GET['id']."'".") ORDER BY RAND() LIMIT 5") );
try{
$arrayRisultato2 = $Select2->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo '<div id="gioielloCollezione">';
if($arrayRisultato2->num_rows > 0){
      echo '
      <a href=\'#gioielloparour\' class=\'salto\'>Salta la lista dei gioielli della stessa collezione e vai alla lista dei gioielli con la stessa <span xml:lang="fr">parure</span></a>
      <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
      <h2>Collezione : <a href="collezione.php?name='.$collection.'">'.$collection.'</a></h2>
      <ul id="collezione">';
 while($row = $arrayRisultato2->fetch_array(MYSQLI_ASSOC)){
   if($row['Esclusiva'] == 0)echo"<li class='elementoGioielliCollezione'>";
   else echo"<li class='elementoGioielliCollezione Esclusiva'>";
       echo "
       <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3><a href=\"gioiello.php?id=".$row['Id']."\">";

       if($row['Esclusiva'] == 0)echo"<img src='".$row['Immagini']."' alt=\"".$row['Descrizione']. "\" height='260px' width='260px' />";
       else echo"<span class='nascosto'>Esclusiva</span><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']. "\" height='260px' width='260px' />";
       echo"</a>
       </li>";
 }
 echo "</ul>";
 }

echo "</div>";
$arrayRisultato2->free();

if($sessioneAperta == true)
  $Select3 = new SelectInterrogation(
    array("Id","Nome","Immagini","Descrizione","Parour","Esclusiva"),array("gioielli"),array("gioielli.Id <> "."'".$_GET['id']."'"."","AND","Parour IN (SELECT gioielli.Parour FROM gioielli WHERE gioielli.Id ="."'".$_GET['id']."'".") ".$vincolo." ORDER BY RAND() LIMIT 3") );
else
  $Select3 = new SelectInterrogation(
    array("Id","Nome","Immagini","Descrizione","Parour","Esclusiva"),array("gioielli"),array("Esclusiva=0","AND","gioielli.Id <> "."'".$_GET['id']."'"."","AND","Parour IN (SELECT gioielli.Parour FROM gioielli WHERE gioielli.Id ="."'".$_GET['id']."'".") ORDER BY RAND() LIMIT 3") );

try{
  $arrayRisultato3 = $Select3->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo '<div id="gioielloparour">';
if($arrayRisultato3->num_rows > 0){
      echo '
      <a href=\'#gioielloTipologia\' class=\'salto\'>Salta la lista dei gioielli con la stessa <span xml:lang="fr">parure</span> e vai alla lista dei gioielli con la stessa tipologia</a>
      <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
      <h2>Della stessa <span xml:lang="fr">Parure</span></h2>
      <ul id="parour">';
 while($row = $arrayRisultato3->fetch_array(MYSQLI_ASSOC)){
   if($row['Esclusiva'] == 0)echo"<li class='elementoGioielliParour'>";
   else echo"<li class='elementoGioielliParour Esclusiva'>";
       echo "
       <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3>";if($row['Esclusiva'] == 0)echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a>";
       else echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a><span class='nascosto'>Esclusiva</span>";
       echo"
       </li>";
 }
 echo "</ul>";
 }

echo "</div>";
$arrayRisultato3->free();

if($sessioneAperta == true)
  $Select4 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "taglia","dimensione"),array("gioielli.Id <> "."'".$_GET['id']."'"."","AND","taglia.Dimensione =dimensione.Codice","AND","taglia.Gioiello = gioielli.Id","AND",
  "dimensione.Tipologia in (SELECT dimensione.Tipologia FROM gioielli, taglia,dimensione WHERE gioielli.Id = taglia.Gioiello AND taglia.Dimensione =dimensione.Codice AND gioielli.Id ="."'".$_GET['id']."'".") ".$vincolo." ORDER BY RAND() LIMIT 3"));
else
  $Select4 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "taglia","dimensione"),array("Esclusiva=0","AND","gioielli.Id <> "."'".$_GET['id']."'"."","AND","taglia.Dimensione =dimensione.Codice","AND","taglia.Gioiello = gioielli.Id","AND",
  "dimensione.Tipologia in (SELECT dimensione.Tipologia FROM gioielli, taglia,dimensione WHERE gioielli.Id = taglia.Gioiello AND taglia.Dimensione =dimensione.Codice AND gioielli.Id ="."'".$_GET['id']."'".") ORDER BY RAND() LIMIT 3"));

try{
  $arrayRisultato4 = $Select4->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo '<div id="gioielloTipologia">';
if($arrayRisultato4->num_rows > 0){
      echo '
      <a href=\'#gioielloMateriale\' class=\'salto\'>Salta la lista dei gioielli con la stessa tipologia e vai alla lista dei gioielli con lo stesso materiale</a>
      <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
      <h2>Della stessa Tipologia : '.$tipology.' </h2>
      <ul id="tipologia">';
 while($row = $arrayRisultato4->fetch_array(MYSQLI_ASSOC)){
   if($row['Esclusiva'] == 0) echo "<li class='elementoGioielliParour'>";
  else echo "<li class='elementoGioielliParour  Esclusiva'>";
  echo"
       <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3>";
       if($row['Esclusiva'] == 0) echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' />";
       else echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /><span class='nascosto'>Esclusiva</span>";
       echo"</a>
       </li>";
 }
 echo "</ul>";
 }

 echo "</div>";
 $arrayRisultato4->free();

if($sessioneAperta == true)$Select5 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "prodotto"),array("gioielli.Id <> "."'".$_GET['id']."'"."","AND",
"gioielli.Id = prodotto.Gioiello AND prodotto.Nome_Materiale in (SELECT prodotto.Nome_Materiale FROM prodotto WHERE prodotto.Gioiello ="."'".$_GET['id']."'".") ".$vincolo." ORDER BY RAND() LIMIT 3"));
else $Select5 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "prodotto"),array("Esclusiva=0","AND","gioielli.Id <> "."'".$_GET['id']."'"."","AND",
 "gioielli.Id = prodotto.Gioiello AND prodotto.Nome_Materiale in (SELECT prodotto.Nome_Materiale FROM prodotto WHERE prodotto.Gioiello ="."'".$_GET['id']."'".") ORDER BY RAND() LIMIT 3"));
try{
 $arrayRisultato5 = $Select5->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo '<div id="gioielloMateriale">';
 if($arrayRisultato5->num_rows > 0){
       echo '
       <a href=\'#gioielloPietra\' class=\'salto\'>Salta la lista dei gioielli con lo stesso materiale e vai alla lista dei gioielli con la stessa pietra</a>
       <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
       <h2>Dello stesso Materiale : '.$material.' </h2>
       <ul id="materiale">';
  while($row = $arrayRisultato5->fetch_array(MYSQLI_ASSOC)){
        if($row['Esclusiva'] == 0) echo "<li class='elementoGioielliParour'>";
        else echo "<li class='elementoGioielliParour Esclusiva'>";
        echo"
        <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3>";
        if($row['Esclusiva'] == 0)echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a>";
        else echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a><span class='nascosto'>Esclusiva</span>";
        echo"
        </li>";
  }
  echo "</ul>";
  }

  echo "</div>";
  $arrayRisultato5->free();
  echo '<div id="gioielloPietra">';
if($CountPtr>0){
if($sessioneAperta == true) $Select6 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "ornamento"),array("gioielli.Id <> "."'".$_GET['id']."'"."","AND","ornamento.Gioiello = gioielli.Id","AND",
  "ornamento.Nome_Pietra in (SELECT ornamento.Nome_Pietra FROM ornamento  WHERE ornamento.Gioiello ="."'".$_GET['id']."'".") ".$vincolo." ORDER BY RAND() LIMIT 3"));
else  $Select6 = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione","Esclusiva"),array("gioielli", "ornamento"),array("Esclusiva=0","AND","gioielli.Id <> "."'".$_GET['id']."'"."","AND","ornamento.Gioiello = gioielli.Id","AND",
  "ornamento.Nome_Pietra in (SELECT ornamento.Nome_Pietra FROM ornamento  WHERE ornamento.Gioiello ="."'".$_GET['id']."'".") ORDER BY RAND() LIMIT 3"));
try{
  $arrayRisultato6 = $Select6->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}

  if($arrayRisultato6->num_rows > 0){
        echo '
        <a href=\'#footer\' class=\'salto\'>Salta la lista dei gioielli con la stessa pietra e vai al <span xml:lang="en">footer</span></a>
        <a href=\'#menu\' class=\'salto\'>Ritorna al menu</a>
        <h2>Con la stessa pietra : '.$pietra.' </h2>
        <ul id="pietra">';
  while($row = $arrayRisultato6->fetch_array(MYSQLI_ASSOC)){
    if($row['Esclusiva'] == 0) echo "<li class='elementogioielliParour'>";
    else echo "<li class='elementogioielliParour  Esclusiva'>";
    echo"
         <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3>";
         if($row['Esclusiva'] == 0)echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a>";
         else echo"<a href=\"gioiello.php?id=".$row['Id']."\"><img src='..".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' /></a><span class='nascosto'>Esclusiva</span>";
         echo"
         </li>";
  }
  echo "</ul>";
  }


  $arrayRisultato6->free();
}
echo "</div>";
}
} catch (Exception $e) {
  echo $e->getMessage();
}
}
}

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);

 ?>
