<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/UpdateInterrogation.php');

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;
    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    $a1 = "Inserimento";
     if($sessioneAperta== true){

        if ($_SERVER["REQUEST_METHOD"] == "GET") {
          if(!empty($_GET["ins"])){
            if($_GET['ins']=='gioielli'){$a1=$a1." gioiello";}
            if($_GET['ins']=='collezioni'){$a1=$a1." collezione";}
            if ($_GET['ins']=='mat'){$a1=$a1." materiale";}
            if ($_GET['ins']=='ptr') {$a1=$a1." pietra";}
            if ($_GET['ins']=='dim') {$a1=$a1." dimensione";}
          }
        }
     }

    $a1 = $a1." - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';



    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; <a href="sezioneDedicata.php">Sezione Dedicata</a> &gt; Inserimento </p>';
    }




    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
    try{


    $interrogazioneS = "";
    $interrogazioneF = "";
    $interrogazioneW = "";

    if($sessioneAperta== true){

        if ($_SERVER["REQUEST_METHOD"] == "GET") {
          if(!empty($_GET["ins"])){
             if($_GET['ins']=='gioielli'){

                 echo '
                 <div id="Ins">
                 <a href="#footer" class="salto">Salta la form d\'inserimento Gioiello</a><a href="#menu" class="salto">Ritorna al menu</a>
                 <form enctype="multipart/form-data" action="inserimentoCompletato.php" method="post">
                 <fieldset>
                   <legend>Inserisci Gioiello</legend>
                   <div>
                    <label for="IdInsG">ID
                     <input type ="text" name="id" id="IdInsG" maxlength="20" class="defaultInputForm"/>
                    </label>
                    <span></span>
                   </div>
                   <div>
                   <label for="NomeInsG">Nome
                     <input type="text" name="nome" id= "NomeInsG" maxlength="20" class="defaultInputForm"/>
                     </label>
                     <span></span>
                   </div>
                    <div>
                    <label for="MassaModGIns">Peso
                      <input type="text" name="PesoInsG" id="MassaModGIns" class="defaultInputForm"/><abbr title="grammi">gr</abbr>
                      </label>
                      <span></span>
                    </div>
                     <div>
                     <label for="DescrizioneInsG">Descrizione
                       <textarea name="descrizione" id ="DescrizioneInsG" rows="5" cols="40" class="defaultInputForm"></textarea>
                       </label>
                       <span></span>
                     </div>
                     <div>
                     Sesso
                       <label for="SessoInsGU"><span><input type="radio" name="sesso" id="SessoInsGU" value="Unisex" checked="checked" />Unisex</span></label>
                       <label for="SessoInsGM"><span><input type="radio" name="sesso" id="SessoInsGM" value="Maschile" />Maschile</span></label>
                       <label for="SessoInsGF"><span><input type="radio" name="sesso" id="SessoInsGF" value="Femminile" />Femminile</span></label>

                       <span></span>
                     </div>
                     <div>
                     Esclusiva
                       <label for="EsclusivaInsGNo"><span><input type="radio" id="EsclusivaInsGNo" name="esclusiva" value="0" checked="checked" />No</span></label>
                       <label for="EsclusivaInsGSi"><span><input type="radio" id="EsclusivaInsGSi" name="esclusiva" value="1" />Si</span></label>

                       <span></span>
                     </div>
                     <div>
                     <label for="ImmagineInsG">Immagine
                       <input type="file" id="ImmagineInsG" name="immagine"   />
                       </label>
                       <span></span>
                       </div>
                        <div>
                        <label for="MaterialeInsG">Seleziona Materiale
                       <select name="Materiale" id="MaterialeInsG">';

                       $mInterrogation = new SelectInterrogation(array("Nome","Colore"),array("materiale"),array("1"));
                       try{
                       $arrayInterrogationm = $mInterrogation->interrogation();

                    } catch (Exception $e) {
                        throw new Exception($e->getMessage());

                    }

                       if($arrayInterrogationm->num_rows > 0){
                             while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                 echo '<option value="'.$row['Nome'].'@'.$row['Colore'].'">'.$row['Nome'].'-'.$row['Colore'].'</option>';
                             }
                       }

                       $arrayInterrogationm->free();




                    echo '
                       </select></label><span></span> oppure
                       <a href="inserimenti.php?ins=mat">Inserisci Materiale</a></div>
                       <div><label for="DimensioneInsG">Seleziona Dimensione
                       <select name="Dimensione" id="DimensioneInsG">';

                       $mInterrogation = new SelectInterrogation(array("Codice","Tipologia","Grandezza","UnitaDiMisura"),
                       array("dimensione"),array("dimensione.Codice not in (SELECT pietra.Dimensione FROM pietra )",
                       "AND","dimensione.Tipologia <> 'Pietra'"));

                       try{

                       $arrayInterrogationm = $mInterrogation->interrogation();

                   } catch (Exception $e) {
                        throw new Exception($e->getMessage());

                    }
                       if($arrayInterrogationm->num_rows > 0){
                             while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                 echo '<option value="'.$row['Codice'].'">'.$row['Tipologia'].' '.$row['Grandezza'].$row['UnitaDiMisura'].'</option>';
                             }
                       }


                       $arrayInterrogationm->free();

                       echo '</select></label><span></span> oppure
                       <a href="inserimenti.php?ins=dim">Inserisci Dimensione</a></div><div>
                       <label for="PietraInsG">Seleziona Pietra
                       <select name="Pietra" id="PietraInsG">
                        <option value="NULL" selected="selected" class="italic">NULL</option>';

                       $mInterrogation = new SelectInterrogation(array("Nome","Colore","Tipologia","Grandezza","UnitaDiMisura"),
                       array("pietra","dimensione"),array("pietra.Dimensione = dimensione.Codice"));

                       try{
                       $arrayInterrogationm = $mInterrogation->interrogation();
                   } catch (Exception $e) {
           throw new Exception($e->getMessage());

         }
                       if($arrayInterrogationm->num_rows > 0){
                             while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                 echo '<option value="'.$row['Nome'].'@'.$row['Colore'].'">'.$row['Nome'].' '.$row['Colore'].'</option>';
                             }
                       }
                       $arrayInterrogationm->free();

                       echo '</select></label><span></span> oppure
                       <a href="inserimenti.php?ins=ptr">Inserisci Pietra</a></div><div>
                       <label for="CollezioneInsG">Seleziona Collezione
                       <select name="Collezione" id="CollezioneInsG">
                       <option value="NULL" selected ="selected" class="italic">NULL</option>';

                       $mInterrogation = new SelectInterrogation(array("Nome","Esclusiva"),
                       array("collezione"),array("1"));
                       try{
                       $arrayInterrogationm = $mInterrogation->interrogation();
                   } catch (Exception $e) {
           throw new Exception($e->getMessage());

         }
                       if($arrayInterrogationm->num_rows > 0){
                             while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                 if($row['Esclusiva']==0)
                                    echo '<option value="'.$row['Nome'].'">'.$row['Nome'].' Pubblica</option>';
                                else {
                                    echo '<option value="'.$row['Nome'].'">'.$row['Nome'].' In Esclusiva</option>';
                                }
                             }
                       }
                       $arrayInterrogationm->free();

                        echo '
                       </select></label><span></span> oppure <a href="inserimenti.php?ins=collezioni">Inserisci Collezione</a></div>

                       <div>
                       Inserisci nuova
                       <label for="ParourModG"><span xml:lang="fr">Parure</span> Gioiello<input type="text" name="parourTesto" id="ParourModG" value=""/></label>
                       <span id="break">
                       oppure seleziona</span>
                       <label for="parourSelezione">
                       <select name ="Parour" id="parourSelezione">
                       <option value="NULL" selected ="selected" class="italic">NULL</option>';


                       $mInterrogation = new SelectInterrogation(array("DISTINCT Parour"),
                       array("gioielli"),array("1"));
                       try{
                       $arrayInterrogationm = $mInterrogation->interrogation();
                   } catch (Exception $e) {
           throw new Exception($e->getMessage());

         }

                       if($arrayInterrogationm->num_rows > 0){
                             while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                 if(!$row['Parour']=="")
                                    echo '<option value="'.$row['Parour'].'">'.$row['Parour'].'</option>';
                             }
                       }

                       $arrayInterrogationm->free();

                       echo '</select></label><span></span></div><div>
                       <input type="submit" name ="submitInsG" value="Inserisci" id="submitInsG" />
                       <input type="reset" id="resetInsG" value="Reset"/></div>
                 </fieldset>
               </form>';
             }
             elseif ($_GET['ins']=='collezioni') {
                 echo '
                 <div id="Ins"><a href="#footer" class="salto">Salta la form d\'inserimento Collezione</a><a href="#menu" class="salto">Ritorna al menu</a>
                 <form action="inserimentoCompletato.php" method="post">
                 <fieldset>
                   <legend>Inserisci Collezione</legend>
                   <div>
                   <label for="nomeCollezione">Nome
                     <input type ="text" name="nomeCollezione" id="nomeCollezione" class="defaultInputForm" maxlength="20"/>
                     </label><span></span>
                   </div>
                     <div>
                     Esclusiva
                       <label for="esclusivaCollezioneNo"><span><input type="radio" name="esclusivaCollezione" value="0" id="esclusivaCollezioneNo" checked="checked" />No</span></label>
                       <label for="esclusivaCollezioneSi"><span><input type="radio" name="esclusivaCollezione" value="1" id="esclusivaCollezioneSi" />Si</span></label>

                     </div>
                     <div>
                     <label for="descrizioneCollezione">Descrizione
                       <textarea name="descrizioneCollezione" id="descrizioneCollezione" rows="5" cols="40" class="defaultInputForm"></textarea>

                       </label><span></span>
                     </div>
                     <div>
                     <label for="immagineCollezione">Immagine
                       <input type="file" name="immagineCollezione"  id="immagineCollezione" />
                       </label>
                       </div>
                       <input type="submit" value="Inserisci" id="submitInsC" name = "submitInsC"/>
                       <input type="reset" id="resetInsC" value="Reset"/>
                 </fieldset>
               </form>';

             }
             elseif ($_GET['ins']=='mat') {
                 echo '<div  id="Ins"><a href="#footer" class="salto">Salta la form d\'inserimento Materiale</a><a href="#menu" class="salto">Ritorna al menu</a>
                 <form action="inserimentoCompletato.php" method="post">
                 <fieldset>
                   <legend>Inserisci Materiale</legend>
                   <div>
                   <label for="nomeMateriale">Nome
                      <input type ="text" name="nomeMateriale" id="nomeMateriale" maxlength="30" class="defaultInputForm"/>
                 </label><span></span>
                   </div>
                     <div>
                     <label for="coloreMateriale">Colore
                         <input type ="text" name="coloreMateriale" id="coloreMateriale" maxlength="20" class="defaultInputForm"/>
                         </label><span></span>
                     </div>
                       <input type="submit" value="Inserisci" id="submitInsM" name ="submitInsM" />
                       <input type="reset" id="resetInsM" value="Reset"/>
                 </fieldset>
               </form>';

             }
             elseif ($_GET['ins']=='ptr') {
                 echo '<div id="Ins"><a href="#footer" class="salto">Salta la form d\'inserimento Pietra</a><a href="#menu" class="salto">Ritorna al menu</a>
                 <form action="inserimentoCompletato.php" method="post">
                 <fieldset>
                   <legend>Inserisci Pietra</legend>
                   <div>
                     <label for="nomePietra">Nome
                       <input type ="text" name="nomePietra" id="nomePietra" maxlength="30" class="defaultInputForm"/></label><span></span>
                   </div>
                   <div>
                   <label for="colorePietra">Colore
                     <input type ="text" name="colorePietra" id="colorePietra" maxlength="20" class="defaultInputForm"/>
                     </label><span></span>
                   </div>
                   <div>
                   <label for="dimensionePietra">Dimensione
                   <select name="dimensionePietra" id="dimensionePietra">
                         ';

                         $mInterrogation = new SelectInterrogation(array("Codice","Tipologia","Grandezza","UnitaDiMisura"),
                         array("dimensione"),array("dimensione.Codice not in (SELECT taglia.Dimensione FROM taglia )",
                         "AND","dimensione.Tipologia = 'Pietra'"));
                         try{
                         $arrayInterrogationm = $mInterrogation->interrogation();
                     } catch (Exception $e) {
             throw new Exception($e->getMessage());

           }
                         if($arrayInterrogationm->num_rows > 0){
                               while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                                   echo '<option value="'.$row['Codice'].'">'.$row['Tipologia'].'-'.$row['Grandezza'].$row['UnitaDiMisura'].'</option>';
                               }
                         }

                         $arrayInterrogationm->free();

                         echo '</select></label>
                         <a href="inserimenti.php?ins=dim">Inserisci Dimensione</a>
                         </div>

                       <input type="submit" value="Inserisci" id="submitInsP" name ="submitInsP" />
                       <input type="reset" id="resetInsP" value="Reset"/>
                 </fieldset>
               </form>';

             }
             elseif ($_GET['ins']=='dim') {
                 echo '
                 <div id="Ins"><a href="#footer" class="salto">Salta la form d\'inserimento Dimensione</a><a href="#menu" class="salto">Ritorna al menu</a>
                 <form action="inserimentoCompletato.php" method="post">
                 <fieldset>
                   <legend>Inserisci Dimensione</legend>
                   <div>
                   <label for="nomeDimensione">Nome
                     <input type ="text" name="nomeDimensione" id="nomeDimensione" maxlength="20" class="defaultInputForm"/>
                     </label><span></span>
                   </div>
                     <div>
                     <label for="udm">Unita di Misura
                        <input type ="text" name="udm" id="udm" maxlength="20" class="defaultInputForm"/>
                      </label><span></span>
                     </div>
                     <div>
                     <label for="grandezzaDim">Grandezza
                         <input type="text" name="grandezzaDim" class="defaultInputForm"
                             id = "grandezzaDim" />
                             </label><span></span>
                     </div>
                     <div>
                     Tipologia
                     <span></span>
                       <label for="tipologiaDimAnello"><span><input type="radio" name="tipologiaDim" id = "tipologiaDimAnello" class="tipDim" value="Anello" checked="checked" />Anello</span></label>
                       <label for="tipologiaDimCollana"><span><input type="radio" name="tipologiaDim" id = "tipologiaDimCollana" class="tipDim" value="Collana" />Collana</span></label>
                       <label for="tipologiaDimBraccial"><span><input type="radio" name="tipologiaDim" id = "tipologiaDimBraccial" class="tipDim" value="Bracciale" />Bracciale</span></label>
                       <label for="tipologiaDimOrecchino"><span><input type="radio" name="tipologiaDim" id = "tipologiaDimOrecchino" class="tipDim" value="Orecchino" />Orecchino</span></label>
                       <label for="tipologiaDimPietra"><span><input type="radio" name="tipologiaDim" id = "tipologiaDimPietra" class="tipDim" value="Pietra" />Pietra</span></label>

                     </div>

                       <input type="submit" value="Inserisci" id="submitInsD" name = "submitInsD" />
                       <input type="reset" id="resetInsD" value="Reset"/>
                 </fieldset>
               </form>';

             }
      }
      echo"</div>";
    }
}


} catch (Exception $e) {
     echo $e->getMessage();

 }
require_once("Parti/footer.php");
echo printfooter($sessioneAperta);
?>
