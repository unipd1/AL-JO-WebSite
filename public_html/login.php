<?php
session_start();

require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');


$a1 = "Esito login - AL.JO. Gioielli Center";
$a2 = "Pagina di avvenuto login oppure di errore";
$a3 = "redirect, login, AL.jo.";
$a4 = "noindex,nofollow";
$sessioneAperta= false;
echo printHeadHTML($a1,$a2,$a3,$a4);

$sessioneAperta = "";

$menu = '<li><a href="index.php" xml:lang="en" >Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>';
$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Esito <span xml:lang="en">login</span></p>';
try{
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    if(isset($_POST["emailL"])) $email=$_POST["emailL"];
    if(isset($_POST["email"])) $email=$_POST["email"];
    if(isset($_POST["emailAmministratore"])) $email=$_POST["emailAmministratore"];
      if(isset($email)&&isset($_POST["password"])){
          $bol = true;
          $error = "general";
          if(preg_match("/^([\w\-\+\.]+)@([\w\-\+\.]+).([\w\-\+\.]+)$/", $email) == 0){
              $bol = false;
              $error = "email";

            }
            if(preg_match("/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/", $_POST["password"]) == 0 && $bol){
              $bol = false;
              $error = "password";
              echo "<p>Errore in <span xml:lang='en'>Password</span>, la <span xml:lang='en'>password</span> deve essere composta da 8 caraterri, una lettera maiuscola e un numero.</p>Verrai reindirizzato a <a href='index.php'>Home</a></p>";
            }
            $a1 = array("Nome","Email","Password","Admin");
            $a2 = array("utente");
            $a3 = array("Email ='".$email."'","AND","Password ='".$_POST['password']."'");

            $select = new SelectInterrogation($a1,$a2,$a3);

            try {
                $risultatoUtente = $select->interrogation();
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
            session_set_cookie_params(86400);
            if($risultatoUtente->num_rows > 0 && $bol==true){

              while($row = $risultatoUtente->fetch_array(MYSQLI_ASSOC)){
                if($row['Email'] == $email && $row['Password'] == $_POST['password']){
                    $_SESSION['username'] = $row['Nome'];
                    $_SESSION['email'] = $row['Email'];
                    $_SESSION['password'] = $row['Password'];
                    $_SESSION['admin'] = $row['Admin'];
                    if($_SESSION['admin'] == 1) $menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';

                    $sessioneAperta = true;
                    $sezioneLogin =$_SESSION['username'];
                    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
                    echo '<p>
                    Accesso effettuato: vai all\'<a href="index.php">home</a>.
                    </p>';
                }

              }
            }
            else{

              $sessioneAperta = false;
              $sezioneLogin ="";
              echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
              if($error=="general")
              echo '<p>
              Accesso non effettuato: <span xml:ang="en">E-mail</span> o <span xml:ang="en">password</span> errate. Vai all\'<a href="index.php">home</a>.
              </p>';
              if($error=="email")  echo '<p>
                Accesso non effettuato: <span xml:lang="en">E-mail</span> non ben formata. Vai all\'<a href="index.php">home</a>.
                </p>';
              if($error=="password")   echo '<p>
                Accesso non effettuato:<span xml:lang="en">Password</span> errata, deve essere composta da 8 caraterri, una lettera maiuscola e un numero. Vai all\'<a href="index.php">home</a>.
                </p>';
            }

      }
      else {
        $sessioneAperta = false;
        $sezioneLogin ="";
        echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
        echo '<p>
        Accesso non effettuato: E-mail o password errate. Vai all\'<a href="index.php">home</a>.
        </p>';
      }
}
} catch (Exception $e) {
    echo $e->getMessage();
}
require_once('Parti/footer.php');
echo printfooter($sessioneAperta);

 ?>
