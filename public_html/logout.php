<?php
session_start();
session_unset();
session_destroy();
session_start();

require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "Esito logout - AL.JO. Gioielli Center";
$a2 = "Pagina di avvenuto lout oppure di errore";
$a3 = "redirect, logout, AL.jo.";
$a4 = "noindex,nofollow";
echo printHeadHTML($a1,$a2,$a3,$a4);

$menu = '<li class="active"><a href="index.php" class="active" xml:lang="en" >Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>';
$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Esito <span xml:lang="en">logout</span></p>';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;

if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $email = $_SESSION['email'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
  $sessioneAperta = true;
  $sezioneLogin =$_SESSION['username'];
  if($admin == 1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
  echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
  echo'<p>
  C\'è stato un errore durante il logout riprova
  </p>';
}
else{
  echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
  echo '<p>
Logout effettuato: Vai all\'<a href="index.php">home</a>.
  </p>';
}
require_once('Parti/footer.php');
echo printfooter($sessioneAperta);
?>
