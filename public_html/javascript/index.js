window.onload = function() {
    //header
    login();
    //index.html
    slider();
    //registrazione.html
    registrazioneControl();
    //login.html
    pageLoginControl();
    //loginAdmin.html
    adminLoginControl();
    /*informazioni.html*/
    informazioniNewTab();
    /*zoom delle immagini*/
    zoom();
    /*controla ricerca*/
    ricerca();
    /*Sezione Dedicata*/
    sezioneDedicata();
};
/*informazioni.html*/
function informazioniNewTab(){
  var link0 = document.getElementById("linkNewTab0");
  var link1 = document.getElementById("linkNewTab1");
  if(link0 !== null) link0.target="_blank";
  if(link1 !== null) link1.target="_blank";
}
//Funzione per settare gli onblur e gli onclick delle form
function setOn(input,example,value,Special, secpass){
  if(input !== null){
    input.value = example;
    if(Special === "password") input.onblur = function(){checkpass(input,value,example, secpass);};
    if(Special === "email") input.onblur = function(){checkemail(input,value,example);};
    if(Special === "other") input.onblur = function(){controllo(input,value,example);};
    input.onclick = function(){svuota(input,example);};
  }
}

//Funzione svuota form dal defoult
function svuota(input, example) {
    if (example === input.value && input.className === "defaultInputForm") {
        input.className = "";
        input.value = "";
    }
}
//funzione controllo form
function controllo(input, testo, defaultValue){
    var text = input.value;
    var padre = (input.parentNode).parentNode;
    if (padre.children[1].children[0]) {
        padre.children[1].removeChild(padre.children[1].children[0]);
    }
    if (text === "" || text === defaultValue) {
        var error = document.createElement("strong");
        error.innerHTML = "Tag '" + testo + "' vuoto";
        error.className = "erroreInputForm";
        padre.children[1].appendChild(error);
        return false;
    }
    return true;
}

//Controlla se l'email è scritta correttamente
function checkemail(input, testo, defaultValue) {
    var ret = controllo(input, testo, defaultValue);
    var padre = (input.parentNode).parentNode;
    var ragexp = /^([\w\-\+\.]+)@([\w\-\+\.]+).([\w\-\+\.]+)$/;
    if (input.value.search(ragexp) !== 0 && ret) {
        var error = document.createElement("strong");
        error.innerHTML = "Hai inserito una E-mail non valida.";
        error.className = "erroreInputForm";
        padre.children[1].appendChild(error);
        return ret && false;
    }
    return ret && true;
}
function passwordDiverse(pass,pass1){
  if(pass !== null && pass1 !== null){
  var padre = (pass.parentNode).parentNode;
  var padrePass1 = (pass1.parentNode).parentNode;

  if(pass.value !== pass1.value){
    var error = document.createElement("strong");
    error.innerHTML = "Le due password inserite sono diverse";
    error.className = "erroreInputForm";
    padre.children[1].appendChild(error);
    return false;
  }
  else{
    if(padrePass1.children[1].children[0]) padrePass1.children[1].removeChild(padrePass1.children[1].children[0]);
  }
  return true;
}
return false;
}
//Controlla se la password ha 1 carattere in maiuscolo e in un numero
function checkpass(input, testo, defaultValue, sec){
  var retPass = controllo(input, testo, defaultValue, sec);
  var padre = (input.parentNode).parentNode;
  var ragexpPass = /^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/;
  if(input.value.search(ragexpPass) !== 0 && retPass){
    var error = document.createElement("strong");
    error.innerHTML = "Hai inserito una Password non valida.\n Deve essere composta da 1 lettera maiuscola , un numero e lunga minimo 8 caratteri";
    error.className = "erroreInputForm";
    padre.children[1].appendChild(error);
    return false;
  }
  if(retPass && typeof sec != 'undefined'){
    retPass=passwordDiverse(input, sec);
  }
  return retPass && true;
}

window.onclick = function(evento) {
    //login
    chiusuraFormFuori('contenitoreLogin', 'bottoneLogin', evento.target);
    //menuSezioneDedicata
    chiudiMenu(evento);
};

/*header*/
//LoginVisualizzazione
function login() {
    nascondiForm('contenitoreLogin');
    disabilitazioneAncora(document.getElementById('ancoraLogin'));
    mostraForm('contenitoreLogin', 'bottoneLogin');
    chiusuraFormBottone('contenitoreLogin', 'chiusuraLogin');
    loginControl();
}

//Controllo della form login
function loginControl() {
    var emailLogExample = "esempio@esempio.com";
    var emailLog = document.getElementById("emailL");
    setOn(emailLog, emailLogExample, "E-mail", "email");

    var passExample = "Password";
    var pass = document.getElementById("password");
    setOn(pass, passExample, "Password", "password");

    var loginSubmit = document.getElementById("loginSubmit");
    if (loginSubmit !== null) {
        loginSubmit.onclick = function() {
          return controllo(emailLog, "E-mail", emailLogExample, "") && checkpass(pass, "Password", passExample, "");
        };
    }
}

//Disabilitazione dell' ancora del bottone
function disabilitazioneAncora(aL) {
  if(aL !== null){
    aL.onclick = function() {
        return false;
    };
  }
}

//nacondo la form
function nascondiForm(form) {
  if(document.getElementById(form) !== null)
    document.getElementById(form).style.display = 'none';
}

//mostro la form
function mostraForm(form, bottone) {
  if(document.getElementById(bottone) !== null && document.getElementById(form) !== null){
    document.getElementById(bottone).onclick = function() {
        document.getElementById(form).style.display = 'block';
    };
  }
}

//chiudo la form attraverso x
function chiusuraFormBottone(form, bottone) {
  if(document.getElementById(bottone) !== null ){
    document.getElementById(bottone).onclick = function() {
        document.getElementById(form).style.display = 'none';
    };
  }
}

//chiudo la form attraverso il click furi da esso

function chiusuraFormFuori(form, bottone, target) {
    var blocco = document.getElementById(form);
    var bot = document.getElementById(bottone);
    if (target == blocco) {
        blocco.style.display = 'none';
    }

}

/*index*/
//slider
var indiceSlider = 1;

function slider() {
    mostraDiv(indiceSlider);
    bottoneSinistro();
    bottoneDestro();
}

function bottoneSinistro() {
    var bSX = document.getElementById("bottoneSinistro");
    if (bSX !== null) bSX.onclick = function() {
        mostraDiv(indiceSlider += -1);
    };

}

function bottoneDestro() {
    var bDX = document.getElementById("bottoneDestro");
    if (bDX !== null) bDX.onclick = function() {
        mostraDiv(indiceSlider += 1);
    };
}

function mostraDiv(n) {
    var x = document.getElementsByClassName("sliderJS");
    if (x.length !== 0) {
        if (n > x.length) {
            indiceSlider = 1;
        }
        if (n < 1) {
            indiceSlider = x.length;
        }
        for (var i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[indiceSlider - 1].style.display = "block";
    }

}

/*login.html*/
function pageLoginControl() {
    var emailPageLogExample = "esempio@esempio.com";
    var emailPageLog = document.getElementById("emailLogin");
    setOn(emailPageLog,emailPageLogExample,"E-mail","email");

    var passPageExample = "Password";
    var passPage = document.getElementById("passwordLogin");
    setOn(passPage,passPageExample,"Password","password");

    var logSubmit = document.getElementById("logSubmit");
    if (logSubmit !== null) {
        logSubmit.onclick = function() {
          return controllo(emailPageLog, "Email", emailPageLogExample, "") && checkpass(passPage, "Password", passPageExample, "");
        };
    }
}

//loginAdmin.html
function adminLoginControl() {
    var emailAdminExample = "esempio@esempio.com";
    var emailAdmin = document.getElementById("emailAmministratore");
    setOn(emailAdmin,emailAdminExample,"E-mail","email");

    var passAdminExample = "Password";
    var passAdmin = document.getElementById("passwordAmministratore");
    setOn(passAdmin,passAdminExample,"Password","password");

    var logASubmit = document.getElementById("logASubmit");
    if (logASubmit !== null) {
        logASubmit.onclick = function() {
            return controllo(emailAdmin,"E-mail", emailAdminExample, "") && checkpass(passAdmin, "Password", passAdminExample, "");
        };
    }
}

/*registrazione.html*/
function registrazioneControl() {
    var userExample = "Esempio";
    var regUser = document.getElementById("RUsername");
    setOn(regUser, userExample, "Username", "other");

    var emailRExample = "esempio@esempio.com";
    var emailR = document.getElementById("emailR");
    setOn(emailR,emailRExample,"E-mail","email");

    var passwordExample = "password";
    var Rpass = document.getElementById("RPassword");
    var passwordRCExample = "password1";
    var RCpass = document.getElementById("RCPassword");
    setOn(Rpass, passwordExample,"Password","password", RCpass);
    setOn(RCpass, passwordRCExample, "Ripeti Password", "password", Rpass);


    var submitReg = document.getElementById("Rsubmit");
    if (submitReg !== null) {
        submitReg.onclick = function() {
            return controllo(regUser, "Username", userExample) && checkemail(emailR, "E-mail", emailRExample) && controllo(Rpass, "Password", passwordExample, RCpass) && controllo(RCpass,"Ripeti Password", passwordRCExample,Rpass);
          };
    }
}


/*zoom-immagini*/
function zoom(){

  var  lDis = document.getElementsByClassName("linkDisabilitato");
  var cacc = document.getElementById('cacciatore');
  var caccImg = document.getElementById('immagineCacciata');
  var img = document.getElementsByClassName("immaginePreda");
  var altImg = document.getElementById("altImmagine");

  var count=0;
  for(var k = 0; k<lDis.length; k++){
    count ++;
  }
  if(cacc !== null && caccImg !== null && img!==null && altImg !== null){
    if(count > 0){
    for(var i = 0; i<lDis.length; i++){
      lDis[i].onclick = function(){
        cacc.style.display="block";
        caccImg.src = this.children[0].src;
        altImg.innerHTML = this.children[0].alt;
        return false;
      };
    }
  }
  else{
    for(var j = 0; j<img.length; j++){
      img[j].onclick = function(){
        cacc.style.display="block";
        caccImg.src = this.src;
        altImg.innerHTML = this.alt;
      };
    }
  }
  }
  var span =document.getElementById("chiusura");
  if(span !== null){
    span.onclick = function(){
      cacc.style.display = "none";
    };
  }
}

function checkricerca(input){
      var padre = document.getElementById('spazioDiRicerca').children[0].children[0].children[0];
      if (padre.children[0].children[0]) {
          padre.children[0].removeChild(padre.children[0].children[0]);
      }
      if (input.value==="") {
          var error = document.createElement("strong");
          error.innerHTML = "campo di inserimento testo vuoto";
          error.className = "erroreInputForm";
          padre.children[0].appendChild(error);
          return false;
      }
      return true;
}

function ricerca(){
  if(document.getElementById('subRicerca')!==null){
    var input = document.getElementById('ricercaForm');
    input.onblur = function(){checkricerca(this);};
  document.getElementById('subRicerca').onclick = function(){
    return checkricerca(input);
  };
}
}
function modificaEffettuata(){
  var Massa = document.getElementById('MassaModG');
  if(Massa!==null){
  Massa.onblur=function(){
    controllaPeso(this,"Peso");
  };
}
}
function controllaPeso(Massa, testo){

  if(Massa !== null){
  var padre = (Massa.parentNode).parentNode;
  if (padre.children[1].children[0]) {
      padre.children[1].removeChild(padre.children[1].children[0]);
  }
    if(Massa.value === "" || Massa.value.search(/^([1-9]\d?\d?|0)(\.\d{1,2})?$/)!==0 || Massa.value === "0"){
          var error = document.createElement("strong");
          error.innerHTML = "Tag '" + testo + "' errato, deve essere un numero tra 0.01 e 999.99";
          error.className = "erroreInputForm";
          padre.children[1].appendChild(error);
          return false;
      }
      return true;
    }
  }


function sezioneDedicata(){
  menuSezioneDedicata();
  inserisciGioiello();
  inserisciCollezione();
  inserisciMateriale();
  inserisciPietra();
  inserisciDimensione();
  modificaEffettuata();
}

function menuSezioneDedicata(){
  var DBtn = document.getElementsByClassName("dropbtn");
  var mDD0 = document.getElementById("myDropdown0");
  var mDD1 = document.getElementById("myDropdown1");
  var mDD2 = document.getElementById("myDropdown2");
  if(DBtn!==null && mDD0 !==null && mDD1 !==null && mDD2!==null){
  nascondiDDM(); 
  DBtn[0].onclick = function(){
    aperturaDDM(mDD0,0);
  }
  DBtn[1].onclick = function(){
    aperturaDDM(mDD1,1);
  }
  DBtn[2].onclick = function(){
    aperturaDDM(mDD2,2);
  }
  }

}

function nascondiDDM(){
  var dDC= document.getElementsByClassName("dropdown-content");
  if(dDC.length !== 0) {
    for(var i =0; i< dDC.length; i++){
      dDC[i].style.display = "none";
    }
  }
}

function aperturaDDM(myDropdown,position){
      if (myDropdown.classList.contains('show')){
        myDropdown.classList.remove('show');
        var x = document.getElementsByClassName("dropdown-content");
        x[position].style.display="none";
      }
      else{
      myDropdown.classList.toggle("show");
      var x = document.getElementsByClassName("dropdown-content");
      x[position].style.display="block";

    }

}

function chiudiDD(input,i){
  if (input.classList.contains('show')) {
    input.classList.remove('show');
    var x = document.getElementsByClassName("dropdown-content");
    x[i].style.display="none";
  }
}

function chiudiDB(input, numberI){
  for(var i = 0; i < input.length; i ++){
    if (input[i].classList.contains('show')){
      input[i].classList.remove('show');
      var x = document.getElementsByClassName("dropdown-content");
      x[numberI[i]].style.display="none";
    }
  }

}

function chiudiMenu(e){
  var myDropdown0 = document.getElementById("myDropdown0");
  var myDropdown1 = document.getElementById("myDropdown1");
  var myDropdown2 = document.getElementById("myDropdown2");
  if(myDropdown0 !== null && myDropdown1 !== null && myDropdown2 !== null){
  if (!e.target.matches('.dropbtn')) {
    chiudiDD(myDropdown0,0);
    chiudiDD(myDropdown1,1);
    chiudiDD(myDropdown2,2);
  }
  else{
    if(e.target.matches('.DB0')){
      chiudiDB([myDropdown1,myDropdown2],[1,2]);
    }
    if(e.target.matches('.DB1')){
      chiudiDB([myDropdown0,myDropdown2],[0,2]);
    }
    if(e.target.matches('.DB2')){
      chiudiDB([myDropdown0,myDropdown1],[0,1]);
    }
  }
  }
}

function inserisciGioiello(){
  var IdInsGExample = 'AA00000000';
  var IdInsG= document.getElementById('IdInsG');
  if(IdInsG !== null)if(IdInsG.value === "")setOn(IdInsG,IdInsGExample ,"ID","other", "");

  var NomeInsGExample = 'Esempio';
  var NomeInsG= document.getElementById('NomeInsG');
  if(NomeInsG !== null)if(NomeInsG.value === "") setOn(NomeInsG,NomeInsGExample ,"Nome","other", "");


  var DescrizioneInsGExample = 'Gioiello ispirato a ...';
  var DescrizioneInsG= document.getElementById('DescrizioneInsG');
  if(DescrizioneInsG !== null)if(DescrizioneInsG.value === "") setOn(DescrizioneInsG,DescrizioneInsGExample ,"Descrizione","other", "");

  var Massa = document.getElementById('MassaModGIns');
  if(Massa!==null){
  Massa.value = "0";
  Massa.onblur=function(){
    controllaPeso(this,"Peso");
  };
  Massa.onclick=function(){
    svuota(this,"0");
  };

  }

  if(document.getElementById('submitInsG') !== null){
  document.getElementById('submitInsG').onclick = function(){
      return controllo(IdInsG, "ID", IdInsGExample) && controllo(NomeInsG, "Nome", NomeInsGExample) && controllaPeso(Massa,"Peso") && controllo(DescrizioneInsG, "Descrizione", DescrizioneInsGExample);
  };
}
}

function inserisciCollezione(){
  var NomeInsCExample = 'Esempio';
  var NomeInsC= document.getElementById('nomeCollezione');
  setOn(NomeInsC,NomeInsCExample ,"Nome","other", "");

  var DescrizioneInsCExample = 'Collezione ispirata a ...';
  var DescrizioneInsC= document.getElementById('descrizioneCollezione');
  if(DescrizioneInsC !== null)if(DescrizioneInsC.value === "")setOn(DescrizioneInsC,DescrizioneInsCExample ,"Descrizione","other", "");

  if(document.getElementById('submitInsC') !== null){
  document.getElementById('submitInsC').onclick = function(){
      return controllo(NomeInsC, "Nome", NomeInsCExample) && controllo(DescrizioneInsC, "Descrizione", DescrizioneInsCExample);
  };
}
}

function inserisciMateriale(){
  var NomeInsMExample = 'Esempio';
  var NomeInsM= document.getElementById('nomeMateriale');
  setOn(NomeInsM,NomeInsMExample ,"Nome","other", "");

  var ColoreInsMExample = 'Esempio';
  var ColoreInsM= document.getElementById('coloreMateriale');
  setOn(ColoreInsM, ColoreInsMExample ,"Colore","other", "");

  if(document.getElementById('submitInsM') !== null){
  document.getElementById('submitInsM').onclick = function(){
      return controllo(NomeInsM, "Nome", NomeInsMExample) && controllo(ColoreInsM, "Colore", ColoreInsMExample);
  };
}
}

function inserisciPietra(){
  var NomeInsPExample = 'Esempio';
  var NomeInsP= document.getElementById('nomePietra');
  setOn(NomeInsP,NomeInsPExample ,"Nome","other", "");

  var ColoreInsPExample = 'Esempio';
  var ColoreInsP= document.getElementById('colorePietra');
  setOn(ColoreInsP, ColoreInsPExample ,"Colore","other", "");

  if(document.getElementById('submitInsP') !== null){
  document.getElementById('submitInsP').onclick = function(){
      return controllo(NomeInsP, "Nome", NomeInsPExample) && controllo(ColoreInsP, "Colore", ColoreInsPExample);
  };
}
}

function inserisciDimensione(){
  var NomeInsDExample = 'Esempio';
  var NomeInsD= document.getElementById('nomeDimensione');
  setOn(NomeInsD,NomeInsDExample ,"Nome","other", "");

  var UMInsDExample = 'Esempio';
  var UMInsD= document.getElementById('udm');
  setOn(UMInsD, UMInsDExample ,"Unità di Misura","other", "");

  var grandInsDExample = "0";
  var grandInsD = document.getElementById('grandezzaDim');
  if(grandInsD!==null){
    grandInsD.value=grandInsDExample;
  grandInsD.onblur=function(){

    controllaPeso(this,"Grandezza");
  };
  grandInsD.onclick=function(){
    svuota(this,grandInsDExample);
  };
  }

var x=true;
  if(document.getElementById('submitInsD') !== null){
  document.getElementById('submitInsD').onclick = function(){
    var x=controllo(NomeInsD, "Nome", NomeInsDExample) && controllo(UMInsD, "Unità di Misura", UMInsDExample) && controllaPeso(grandInsD,"Grandezza");
    if(x === true){
    var grandTip =document.getElementsByClassName('tipDim');
    var check = false;

    var padre = grandTip[0].parentNode.parentNode.parentNode;
    if(padre.children[0].children[0]){
      padre.children[0].removeChild(padre.children[0].children[0]);
      }

    for(var i = 0; i<grandTip.length; i++){
      if(grandTip[i].checked === true) check=true;
    }
    if(check === false){
      var error = document.createElement("strong");
      error.innerHTML = "Devi scegliere la tipologia";
      error.className="erroreInputForm";
      padre.children[0].appendChild(error);
    }
    return check
  }
  return x;

  };
}
}
