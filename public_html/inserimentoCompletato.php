<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('Parti/isUploadFile.php');
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');


    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);

    $menu = '<li xml:lang="en"><a href="index.php">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;
    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }


    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }
    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Sezione Dedicata</p>';



    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

    

    try {

    $interrogazioneS = "";
    $interrogazioneF = "";
    $interrogazioneW = "";
    $strErr = "";
    $nme = "";
    $errImm = "";

    if($sessioneAperta== true){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          if(!empty($_POST["submitInsG"])){
              if(!empty($_POST["id"])&&!empty($_POST["nome"])&&!empty($_POST["Materiale"])&&
              !empty($_POST["Dimensione"])){
                  $insertInt = "";
                  $table = array("Id");
                  $values = array($_POST["id"]);
                  if(!ctype_alnum($_POST["id"])){
                       $strErr='<p>Id deve contenere solo caratteri alfanumerici</p>';
                  }else {
                       if(ctype_digit($_POST["id"])){
                           $strErr=$strErr.'<p>Id non deve contenere solo caratteri numerici</p>';
                       }
                  }
                  if(ctype_alnum(trim(str_replace(' ','',$_POST["nome"])))){
                         $nme = htmlentities($_POST["nome"]);
                         array_push($table,"Nome");
                         array_push($values,$nme);
                  }
                  else {
                      $strErr=$strErr.'<p>Nome deve contenere solo caratteri alfanumerici e niente caratteri accentati</p>';
                  }
                  if(!empty($_POST["descrizione"])){
                      $es = htmlentities($_POST["descrizione"]);
                      $ass = preg_replace("/'/","&#39;",$es);
                      array_push($table,"Descrizione");
                      array_push($values,$es);
                  }
                  if(!empty($_POST["PesoInsG"])){
                      if(!preg_match("/[0.01-999.99]/",$_POST["PesoInsG"])){
                          $strErr=$strErr.'<p>Peso deve essere compreso tra 0.01 e 999.999</p>';
                      }else{
                      array_push($table,"Peso");
                      array_push($values,$_POST["PesoInsG"]);
                  }
                  }
                    if(!empty($_POST["sesso"])){
                        array_push($table,"Sesso");
                        array_push($values,$_POST["sesso"]);
                    }
                    if(isset($_POST["esclusiva"])){
                        array_push($table,"Esclusiva");
                        array_push($values,$_POST["esclusiva"]);
                    }
                    if(strlen($strErr)==0){
                        $errImm = isUploadFile('img/gioielli/'.$_POST["id"],"immagine");
                        if(($errImm=='png')||($errImm=='jpeg')||($errImm=='jpg')){
                                array_push($table,"Immagini");
                                array_push($values,"img/gioielli/".$_POST["id"].".".$errImm);

                        }
                            else {
                                if($errImm!="IMGNOTINSERT")
                                    $strErr=$strErr.'<p>'.$errImm.'</p>';
                            }
                    }
                    if(!empty($_POST["Collezione"])){
                        if($_POST["Collezione"]!="NULL"){
                            array_push($table,"Collezione");
                            array_push($values,$_POST["Collezione"]);
                        }
                    }
                    if(!empty($_POST["parourTesto"])){
                        if(ctype_alnum($_POST["parourTesto"])){
                            array_push($table,"Parour");
                            array_push($values,$_POST["parourTesto"]);
                        }else {
                            $strErr=$strErr.'<p>Parour deve contenere solo valori alfabetici o numerici</p>';
                        }
                    }else {
                        if($_POST["Parour"]!="NULL"){
                            if(ctype_alnum($_POST["Parour"])){
                                array_push($table,"Parour");
                                array_push($values,$_POST["Parour"]);
                            }else {
                                $strErr=$strErr.'<p>Parour deve contenere solo valori alfabetici o numerici</p>';
                            }
                        }
                    }


                    if(strlen($strErr)==0){


                    $values1 = array($values);

                    $insertInt = new InsertInterrogation(array("gioielli"),$table,$values1);
                    try{
                            $insertInt->interrogation();
                    } catch (Exception $e) {
                    if($e->getMessage() == "DPE"){
                      echo "<p>Errore Id gi&agrave; inserita </p><p><a href='inserimenti.php?ins=gioielli'>ricompila la form</a></p>";
                      throw new Exception("",1);
                  }
                    else {
                      throw new Exception($e->getMessage(),1);
                    }
                  }
                    $piecesMat = explode("@", $_POST['Materiale']);

                    $insertMat = new InsertInterrogation(array("prodotto"),
                    array("Gioiello","Nome_Materiale","Colore_Materiale"),
                    array(array($_POST['id'],$piecesMat[0],$piecesMat[1])));
                    $insertMat->interrogation();
                    if($_POST['Pietra']!="NULL"){
                        $piecesPietr = explode("@", $_POST['Pietra']);

                        $insertPietr = new InsertInterrogation(array("ornamento"),
                        array("Gioiello","Nome_Pietra","Colore_Pietra"),
                        array(array($_POST['id'],$piecesPietr[0],$piecesPietr[1])));
                        $insertPietr->interrogation();

                    }

                    $insertIntDim = new InsertInterrogation(array("taglia"),
                    array("Gioiello","Dimensione"),array(array($_POST['id'],$_POST['Dimensione'])));
                    $insertIntDim->interrogation();


                    echo '<p>Inserimento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';

              }
              else {
                  echo '<p>Errore Compilazione Form <p>'.$strErr.'<p><a href="inserimenti.php?ins=gioielli">ricompila la form</a></p>';
              }
          }else {
              echo '<p>Errore Compilazione Form, il nome e il codice sono valori obbligatori da riempire, <a href="inserimenti.php?ins=gioielli">ricompila la form</a></p>';
          }

            }
            elseif(!empty($_POST["submitInsC"])){
                if(!empty($_POST["nomeCollezione"])){
                    $insertInt = "";
                    $strErr = "";
                    $table = array("Nome");
                    $values = array($_POST["nomeCollezione"]);
                    if(!ctype_alnum($_POST["nomeCollezione"])){
                         $strErr='<p>Il nome della collezione deve contenere solo caratteri alfanumerici</p>';
                    }else {
                         if(ctype_digit($_POST["nomeCollezione"])){
                             $strErr=$strErr.'<p>Il nome della collezione non deve contenere solo caratteri numerici</p>';
                         }
                    }

                    if(!empty($_POST["descrizioneCollezione"])){
                        $ass = preg_replace("/'/","&#39;",$_POST["descrizioneCollezione"]);
                        $es = htmlentities($ass);
                        array_push($table,"Descrizione");
                        array_push($values,$es);
                    }

                    if(isset($_POST["esclusivaCollezione"])){
                        array_push($table,"Esclusiva");
                        array_push($values,$_POST["esclusivaCollezione"]);
                    }
                    if(strlen($strErr)==0){
                        $errImm = isUploadFile('img/collezioni/'.$_POST["nomeCollezione"],"immagineCollezione");
                            if(($errImm=='png')||($errImm=='jpeg')||($errImm=='jpg')){
                                    array_push($table,"Immagine");
                                    array_push($values,"img/collezioni/".$_POST["nomeCollezione"].".".$errImm);
                            }
                            else {
                                if($errImm!="IMGNOTINSERT")
                                    $strErr=$strErr.'<p>'.$errImm.'</p>';
                            }
                    }
                    if(strlen($strErr)==0){

                        $values1 = array($values);

                        $insertInt = new InsertInterrogation(array("collezione"),$table,$values1);
                        try {
                        $insertInt->interrogation();
                    } catch (Exception $e) {
                if($e->getMessage() == "DPE"){
                echo "<p>Errore Nome gi&agrave; inserito </p><p><a href='inserimenti.php?ins=collezioni'>ricompila la form</a></p>";
                throw new Exception("",1);
            }
                else {
                  throw new Exception($e->getMessage(),1);
                }
              }

                        echo '<p>Inserimento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
                    }
                    else{
                        echo '<p>Errore Compilazione Form</p>'.$strErr.'<p><a href="inserimenti.php">ricompila la form</a></p>';
                    }
                }else {
                    echo '<p>Errore Compilazione Form, il nome della Collezione è obbligatorio <a href="inserimenti.php?ins=collezioni">ricompila la form</a></p>';
                }
            }
            elseif(!empty($_POST["submitInsM"])){
                if(!empty($_POST["nomeMateriale"])&&
                !empty($_POST["coloreMateriale"])){
                    $insertInt = "";
                    $strErr = "";
                    $table = array("Nome","Colore");
                    $values = array($_POST["nomeMateriale"],$_POST["coloreMateriale"]);

                    if(!ctype_alnum($_POST["nomeMateriale"])){
                         $strErr='<p>Il nome del materiale deve contenere solo caratteri alfanumerici</p>';
                    }else {
                         if(ctype_digit($_POST["nomeMateriale"])){
                             $strErr=$strErr.'<p>Il nome del materiale non deve contenere solo caratteri numerici</p>';
                         }
                    }
                    if(!ctype_alnum($_POST["coloreMateriale"])){
                         $strErr=$strErr.'<p>Il colore del materiale deve contenere solo caratteri alfanumerici</p>';
                    }else {
                         if(ctype_digit($_POST["coloreMateriale"])){
                             $strErr=$strErr.'<p>Il colore del materiale non deve contenere solo caratteri numerici</p>';
                         }
                    }
                    if(strlen($strErr)==0){

                    $values1 = array($values);

                    $insertInt = new InsertInterrogation(array("materiale"),$table,$values1);
                    try {
                    $insertInt->interrogation();
                } catch (Exception $e) {
            if($e->getMessage() == "DPE"){
              echo "<p>Errore Nome Materia e Colore Materiale gi&agrave; inseriti </p><p><a href='inserimenti.php?ins=mat'>ricompila la form</a></p>";
              throw new Exception("",1);
          }
            else {
              throw new Exception($e->getMessage(),1);
            }
          }

                    echo '<p>Inserimento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';
                }else{
                echo '<p>Errore Compilazione Form</p>'.$strErr.'<p><a href="inserimenti.php?ins=mat">ricompila la form</a></p>';
            }
        }else {
            echo '<p>Errore Compilazione Form, <a href="inserimenti.php?ins=mat">ricompila la form</a></p>';
        }

    }elseif(!empty($_POST["submitInsP"])){
        if(!empty($_POST["nomePietra"])&&
        !empty($_POST["colorePietra"])&&
        !empty($_POST["dimensionePietra"])){
            $insertInt = "";
            $strErr = "";
            $table = array("Nome","Colore","Dimensione");
            $values = array($_POST["nomePietra"],$_POST['colorePietra'],$_POST["dimensionePietra"]);

            if(!ctype_alnum($_POST["nomePietra"])){
                 $strErr='<p>Il nome della pietra deve contenere solo caratteri alfanumerici</p>';
            }else {
                 if(ctype_digit($_POST["nomePietra"])){
                     $strErr=$strErr.'<p>Il nome della pietra non deve contenere solo caratteri numerici</p>';
                 }
            }
            if(!ctype_alnum($_POST["colorePietra"])){
                 $strErr=$strErr.'<p>Il colore della pietra deve contenere solo caratteri alfanumerici</p>';
            }else {
                 if(ctype_digit($_POST["colorePietra"])){
                     $strErr=$strErr.'<p>Il colore della pietra non deve contenere solo caratteri numerici</p>';
                 }
            }
            if(strlen($strErr)==0){
            $values1 = array($values);

            $insertInt = new InsertInterrogation(array("pietra"),$table,$values1);
            try {
            $insertInt->interrogation();
        } catch (Exception $e) {
    if($e->getMessage() == "DPE"){
    echo "<p>Errore Nome Pietra e Colore Pietra gi&agrave; inseriti </p><p><a href='inserimenti.php?ins=ptr'>ricompila la form</a></p>";
    throw new Exception("",1);
    }
    else {
      throw new Exception($e->getMessage(),1);
    }
  }

            echo '<p>Inserimento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';

        }else{
        echo '<p>Errore Compilazione Form</p>'.$strErr.'<p><a href="inserimenti.php?ins=ptr">ricompila la form</a></p>';
}
}else {
    echo '<p>Errore Compilazione Form, s <a href="inserimenti.php?ins=ptr">ricompila la form</a></p>';
}

    }elseif(!empty($_POST["submitInsD"])){
        if(!empty($_POST["nomeDimensione"])&&
        !empty($_POST["grandezzaDim"])&&
        !empty($_POST["tipologiaDim"])){
            $insertInt = "";
            $strErr = "";
            $table = array("Codice","Grandezza","Tipologia");
            $values = array($_POST["nomeDimensione"],$_POST["grandezzaDim"],$_POST["tipologiaDim"]);

            if(!ctype_alnum($_POST["nomeDimensione"])){
                 $strErr='<p>Il codice della dimensione deve contenere solo caratteri alfanumerici</p>';
            }else {
                 if(ctype_digit($_POST["nomeDimensione"])){
                     $strErr=$strErr.'<p>Il codice della dimensione non deve contenere solo caratteri numerici</p>';
                 }
            }

            if(!preg_match("/[0.001-999.999]/",$_POST["grandezzaDim"])){
                $strErr=$strErr.'<p>La grandezza della dimensione deve essere compresa tra 0.001 e 999.999</p>';
            }

            if(!empty($_POST["udm"])){
                if(ctype_alpha($_POST["udm"])){
                    array_push($table,"UnitaDiMisura");
                    array_push($values,$_POST["udm"]);
                }else {
                    $strErr=$strErr.'<p>L\'unit&agrave; di misura deve contenere solo caratteri alfabetici</p>';
                }

            }
            if(strlen($strErr)==0){
            $values1 = array($values);

            $insertInt = new InsertInterrogation(array("dimensione"),$table,$values1);
            try {
            $insertInt->interrogation();
        } catch (Exception $e) {
    if($e->getMessage() == "DPE"){
        echo "<p>Errore Codice gi&agrave; inserito </p><p><a href='inserimenti.php?ins=dim'>ricompila la form</a></p>";
        throw new Exception("",1);
    }
    else {
      throw new Exception($e->getMessage(),1);
    }
  }

            echo '<p>Inserimento riuscito, torna alla <a href="sezioneDedicata.php">Sezione Dedicata</a></p>';

        }else{
        echo '<p>Errore Compilazione Form</p>'.$strErr.'<p><a href="inserimenti.php?ins=dim">ricompila la form</a></p>';
    }
}else {
    echo '<p>Errore Compilazione Form, <a href="inserimenti.php?ins=dim">ricompila la form</a></p>';
}
}
}
}
} catch (Exception $e) {

 echo $e->getMessage();
}
require_once("Parti/footer.php");
echo printfooter($sessioneAperta);
?>
