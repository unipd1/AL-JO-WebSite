<?php
session_start();
require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "404 - AL.JO. Gioielli Center";
$a2 = "Pagina di 404, errore nel codice dell'URL";
$a3 = "404, AL.JO., Vicenza";
$a4 = "index,follow";

echo printHeadHTML($a1,$a2,$a3,$a4);


$menu = '<li><a href="index.php">Home</a></li>
<li><a href="lista_gioielli.php">Gioielli</a></li>
<li><a href="lista_collezioni.php">Collezioni</a></li>
<li><a href="informazioni.php">Informazioni</a></li>';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;
if(isset($_SESSION['username'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}

$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; 404</p>';

echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

echo file_get_contents('Parti/404Body.html');

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);
 ?>
