<?php
session_start();
require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');


$a1 = "Home - AL.JO. Gioielli Center";
$a2 = "Pagina inziale del sito AL.JO.
      Gioielli in cui c'è uno slider con
      gli obiettivi della azienda, e dei link ai
       gioielli e alle collezioni";
$a3 = "AL.JO., Gioielli, Collezioni, Collane,
      Bracciali, Anelli, Vicenza, Bigiotteria";
$a4 = "index,follow";
echo printHeadHTML($a1,$a2,$a3,$a4);


$menu = '<li xml:lang="en" class="active">Home</li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;
$boolSaltaG = false;
$boolSaltaC = false;
if(isset($_SESSION['username'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}

$breadCrumb ='<p>Ti trovi in : <span xml:lang="en">Home</span></p>';

echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

echo"<a href='#indexGioielli' class='salto'>Salta lo <span xml:lang='en'>slider</span> contente delle immagini e vai alla lista dei gioielli</a>
<a href='#menu' class='salto'>Ritorna al menu</a>";
echo file_get_contents('Parti/indexBody.html');


$selectG = new SelectInterrogation(array("Id","Nome","Immagini","Descrizione"),
         array("gioielli"),array("esclusiva = 0","AND","1 ORDER BY RAND() LIMIT 3"));
try {
  $risultatoG = $selectG->interrogation();
} catch (Exception $e) {
  $boolSaltaG = true;
}
if($boolSaltaG==false){
if($risultatoG->num_rows > 0){
   echo'<div id="indexGioielli">
  <a href="#indexCollezioni" class=\'salto\'>Salta lo l\'elenco dei gioielli e vai all\'elenco delle collezioni</a>
  <a href="#menu" class="salto">Ritorna al menu</a>
         <h2><a href="lista_gioielli.php">I nostri Gioielli</a></h2><p>
                  I Gioielli sono costruiti interamente a mano dai nostri artigiani.
               </p>';
      while($row = $risultatoG->fetch_array(MYSQLI_ASSOC)){
         echo "<a href=\"gioiello.php?id=".$row['Id']."\">
            <img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" height='260px' width='260px' />
            </a>";

      }
      echo '
         </div>';
   }
   $risultatoG->free();
 }

   $selectC = new SelectInterrogation(array("Nome","Immagini","Descrizione"),
            array("collezione"),array("Esclusiva = 0","AND","1 ORDER BY RAND() LIMIT 3"));

   try {
            $risultatoC = $selectC->interrogation();
       } catch (Exception $e) {
         $boolSaltaC = true;
       }
  if($boolSaltaC==false){
   if($risultatoC->num_rows > 0){
      echo '<div id="indexCollezioni">
      <a href="#footer" class="salto" >Salta l\'elenco delle collezioni e vai al <span xml:lang=\'en\'>footer</span>
      </a><a href="#menu" class="salto">Ritorna al menu</a>
         <h2><a href="lista_collezioni.php">Le nostre Collezioni</a></h2>
         <p>
                  Le collezioni nascono da un sogno che i nostri soci
                  riescono a portare nel nostro mondo.
               </p>
               ';
         while($row = $risultatoC->fetch_array(MYSQLI_ASSOC)){
            echo "
                  <a href=\"collezione.php?name=".$row['Nome']."\" class='linkDisabilitato'><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/></a>
                ";
         }
         echo '
          </div>';
      }
      $risultatoC->free();
    }

      require_once('Parti/footer.php');
      echo printfooter($sessioneAperta);
 ?>
