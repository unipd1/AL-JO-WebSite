<?php
session_start();

  require_once("Parti/stampaHTML.php");
  require_once('../php/SelectInterrogation.php');

  $a1 = "Collezione - AL.JO. Gioielli Center";

  if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if(!empty($_GET["name"])){
          $a1 ="Collezione ".$_GET["name"]." - AL.JO. Gioielli Center";
        }
  }

  $a2 = "Pagina in cui viene descritta una collezione con i suoi relativi gioielli";
  $a3 = "Gioielli, AL.JO., Collane, Bracciali, Anelli, Vicenza";
  $a4 = "index,follow";
  echo printHeadHTML($a1,$a2,$a3,$a4);


  $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li class="active superActive"><a href="lista_collezioni.php" class="active">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>';

  $sessioneAperta = false;
  $sezioneLogin = "";
  $admin = 0;

  if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
        $sessioneAperta = true;
        $sezioneLogin =$_SESSION['username'];
        $email = $_SESSION['email'];
        $admin = $_SESSION['admin'];
  }

  if($sessioneAperta== true){
      if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
  }


  $getId = "";

  if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if(!empty($_GET["name"])){
              $getId = $_GET['name'];
        }
  }

  $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt;
  <a href="lista_collezioni.php">Lista Collezioni</a>&gt; Collezione '.$getId."</p>";

  echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);

  try {


  if(!empty($_GET["name"])){

  $a1 = array("distinct collezione.Nome as CN", "collezione.Descrizione as CD", "collezione.Immagini as IM","Esclusiva");
  $a2 = array("collezione");
  $a3 = array("collezione.Nome ="."'".$getId."'");
  $counter = 0;

  $question = new SelectInterrogation(array("Esclusiva"),$a2,$a3);
  try{
  $ARisultato = $question->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
  $risultatoEsclusiva = 0;
  if($ARisultato->num_rows > 0){
   while($row = $ARisultato->fetch_array(MYSQLI_ASSOC)){
        $risultatoEsclusiva= $row['Esclusiva'];
   }
  }

  $ARisultato->free();
$vincolo = "";
if($risultatoEsclusiva==1){

if($sessioneAperta==true){

    array_push($a3,"AND","collezione.Esclusiva = ".$risultatoEsclusiva);
    if($admin==0){
      array_push($a2,"esclusivacollezione");
      array_push($a3,"AND","collezione.Nome = esclusivacollezione.Collezione");
      array_push($a3,"AND","esclusivacollezione.Utente ='".$email."'");

      $vincolo = "AND (gioielli.Esclusiva = 0 OR gioielli.Id  in (SELECT Gioiello FROM esclusivagioiello  WHERE esclusivagioiello.Utente = '".$email."')) ";
    }
}
else {
  array_push($a3,"AND","collezione.Esclusiva = 0");
}
}else {
  array_push($a3,"AND","collezione.Esclusiva = 0");
}

$limite1 = 0;
$limite2 = 30;

$Select = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato = $Select->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo '<div id="cacciatore" class="divZoom">
  <span id="chiusura">&times;</span>
  <img src="img/broken.jpg" class="contenutoImmagine" id="immagineCacciata" alt="Immagine di default"/>
  <div id="altImmagine"></div>
</div>';
if($arrayRisultato->num_rows > 0){
  $counter= 1;
 while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
       echo "<div id='contenitoreCollezione'>
       <img src='".$row['IM']."' alt='immagine collezione' width='1600px' height='200px'  />
       <div id='titoloCollezione'>
                  <h1>".$row['CN']."</h1>";
        if($sessioneAperta==true){
          if($row['Esclusiva']==1){
            echo "<div id = 'collezioneInEsclusiva'><p>
            Collezione in Esclusiva
            </p></div>";
          }
        }

        echo"<a href='#divListaGioielli' class='salto'>Salta la descrizione e vai all'elenco dei gioielli di collezioni</a>
        <a href='#menu' class='salto'>Ritorna al menu</a>
        <p>".$row['CD']."</p>
            </div>
       </div>";
      }
}
echo "";
$arrayRisultato->free();
if($counter>0){
$a1 = array("gioielli.Id as GI","gioielli.Nome as GN", "gioielli.Immagini as GIM ","gioielli.Descrizione as GD","gioielli.Esclusiva as GE");
$a2 = array("collezione","gioielli");
if($sessioneAperta==true)$a3 = array("gioielli.Collezione = collezione.Nome", "AND", "collezione.Nome ="."'".$_GET['name']."' ".$vincolo."ORDER  By RAND()");
else $a3 = array("gioielli.Collezione = collezione.Nome", "AND", "collezione.Nome ="."'".$_GET['name']."'", "AND" ,"collezione.Esclusiva = 0","AND","gioielli.Esclusiva = 0 ORDER  By RAND()");

$Select1 = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato1 = $Select1->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
$risultato = $arrayRisultato1->num_rows;
$arrayRisultato1->free();
array_push($a3,"LIMIT ".$limite1.",".$limite2);
$Select1 = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato1 = $Select1->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo"<div id='divListaGioielli'><a href='#divNFP' class='salto'>Salta i gioielli a vai al numero di pagine</a>
      <a href='#menu' class='salto'>Ritorna al menu</a>";
if($arrayRisultato1->num_rows > 0){
      if(!empty($_GET["numero"])){
           $limite1 = ($_GET["numero"]-1)*30;
      }
      echo "
      <ul id='listaGioielli'>";
 while($row = $arrayRisultato1->fetch_array(MYSQLI_ASSOC)){
    if($row['GE']==0){
         echo "<li class='elementoGioielli'>
         <h2><a href=\"gioiello.php?id=".$row['GI']."\">".$row['GN']."</a></h2><a class='linkDisabilitato' href=\"gioiello.php?id=".$row['GI']."\"><img src='".$row['GIM']."' alt=\"".$row['GD']."\" class='immaginePreda' width='260px' height='260px' /></a>";
       }
       else{
         echo "<li class='elementoGioielli Esclusiva'><h2><a href=\"gioiello.php?id=".$row['GI']."\">".$row['GN']."</a></h2><a class='linkDisabilitato' href=\"gioiello.php?id=".$row['GI']."\"><img src='".$row['GIM']."' alt=\"".$row['GD']."\" class='immaginePreda' width='260px' height='260px' /><span class='nascosto'>Esclusiva</span></a>";
       }
      echo"
      </li>";
 }
 echo "</ul>";
}

echo "</div>";
$arrayRisultato1->free();



echo "<div id='divNFP'><a href='#footer' class='salto'>Salta i numeri fondo pagina  e vai al <span xml:lang='en'>footer</span></a>
<a href='#menu' class='salto'>Ritorna al menu</a>";
echo "<ul id='numeriFondoPagina'>";
if(intval($risultato/30) == 0)
  $ri = 1;
else{
  if($risultato%30>0){
      $ri = intval($risultato/30) +1;
  }
  else {
    $ri = $risutato/30;
  }
}

for ($i=0; $i < intval($ri); $i++) {
  if(!isset($_GET["numero"])){
    if(($i+1)==1){
      echo"<li class='elementoNumeroGioiello active'>
      <span class='active'>".($i+1)."</span>
      </li>";
    }
    else{
      echo"<li class='elementoNumeroGioiello'>
      <a href=\"collezione.php?name=".$_GET['name']."&numero=".($i+1)."\">".($i+1)."</a>
      </li>";
    }
  }else{
    if($_GET["numero"]==($i+1)){
      echo"<li class='elementoNumeroGioiello active'>
      <span class='active'>".($i+1)."</span>
      </li>";
    }
    else{
      echo"<li class='elementoNumeroGioiello'>
      <a href=\"collezione.php?name=".$_GET['name']."&numero=".($i+1)."\">".($i+1)."</a>
      </li>";
    }
  }

}
echo"</ul></div>";

}else {
  echo "<div id='contenitoreCollezione'>
  <div id='titoloCollezione'>
             <h1>Collezione non trovata</h1>
             <p>La collezione ricercata non è stata trovata</p>
       </div>
  </div>";
}

}else {
  echo "<div id='contenitoreCollezione'>
  <div id='titoloCollezione'>
             <h1>Collezione non trovata</h1>
             <p>La collezione ricercata non è stata trovata</p>
       </div>
  </div>";
}

} catch (Exception $e) {
    echo $e->getMessage();
}

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);

 ?>
