<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/DeleteInterrogation.php');

    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);
    try {

    $menu = '<li><a href="index.php">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin=0;

    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true && $admin==1){

        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;
        <a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a>&gt; Aggiornamento&nbsp;Esclusiva</p>';
    }
    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
    if($sessioneAperta== true){


        if ($_SERVER["REQUEST_METHOD"] == "POST") {

          if(!empty($_POST["id"])&&!empty($_POST['elem'])){
              $inter = "";
              $from = array();
              $where = array();
              $pieces = explode("&", $_POST["id"]);
              if(!isset($pieces[0])){
                echo"<p>
                Modifica avvenuta con successo.Ritorna a <a href='sezioneDedicata.php'>Sezione Dedicata</a>
                </p>";
              }

              if($pieces[0]=='eliminaGioielloEsclusiva'){

                  $values = isset($_POST['elem']) ? $_POST['elem'] : array();

                  if (count($values)==0){
                      echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
                      <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
                  }
                  else {
                      for ($i=0; $i <count($values) ; $i++) {
                          if($i==0){
                              array_push($where,"Gioiello ='".$values[$i]."'");
                          }else {
                              array_push($where,"OR","Gioiello ='".$values[$i]."'");
                          }
                      }
                  array_push($where,"AND","Utente = '".$pieces[1]."'");
                  $inter= new DeleteInterrogation("esclusivagioiello",$where);
                  try {
                  $risult = $inter->interrogation();
              } catch (Exception $e) {
                  throw new Exception($e->getMessage());
              }
                  echO"<p>
                  Eliminazione avvenuta con successo, ritorna ad <a href='aggiornaEsclusive.php'>aggiorna esclusive</a>
                  </p>";
              }
             }
              elseif($pieces[0]=='eliminaCollezioneEsclusiva'){

                  $values = isset($_POST['elem']) ? $_POST['elem'] : array();
                  if (count($values)==0){
                      echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
                      <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
                  }
                  else {
                      for ($i=0; $i <count($values) ; $i++) {
                          if($i==0){
                              array_push($where,"Collezione ='".$values[$i]."'");
                          }else {
                              array_push($where,"OR","Collezione ='".$values[$i]."'");
                          }
                      }
                  array_push($where,"AND","Utente = '".$pieces[1]."'");
                  $inter= new DeleteInterrogation("esclusivacollezione",$where);
                  try {
                  $risult = $inter->interrogation();
              } catch (Exception $e) {
                  throw new Exception($e->getMessage());
              }
                  echo"<p>
                  Eliminazione avvenuta con successo, ritorna ad <a href='aggiornaEsclusive.php'>aggiorna esclusive</a>
                  </p>";
              }
              }
              elseif($pieces[0]=='aggiungiGioielloEsclusiva'){

                  $values = isset($_POST['elem']) ? $_POST['elem'] : array();
                  array_push($from,"esclusivagioiello");
                  $table = array("Utente","Gioiello");

                  if (count($values)==0){
                      echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
                      <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
                  }
                  else {
                      for ($i=0; $i <count($values) ; $i++) {
                          array_push($where,array($pieces[1],$values[$i]));
                      }
                  $inter= new InsertInterrogation($from,$table,$where);
                  try {
                  $risult = $inter->interrogation();
              } catch (Exception $e) {
                  throw new Exception($e->getMessage());
              }
                  echo"<p>
                  Inserimento avvenuto con successo, ritorna ad <a href='aggiornaEsclusive.php'>aggiorna esclusive</a>
                  </p>";
              }
              }
              elseif($pieces[0]=='aggiungiCollezioneEsclusiva'){

                  $values = isset($_POST['elem']) ? $_POST['elem'] : array();
                  array_push($from,"esclusivacollezione");
                  $table = array("Utente","Collezione");

                  if (!count($values)){
                      echo '<p>Errore! Devi selezionare almeno un gioiello, torna alla
                      <a href="modifica.php?del=mat">lista di materiali</a>.</p>';
                  }
                  else {
                      for ($i=0; $i <count($values) ; $i++) {
                          array_push($where,array($pieces[1],$values[$i]));
                      }
                  $inter= new InsertInterrogation($from,$table,$where);
                  try {
                  $risult = $inter->interrogation();
              } catch (Exception $e) {
                  throw new Exception($e->getMessage());
              }
                  echo"<p>
                  Inserimento avvenuto con successo, ritorna ad <a href='aggiornaEsclusive.php'>aggiorna esclusive</a>
                  </p>";
              }
            }
          }else{
            echo"<p>
            Errore riprova ad eseguire l'operazione
            </p>";
          }
    }
}
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    require_once("Parti/footer.php");
    echo printfooter($sessioneAperta);
?>
