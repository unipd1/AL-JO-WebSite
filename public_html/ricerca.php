<?php

session_start();

require_once("Parti/stampaHTML.php");
require_once('../php/SelectInterrogation.php');

$a1 = "Ricerca - AL.JO. Gioielli Center";
$a2 = "Pagina in cui è presente una ricerca in base a i dati inseriti";
$a3 = "";
$a4 = "noindex,follow";
$adminString1 = "";
$adminString2 = "";
echo printHeadHTML($a1,$a2,$a3,$a4);



$menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
<li><a href="lista_gioielli.php">Gioielli</a></li>
<li><a href="lista_collezioni.php">Collezioni</a></li>
<li><a href="informazioni.php">Informazioni</a></li>
';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;

if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
    $sessioneAperta = true;
    $sezioneLogin =$_SESSION['username'];
    $admin = $_SESSION['admin'];
    $email = $_SESSION['email'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}
$breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Ricerca</p>';

echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);

echo "<div id='hRicerca'><h1>Risultati Ricerca</h1>";

try{

if ($_SERVER["REQUEST_METHOD"] == "GET") {
      if(empty($_GET["ricercaForm"])){
            echo "<div id ='risultatiRicerca'>Devi scrivere all'interno della barra per produrre una ricerca</div>";
      }
      else {



$a1 = array("DISTINCT Id","Nome","Immagini","Descrizione","Esclusiva","Collezione");
$a2 = array("gioielli");
$a3 = array("( gioielli.Nome like "."'%".$_GET['ricercaForm']."%'","OR",
            "gioielli.Descrizione like "."'%".$_GET['ricercaForm']."%'","OR",
            "gioielli.Collezione like "."'%".$_GET['ricercaForm']."%'","OR",
            "gioielli.Id in (SELECT gioielli.Id FROM gioielli, prodotto WHERE gioielli.Id = prodotto.Gioiello AND prodotto.Nome_Materiale="."'".$_GET['ricercaForm']."'"." )","OR",
            "gioielli.Id in (SELECT gioielli.Id FROM gioielli, ornamento WHERE gioielli.Id = ornamento.Gioiello AND ornamento.Nome_Pietra ="."'".$_GET['ricercaForm']."'"." )","OR",
            "gioielli.Id in (SELECT gioielli.Id FROM gioielli, dimensione, taglia WHERE gioielli.Id = taglia.Gioiello  AND taglia.Dimensione = dimensione.Codice AND dimensione.Tipologia ="."'".$_GET['ricercaForm']."'"." ) )");
if($sessioneAperta==true){
  if($admin==0){
      array_push($a3,"AND","esclusiva = 0","UNION
      (SELECT Id, Nome, Immagini, Descrizione, Esclusiva, Collezione
      FROM esclusivagioiello, gioielli
      WHERE esclusivagioiello.Utente ='".$email."'","AND",
      "esclusivagioiello.Gioiello=gioielli.Id) ");
  }
}
else {
    array_push($a3,"AND","esclusiva = 0 ");
}

$risultato = 0;
$Select = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato = $Select->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
$risultato = $arrayRisultato->num_rows;
echo "<div id ='risultatiRicerca'><h2>La ricerca ha prodotto ".$risultato." risutati.</h2></div></div>";

$limite1 = 0;
$limite2 = 10;
if ($_SERVER["REQUEST_METHOD"] == "GET") {
 if(!empty($_GET["numero"])){
          $limite1 = ($_GET["numero"]-1)*10;
 }
}
$arrayRisultato->free();


array_push($a3," ORDER BY Nome, Collezione, Descrizione LIMIT ".$limite1.",".$limite2);
$Select = new SelectInterrogation($a1,$a2,$a3);
try{
$arrayRisultato = $Select->interrogation();
} catch (Exception $e) {
  throw new Exception($e->getMessage());

}
echo"
<a href='#divNumeriFondoPagina' class='salto'>Salta i risultati di ricerca e vai ai numeri di cambio pagina</a>
<a href='#menu' class='salto'>Ritorna al menu</a>";
if($arrayRisultato->num_rows > 0){
  if($sessioneAperta==true&&$admin==1){
          echo "<form action='modificaEffettuata.php' method='post'>";
      }
      echo '
      <ul id="ricerca">';
while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
  if($sessioneAperta==true&&$admin==1){
    $adminString1 = "
      <div>
        <button type='submit' name='modificaGiol' class='modificaGiol' value='".$row['Id']."'>Modifica</button>
        <input type='checkbox' name='eliminaGiol[]' value='".$row['Id']."'/>Elimina
      </div>";
    $adminString2 = "
      <div id='eliminaDiv' >
        <label for='eliminaG' id='eliminaLabel'>
        <button type='submit' name='eliminaG' id='eliminaG' value='elimina'>Elimna</button></label>
      </div>
    </form>";
  }
  if($row["Esclusiva"]==1){
    echo "<li class='elementoGioielliRicerca EsclusivaRicerca'><h2><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h2>
              <a href=\"gioiello.php?id=".$row['Id']."\" class='linkDisabilitato'><span class='nascosto'>Esclusiva</span><img src='".$row['Immagini']."' alt=".'"'.$row['Descrizione'].'"'. " class='immaginePreda' width='260px' height='260px'/></a>
              ".$adminString1."
              </li>";
    }else{
    echo "<li class='elementoGioielliRicerca'>

       <h3><a href=\"gioiello.php?id=".$row['Id']."\">".$row['Nome']."</a></h3><a href=\"gioiello.php?id=".$row['Id']."\"><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\"  width='260px' height='260px' /></a>
       ".$adminString1."
       </li>";
     }
}
echo "</ul>".$adminString2;
}

echo "";
$arrayRisultato->free();

echo "<div id='divNumeriFondoPagina'>

<a href='#footer' class='salto'>Salta i numeri di cambio pagina e vai al <span xml:lang='en'>footer</span></a>
<a href='#menu' class='salto'>Ritorna al menu</a>
<ul id='numeriFondoPagina'>";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if(intval($risultato/10) == 0)
    $ri = 1;
  else{
    if($risultato%10>0){
        $ri = intval($risultato/10) +1;
    }
    else {
      $ri = $risutato/10;
    }
  }

for ($i=0; $i <intval($ri); $i++) {
  if(!isset($_GET["numero"])){
    if($i+1==1){
      echo"<li class='elementoNumeroGioiello active'>
      <span class='active'>".($i+1)."</span>
      </li>";
    }
    else{
      echo"<li class='elementoNumeroGioiello'>
      <a href=\"ricerca.php?ricercaForm=".$_GET['ricercaForm']."&amp;numero=".($i+1)."\">".($i+1)."</a>
      </li>";
    }
}
else{
  if($_GET["numero"]==($i+1)){
    echo"<li class='elementoNumeroGioiello active'>
    <span class='active'>".($i+1)."</span>
    </li>";
  }
  else{
    echo"<li class='elementoNumeroGioiello'>
    <a href=\"ricerca.php?ricercaForm=".$_GET['ricercaForm']."&amp;numero=".($i+1)."\">".($i+1)."</a>
    </li>";
  }
}
}
}
echo"</ul>";

}

}
echo"</div>";


} catch (Exception $e) {
   echo $e->getMessage();

}

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);

 ?>
