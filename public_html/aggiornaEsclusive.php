<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/UpdateInterrogation.php');

    $a1 = "Aggiorna Esclusive - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;

    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];

    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;
        <a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a>&gt; Aggiornamento&nbsp;Esclusiva</p>';
    }
    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
    try {


    if($sessioneAperta== true){
        $mInterrogation = new SelectInterrogation(
            array("COUNT(*) AS CONTEGGIO"),
            array("utente"),array("Admin = 0"));
        try{
            $arrayInterrogationm = $mInterrogation->interrogation();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $risultato = 0;
        if($arrayInterrogationm->num_rows > 0){
            while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                $risultato+=$row['CONTEGGIO'];
            }
        }
        $arrayInterrogationm->free();

        $limite1 = 0;
        $limite2 = 30;
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            if(!empty($_GET["numero"])){
                    $limite1 = ($_GET["numero"]-1)*30;

            }
        }
        $mInterrogation = new SelectInterrogation(
            array("Nome","Email"),array("utente"),array("Admin = 0 LIMIT ".$limite1.",".$limite2));
        try{
            $arrayInterrogationm = $mInterrogation->interrogation();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        echo "<a href='#footer' class='salto'>Salta la form di eliminazione e aggiunta esclusiva e vai al <span xml:lang='en'>footer</span></a>
            <a href='#menu' class='salto'>Ritorna al menu</a>
            <form action='modificaEsclusive.php' method='post'>";
        if($arrayInterrogationm->num_rows > 0){
            echo "
           <ul id ='listaEliminazione'>
            ";

              while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                  $rowID=str_replace("@","",$row['Email']);
               echo "
               <li class='elementoEliminazione'>
               <h2>".$row['Email']."</h2>
               <p>Nome Utente : ".$row['Nome']."</p>
               <label for='utentiEs".$rowID."'><input id='utentiEs".$rowID."' type='radio' name='utentiEs' value='".$row['Email']."'/>Seleziona ".$row['Email']."</label>
               </li>";
              }
              echo "</ul>";
        }

        if($sessioneAperta== true){
        echo"<div id='divBottoni'>
        <button type='submit' name='id' value='eliminaGioielloEsclusiva'>Elimina un gioiello in esclusiva</button>
        <button type='submit' name='id' value='eliminaCollezioneEsclusiva'>Elimina una collezione in esclusiva</button>
        <button type='submit' name='id' value='aggiungiGioielloEsclusiva'>Aggiungi un nuovo gioiello in esclusiva</button>
        <button type='submit' name='id' value='aggiungiCollezioneEsclusiva'>Aggiungi una nuova collezione in esclusiva</button>
        </div></form>";
      }

        $arrayInterrogationm->free();
        echo "<ul id='numeriFondoPagina'>";
        if(intval($risultato/30) == 0)
            $ri = 1;
        else{
            if($risultato%30>0){
                $ri = intval($risultato/30) +1;
            }
            else {
                $ri = $risutato/30;
            }
        }
        for ($i=0; $i < intval($ri); $i++) {
          if(!isset($_GET["numero"])){
            if($i+1==1){
            echo"<li class='elementoNumeroGioiello active'>
            <span class='active'>".($i+1)."</span>
            </li>";
            }
            else{
              echo"<li class='elementoNumeroGioiello'>
                <a href=\"aggiornaEsclusive.php?numero=".($i+1)."\">".($i+1)."</a>
                </li>";
            }

          }
          else{
            if($_GET["numero"]==($i+1)){

              echo"<li class='elementoNumeroGioiello active'>
              <span class='active'>".($i+1)."</span>
              </li>";
            }
            else{
              echo"<li class='elementoNumeroGioiello'>
                <a href=\"aggiornaEsclusive.php?numero=".($i+1)."\">".($i+1)."</a>
                </li>";
            }
          }


        }
        echo"</ul>";
    }
} catch (Exception $e) {
    echo $e->getMessage();

}
    require_once("Parti/footer.php");
    echo printfooter($sessioneAperta);
?>
