<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');

    $a1 = "Lista Collezioni - AL.JO. Gioielli Center";

    if ($_SERVER["REQUEST_METHOD"] == "GET") {
      if(!empty($_GET["tipo"])){
        if(!empty($_GET["ord"])){
          $a1 = "Lista Collezioni ".$_GET["tipo"]." ".$_GET["ord"]." - AL.JO. Gioielli Center";
        }
        else{
          $a1 = "Lista Collezioni ".$_GET["tipo"]." - AL.JO. Gioielli Center";
        }
      }
      else{
        if(!empty($_GET["ord"])){
          $a1 = "Lista Collezioni ".$_GET["ord"]." - AL.JO. Gioielli Center";
        }
      }
    }

    $a2 = "Pagina in cui c'è una lista di collezioni
        in cui se ne può selezionare una per andare nel dettaglio";
    $a3 = "Collezioni, AL.JO., Collane, Bracciali, Anelli, Vicenza";
    $a4 = "index,follow";
    $stringAdmin1 = "";
    $stringAdmin2 = "";
    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li class="active">Collezioni</li>
    <li><a href="informazioni.php">Informazioni</a></li>';

      $sessioneAperta = false;
      $sezioneLogin = "";


    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }

    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
    }

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Lista Collezioni</p>';

    echo printHTML($menu,$sessioneAperta,$sezioneLogin,true,$breadCrumb);

    try {

    $stringaDiQuery = $_SERVER['QUERY_STRING'];
    $stringaOrder = "";
    $stringaOrdURL = "";
    $countMatchAsc = preg_match('/(asc)/',$stringaDiQuery);
    $countMatchDesc = preg_match('/(desc)/',$stringaDiQuery);
    $countMatchNum = preg_match('/numero/',$stringaDiQuery);
    $countMatchTipo = preg_match('/tipo/',$stringaDiQuery);
    $lastInterrogation = "";
    $risultato=0;

    if($countMatchDesc>0){
        $stringaOrdURL = "ord=descNm&";
        $stringaOrder = " ORDER BY Nome DESC";
    }
    elseif ($countMatchAsc>0) {
        $stringaOrdURL = "ord=ascNm&";
        $stringaOrder = " ORDER BY Nome ASC";
    }
    if($countMatchAsc>0||$countMatchDesc>0||$countMatchNum>0){

        $pieces = explode("&", $stringaDiQuery);
        if(count($pieces)==1){
            $stringaDiURL = "lista_collezioni.php?";
        }
        else {
            if($countMatchTipo>0)
                $stringaDiURL = "lista_collezioni.php?".$pieces[0]."&";
            else {
                $stringaDiURL = "lista_collezioni.php?";
        }
    }

    }else {
        if($stringaDiQuery==""){


            $stringaDiURL = "lista_collezioni.php?";
        }
        else{
            $stringaDiURL = "lista_collezioni.php?".$stringaDiQuery."&amp;";
        }
    }
    echo '<div id="cacciatore" class="divZoom">
      <span id="chiusura">&times;</span>
      <img src="img/broken.jpg" class="contenutoImmagine" id="immagineCacciata" alt="Immagine di default"/>
      <div id="altImmagine"></div>
    </div>';



    if($sessioneAperta==true){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
          echo "<div id ='menuListaGioielli'><a href='#menuOrdinamentoCollezioni' class='salto'>Salta il menu di selezione del tipo di collezione e vai al menu d'ordinamento</a>
          <a href='#menu' class='salto'>Ritorna al menu</a>
              <ul id='menuGioielli'>";
            if(empty($_GET["tipo"])){

                    echo"<li class='ElementoMenuGioiello active'><span class=\"active\">Tutti</span></li>
                    <li class='ElementoMenuGioiello'><a href=\"lista_collezioni.php?tipo=esclusive\">Esclusive</a></li>";
                  }
                  else{
                        if($_GET["tipo"] == "esclusive"){
                            $stringaOrdURL = "tipo=esclusive&".$stringaOrdURL;
                            $lastInterrogation = "collezione.Esclusiva = 1";

                          echo"<li class='ElementoMenuGioiello'><a href=\"lista_collezioni.php\">Tutti</a></li>
                          <li class='ElementoMenuGioiello  active'><span class=\"active\">Esclusive</span></li> ";
                        }
                        else{
                            echo"<li class='ElementoMenuGioiello'><a href=\"lista_collezioni.php\">Tutti</a></li>
                            <li class='ElementoMenuGioiello'><a href=\"lista_collezioni.php?tipo=esclusive\" >Esclusive</a></li> ";
                      }
                }
                echo"</ul>
                </div>";
            }
    }

    echo "<div id ='menuOrdinamentoCollezioni'>
      <a href='#listaPaginaCollezioni' class='salto'>Salta il menu d'ordinamento della lista collezioni e vai alla lista delle collezioni</a>
      <a href='#menu' class='salto'>Ritorna al menu</a>
            <ul id='menuOrdinamento'>
                <li class='ElementoOrdinamentoCollezioni'>
                    Nome <a href='".$stringaDiURL."ord=ascNm'><abbr title='ordina in base al nome dalla A alla Z'>A-Z</abbr></a>
                    <a href='".$stringaDiURL."ord=descNm'><abbr title='ordina in base al nome dalla Z alla A'>Z-A</abbr></a>
                </li>
            </ul>
          </div>";

    $selectI = array("COUNT(*) AS CONTEGGIO");
    $fromI = array("collezione");
    $whereI = array();



    if($sessioneAperta == true){
        if($admin==1){
                if(strlen($lastInterrogation) > 0)
                    array_push($whereI,$lastInterrogation);
                else {
                    array_push($whereI,"1");
                }
        }
        else{
            if(strlen($lastInterrogation) > 0){
                array_push($fromI, "esclusivacollezione");
                array_push($whereI,"esclusivacollezione.Utente ='".$email."'",
                "AND","esclusivacollezione.Collezione = collezione.Nome");
            }
            else {
                array_push($whereI,"Esclusiva = 0");
                array_push($whereI," UNION  SELECT COUNT(*) AS CONTEGGIO
                FROM collezione JOIN esclusivacollezione ON
                collezione.Nome = esclusivacollezione.Collezione
                WHERE Esclusiva = 1 AND esclusivacollezione.Utente ='".$email."'");
            }


        }

    }else {
        array_push($whereI,"Esclusiva=0");
    }

    $limite1 = 0;
    $limite2 = 12;
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if(!empty($_GET["numero"])){
                $limite1 = ($_GET["numero"]-1)*12;

        }
    }

    $selectI = array("Nome","Descrizione","Esclusiva","Immagini");

    if($sessioneAperta == true){
        if($admin==0){
            if(strlen($lastInterrogation)==0){
                $whereI[count($whereI)-1] = " UNION  SELECT Nome , Descrizione , Esclusiva, Immagini
                FROM collezione JOIN esclusivacollezione ON
                collezione.Nome = esclusivacollezione.Collezione
                WHERE Esclusiva = 1 AND esclusivacollezione.Utente ='".$email."'";
            }
        }
        if(strlen($stringaOrder)==0)
            $stringaOrder = " ORDER BY Esclusiva DESC ";
    }



    $Select = new SelectInterrogation($selectI,$fromI,$whereI);

    try {
        $arrayRisultato = $Select->interrogation();
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }

    $risultato = $arrayRisultato->num_rows;

    $arrayRisultato->free();
    array_push($whereI," ".$stringaOrder." ","LIMIT ".$limite1.",".$limite2);
    $Select = new SelectInterrogation($selectI,$fromI,$whereI);

        try {
            $arrayRisultato = $Select->interrogation();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }


    echo '<div id="listaPaginaCollezioni">
    <a href="#divNFP" class="salto">Salta la lista di gioielli e vai ai numeri di cambio pagina</a>
    <a href="#menu" class="salto">Ritorna al menu</a>
      ';
    if($arrayRisultato->num_rows > 0){
        if($sessioneAperta==true&&$admin==1){
            echo "<form action='modificaEffettuata.php' method='post'>";

        }
        echo '<ul id="listaCollezioni">';
        while($row = $arrayRisultato->fetch_array(MYSQLI_ASSOC)){
            if($sessioneAperta==true){
                if($admin==1){
                    $stringAdmin1 = "
                        <div>
                            <label for='modificaCool".$row['Nome']."'><button id='modificaCool".$row['Nome']."' type='submit' name='modificaColl'
                            class='modificaColl' value='".$row['Nome']."'>
                                Modifica
                            </button></label>
                            <label for='eliminaColl".$row['Nome']."'><input id='eliminaColl".$row['Nome']."' type='checkbox' name='eliminaColl[]'
                             value='".$row['Nome']."'/>Elimina ".$row['Nome']."</label>
                        </div>";
                    $stringAdmin2 = "
                        <div id='eliminaDiv' >
                            <label for='eliminaC' id='eliminaLabel'>
                            <button type='submit' name='eliminaC' id='eliminaC'
                             value='elimina'>Elimina</button></label>
                        </div>
                    </form>";
                }
                if($row['Esclusiva']==1)
                    echo "<li class='elementoCollezione Esclusiva'><span class='nascosto'>Esclusiva</span>";
                else {
                    echo "<li class='elementoCollezione'>";
                }
            }
            else {
                echo "<li class='elementoCollezione'>";

            }
            echo "

            <h2><a href=\"collezione.php?name=".$row['Nome']."\">".$row['Nome']."</a></h2>
            <a href=\"collezione.php?name=".$row['Nome']."\" class='linkDisabilitato'><img src='".$row['Immagini']."' alt=\"".$row['Descrizione']."\" class='immaginePreda' width='260px' height='260px'/></a>
            ".$stringAdmin1."</li>";
        }
        echo "</ul>".$stringAdmin2;
    }
    $arrayRisultato->free();


    echo "<div id='divNFP'>
    <a href='#footer' class='salto'>Salta la lista di gioielli e vai al <span xml:lang='en'>footer</span></a>
    <a href='#menu' class='salto'>Ritorna al menu</a>
    <ul id='numeriFondoPagina'>";
    if(intval($risultato/12) == 0)
        $ri = 1;
    else{
        if($risultato%12>0){
            $ri = intval($risultato/12) +1;
        }
        else {
            $ri = $risutato/12;
        }
    }
    for ($i=0; $i < intval($ri); $i++) {
      if(!isset($_GET["numero"])){
        if($i+1==1){
        echo"<li class='elementoNumeroGioiello active'>
        <span class='active'>".($i+1)."</span>
        </li>";
        }
        else{
          echo"<li class='elementoNumeroGioiello'>
            <a href=\"lista_collezioni.php?".$stringaOrdURL."numero=".($i+1)."\">".($i+1)."</a>
            </li>";
        }

      }
      else{
        if($_GET["numero"]==($i+1)){

          echo"<li class='elementoNumeroGioiello active'>
          <span class='active'>".($i+1)."</span>
          </li>";
        }
        else{
          echo"<li class='elementoNumeroGioiello'>
            <a href=\"lista_collezioni.php?".$stringaOrdURL."numero=".($i+1)."\">".($i+1)."</a>
            </li>";
        }
      }


    }
    echo"</ul></div></div>";

    } catch (Exception $e) {
        echo $e->getMessage();
    }

    require_once('Parti/footer.php');
    echo printfooter($sessioneAperta);
    ?>
