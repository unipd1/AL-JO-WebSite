<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');

    $a1 = "Sezione Dedicata - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";
    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;

    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }
    $sessioneAperta = true;
    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li class="active"><span class="active">Sezione&nbsp;Dedicata</span></li>';
    }
    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; Sezione Dedicata</p>';

    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);

    if($sessioneAperta== true){

      echo file_get_contents('Parti/sessioneBody.html');

    };

    require_once("Parti/footer.php");
    echo printfooter($sessioneAperta);
    ?>
