<?php
    session_start();

    require_once("Parti/stampaHTML.php");
    require_once('../php/SelectInterrogation.php');
    require_once('../php/InsertInterrogation.php');
    require_once('../php/UpdateInterrogation.php');

    $sessioneAperta = false;
    $sezioneLogin = "";
    $admin = 0;
    if(isset($_SESSION['username'])&&isset($_SESSION['email'])&&isset($_SESSION['password'])){
          $sessioneAperta = true;
          $sezioneLogin =$_SESSION['username'];
          $email = $_SESSION['email'];
          $admin = $_SESSION['admin'];
    }
    $a1 = "Modifica";

    if($sessioneAperta== true){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
          if(!empty($_GET["del"])){
              if ($_GET['del']=='mat'){$a1 =$a1." materiale";}
              if ($_GET['del']=='ptr'){$a1 =$a1." pietra";}
              if ($_GET['del']=='dim'){$a1 =$a1." dimensione";}
              if ($_GET['del']=='ute'){$a1 =$a1." utente";}
          }
        }
    }

    $a1 =$a1." - AL.JO. Gioielli Center";
    $a2 = "";
    $a3 = "";
    $a4 = "noindex,follow";

    echo printHeadHTML($a1,$a2,$a3,$a4);


    $menu = '<li xml:lang="en"><a href="index.php">Home</a></li>
    <li><a href="lista_gioielli.php">Gioielli</a></li>
    <li><a href="lista_collezioni.php">Collezioni</a></li>
    <li><a href="informazioni.php">Informazioni</a></li>';

    $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a>&gt;Pagina&nbsp;non&nbsp;trovata</p>';
    if($sessioneAperta== true){
        if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
        $breadCrumb ='<p>Ti trovi in : <a href="index.php" xml:lang="en">Home</a> &gt; <a href="sezioneDedicata.php">Sezione Dedicata</a> &gt; Modifica/Rimuovi</p>';
    }

    echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);
    try{




    $interrogazioneS = "";
    $interrogazioneF = "";
    $interrogazioneW = "";

    if($sessioneAperta== true){

        if ($_SERVER["REQUEST_METHOD"] == "GET") {
          if(!empty($_GET["del"])){
              $risultato = 0;
              if ($_GET['del']=='mat') {
                 $getter = "del=mat";
                 $mInterrogation = new SelectInterrogation(
                     array("COUNT(*) AS CONTEGGIO"),
                     array("materiale"),array(
                         "Nome not in (SELECT Nome_Materiale
                         FROM prodotto ) AND Colore not in
                         (SELECT Colore_Materiale
                         FROM prodotto )"));
                try{
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }

                 $risultato = 0;
                 if($arrayInterrogationm->num_rows > 0){
                     while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                         $risultato+=$row['CONTEGGIO'];
                     }
                 }
                 $arrayInterrogationm->free();

                 $limite1 = 0;
                 $limite2 = 30;
                 if ($_SERVER["REQUEST_METHOD"] == "GET") {
                     if(!empty($_GET["numero"])){
                             $limite1 = ($_GET["numero"]-1)*30;

                     }
                 }

                 $mInterrogation = new SelectInterrogation(
                     array("Nome","Colore"),array("materiale"),array(
                         "Nome not in (SELECT Nome_Materiale
                         FROM prodotto ) AND Colore not in
                         (SELECT Colore_Materiale
                         FROM prodotto ) LIMIT ".$limite1.",".$limite2));
                         try {
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }
                 if($arrayInterrogationm->num_rows > 0){
                     echo "<form action='modificaEffettuata.php' method='post'>
                     <div id='divListaEliminazione'>
                     <a href='#divNFP' class='salto' >Salta la lista di elementi da eliminare e vai numeri di cambio pagina</a>
                     <a href='#menu' class='salto'>Ritorna al menu</a>
                     <ul id ='listaEliminazione'>

                     ";
                       while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                           echo "<li class='elementoEliminazione'>
                           <h2>".$row['Nome']."</h2>
                           <p>Colore Materiale : ".$row['Colore']."</p>
                           <label for='eliminaMat".$row['Nome'].$row['Colore']."'><input id='eliminaMat".$row['Nome'].$row['Colore']."' type='checkbox' name='eliminaMat[]' value='".$row['Nome']."@".$row['Colore']."'/>Elimina ".$row['Nome']."/".$row['Colore']."</label>
                           </li>";
                   }
                   echo "</ul></div>
                   <div id='eliminaDiv' >
                   <label for='eliminaM' id='eliminaLabel'>
                   <button type='submit' name='eliminaM' id='eliminaM' value='elimina'>Elimina</button></label>
                   </div>
                        </form>";
                 }

                 $arrayInterrogationm->free();
             }
             elseif ($_GET['del']=='ptr') {
                 $getter = "del=ptr";
                 $mInterrogation = new SelectInterrogation(
                     array("COUNT(*) AS CONTEGGIO"),
                     array("pietra"),array(
                         "Nome not in (SELECT Nome_Pietra
                         FROM ornamento ) AND Colore not in
                         (SELECT Colore_Pietra
                         FROM ornamento )"));
                         try{
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }

                 $risultato = 0;
                 if($arrayInterrogationm->num_rows > 0){
                     while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                         $risultato+=$row['CONTEGGIO'];
                     }
                 }
                 $arrayInterrogationm->free();

                 $limite1 = 0;
                 $limite2 = 30;
                 if ($_SERVER["REQUEST_METHOD"] == "GET") {
                     if(!empty($_GET["numero"])){
                             $limite1 = ($_GET["numero"]-1)*30;

                     }
                 }


                 $mInterrogation = new SelectInterrogation(
                     array("Nome","Colore"),
                     array("pietra"),
                     array("Nome not in (SELECT Nome_Pietra
                     FROM ornamento ) AND Colore not in
                     (SELECT Colore_Pietra
                     FROM ornamento )  LIMIT ".$limite1.",".$limite2));
                     try {
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }
                 if($arrayInterrogationm->num_rows > 0){
                     echo "<form action='modificaEffettuata.php' method='post'>
                     <div id='divListaEliminazione'>
                     <a href='#divNFP' class='salto' >Salta la lista di elementi da eliminare e vai numeri di cambio pagina</a>
                     <a href='#menu' class='salto'>Ritorna al menu</a>
                  <ul id ='listaEliminazione'>

                     ";
                       while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                        echo "
                        <li class='elementoEliminazione'>
                        <h2>".$row['Nome']."</h2>
                        <p>Colore Pietra : ".$row['Colore']."</p>
                        <label for='eliminaPtr".$row['Nome'].$row['Colore']."'><input id='eliminaPtr".$row['Nome'].$row['Colore']."' type='checkbox' name='eliminaPtr[]' value='".$row['Nome']."@".$row['Colore']."'/>Elimina ".$row['Nome']."/".$row['Colore']."</label>
                        </li>";
                       }

                       echo "</ul></div>
                       <div id='eliminaDiv' >
                       <label for='eliminaP' id='eliminaLabel'>
                       <button type='submit' name='eliminaP' id='eliminaP' value='elimina'>Elimina</button></label>
                       </div>
                       </form>";
                 }

                 $arrayInterrogationm->free();
             }
             elseif ($_GET['del']=='dim') {
                 $getter = "del=dim";
                 $mInterrogation = new SelectInterrogation(
                     array("COUNT(*) AS CONTEGGIO"),
                     array("dimensione"),array("Codice not in (
                         SELECT Dimensione FROM taglia
                     ) AND Codice not in (
                         SELECT Dimensione FROM pietra
                     ) "));
                     try {
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }

                 $risultato = 0;
                 if($arrayInterrogationm->num_rows > 0){
                     while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                         $risultato+=$row['CONTEGGIO'];
                     }
                 }
                 $arrayInterrogationm->free();

                 $limite1 = 0;
                 $limite2 = 30;
                 if ($_SERVER["REQUEST_METHOD"] == "GET") {
                     if(!empty($_GET["numero"])){
                             $limite1 = ($_GET["numero"]-1)*30;

                     }
                 }

                 $mInterrogation = new SelectInterrogation(
                     array("Codice","Tipologia","UnitaDiMisura","Grandezza"),
                     array("dimensione"),array("Codice not in (
                         SELECT Dimensione FROM taglia
                     ) AND Codice not in (
                         SELECT Dimensione FROM pietra
                     )  LIMIT ".$limite1.",".$limite2));
                     try{
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }
                 if($arrayInterrogationm->num_rows > 0){
                     echo "<form action='modificaEffettuata.php' method='post'>
                     <div id ='divListaEliminazione'>
                     <a href='#divNFP' class='salto' >Salta la lista di elementi da eliminare e vai numeri di cambio pagina</a>
                     <a href='#menu' class='salto'>Ritorna al menu</a>

                    <ul id ='listaEliminazione'>

                    ";
                       while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                        echo "
                        <li class='elementoEliminazione'>
                        <h2>".$row['Codice']."</h2>
                        <p>Tipologia : ".$row['Tipologia']."</p>
                        <p>Grandezza : ".$row['Grandezza'].$row['UnitaDiMisura']."</p>
                        <label for='eliminaDim".$row['Codice']."'><input id='eliminaDim".$row['Codice']."' type='checkbox' name='eliminaDim[]' value='".$row['Codice']."'/>Elimina ".$row['Codice']."</label>
                        </li>";
                       }
                       echo "</ul></div>
                       <div id='eliminaDiv' >
                       <label for='eliminaD' id='eliminaLabel'>
                       <button type='submit' name='eliminaD' id='eliminaD' value='elimina'>Elimina</button></label>
                       </div>
                       </form>";
                 }

                 $arrayInterrogationm->free();
             }
             elseif ($_GET['del']=='ute') {
                 $getter = "del=ute";
                 $mInterrogation = new SelectInterrogation(
                     array("COUNT(*) AS CONTEGGIO"),
                     array("utente"),array("Admin = 0"));
                     try {
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }

                 $risultato = 0;
                 if($arrayInterrogationm->num_rows > 0){
                     while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                         $risultato+=$row['CONTEGGIO'];
                     }
                 }
                 $arrayInterrogationm->free();

                 $limite1 = 0;
                 $limite2 = 30;
                 if ($_SERVER["REQUEST_METHOD"] == "GET") {
                     if(!empty($_GET["numero"])){
                             $limite1 = ($_GET["numero"]-1)*30;

                     }
                 }

                 $mInterrogation = new SelectInterrogation(
                     array("Nome","Email"),array("utente"),array("Admin = 0 LIMIT ".$limite1.",".$limite2));
                try {
                 $arrayInterrogationm = $mInterrogation->interrogation();
             } catch (Exception $e) {
                 throw new Exception($e->getMessage());
             }
                 if($arrayInterrogationm->num_rows > 0){
                     echo "<form action='modificaEffettuata.php' method='post'>
                     <div id ='divListaEliminazione'>

                     <a href='#divNFP' class='salto' >Salta la lista di elementi da eliminare e vai numeri di cambio pagina</a>
                     <a href='#menu' class='salto'>Ritorna al menu</a>
                    <ul id ='listaEliminazione'>

                    ";
                       while($row = $arrayInterrogationm->fetch_array(MYSQLI_ASSOC)){
                           $rowID=str_replace("@","",$row['Email']);
                        echo "
                        <li class='elementoEliminazione'>
                        <h2>".$row['Email']."</h2>
                        <p>Nome Utente : ".$row['Nome']."</p>
                        <label for='eliminaUtente".$rowID."'><input id='eliminaUtente".$rowID."' type='checkbox' name='eliminaUtente[]' value='".$row['Email']."'/>Elimina ".$row['Email']."</label>
                        </li>";
                       }
                       echo "</ul></div>
                       <div id='eliminaDiv' >
                       <label for='eliminaU' id='eliminaLabel'>
                       <button type='submit' name='eliminaU' id='eliminaU' value='elimina'>Elimina</button></label>
                       </div>
                       </form>";
                 }

                 $arrayInterrogationm->free();
             }
             echo "<div id='divNFP'>
             <a href='#footer' class='salto'>Salta i numeri di cambio pagina e vai al <span xml:lang ='en'>footer</span></a>
             <a href='#menu' class='salto'>Ritorna al menu</a>
              <ul id='numeriFondoPagina'>

             ";
             if(intval($risultato/30) == 0)
                 $ri = 1;
             else
                 if($risultato%30>0){
                     $ri = intval($risultato/30) +1;
                 }
                 else {
                     $ri = $risutato/30;
                 }
             for ($i=0; $i < intval($ri); $i++) {
               if(!isset($_GET["numero"])){
                 if($i+1==1){
                 echo"<li class='elementoNumeroGioiello active'>
                 <span class='active'>".($i+1)."</span>
                 </li>";
                 }
                 else{
                   echo"<li class='elementoNumeroGioiello'>
                     <a href=\"modifica.php?".$getter."&numero=".($i+1)."\">".($i+1)."</a>
                     </li>";
                 }

               }
               else{
                 if($_GET["numero"]==($i+1)){

                   echo"<li class='elementoNumeroGioiello active'>
                   <span class='active'>".($i+1)."</span>
                   </li>";
                 }
                 else{
                   echo"<li class='elementoNumeroGioiello'>
                     <a href=\"modifica.php?".$getter."&numero=".($i+1)."\">".($i+1)."</a>
                     </li>";
                 }
               }


             }
             echo"</ul></div>";
      }
    }
}
} catch (Exception $e) {
    echo $e->getMessage();
}
require_once("Parti/footer.php");
echo printfooter($sessioneAperta);
?>
