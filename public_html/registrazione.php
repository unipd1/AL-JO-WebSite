<?php
require_once("Parti/stampaHTML.php");
require_once('../php/InsertInterrogation.php');

$a1 = "Registrazione risultato - AL.JO. Gioielli Center";
$a2 = "";
$a3 = "";

$a4 = "noindex,nofollow";
echo printHeadHTML($a1,$a2,$a3,$a4);


$menu = '<li><a href="index.php" xml:lang="en">Home</a></li>
         <li><a href="lista_gioielli.php">Gioielli</a></li>
         <li><a href="lista_collezioni.php">Collezioni</a></li>
         <li><a href="informazioni.php">Informazioni</a></li>
         ';

$sessioneAperta = false;
$sezioneLogin = "";
$admin = 0;
if(isset($_SESSION['username'])){
      $sessioneAperta = true;
      $sezioneLogin =$_SESSION['username'];
      $admin = $_SESSION['admin'];
}

if($sessioneAperta== true){
    if($admin==1)$menu = $menu.'<li><a href="sezioneDedicata.php">Sezione&nbsp;Dedicata</a></li>';
}

$breadCrumb ='<p>Ti trovi in :<a href="index.php" xml:lang="en">Home</a> &gt; Registrtazione &gt; Successo o Errore</p>';
echo printHTML($menu,$sessioneAperta,$sezioneLogin,false,$breadCrumb);


if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(empty($_POST["RUsername"]) or empty($_POST["email"]) or empty($_POST["RPassword"]) or empty($_POST["RCPassword"])){
    echo "<p>Errore Inserimento Dati</p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
  }
  else {
    $bol = true;
    if(preg_match("/^([\w\-\+\.]+)@([\w\-\+\.]+).([\w\-\+\.]+)$/", $_POST["email"]) == 0){
      $bol = false;
      echo "<p>Errore in <span xml:lang='en'>E-mail</span>, l'<span xml:lang='en'>email</span> deve essere ben formata.</p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
    }
    if(preg_match("/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/", $_POST["RPassword"]) == 0 && $bol){
      $bol = false;
      echo "<p>Errore in <span xml:lang='en'>Password</span>, la <span xml:lang='en'>password</span> deve essere composta da 8 caraterri, una lettera maiuscola e un numero.</p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
    }
    if(preg_match("/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/", $_POST["RCPassword"]) == 0 && $bol){
      $bol = false;
      echo "<p>Errore in Ripeti <span xml:lang='en'>password</span>, la <span xml:lang='en'>password</span> deve essere composta da 8 caraterri, una lettera maiuscola e un numero.</p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
    }
    if($_POST["RPassword"]==$_POST["RCPassword"] && $bol){
      $a1 =["utente"];
      $a2 =["Email","Password","Nome"];
      $a3 =[array($_POST["email"],$_POST["RPassword"],$_POST['RUsername'])];
      $insert = new InsertInterrogation($a1,$a2,$a3);
      try {
        $insert->interrogation();
        echo "<p>Inserimento Dati avvenuto con successo</p>";
      } catch (Exception $e) {
        if($e->getMessage() == "DPE")
          echo "<p>Errore <span xml:lang='en'>e-mail</span> gi&agrave; inserita </p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
        else {
          throw new Exception($e->getMessage(),1);
        }
      }
    }
    else {
      if($bol)echo "<p>Errore <span xml:lang='en'>Password</span> inserite diverse</p><p>Torna indietro <a href='registrazione.html'>Registrazione</a></p>";
    }
  }
}

require_once('Parti/footer.php');
echo printfooter($sessioneAperta);

 ?>
