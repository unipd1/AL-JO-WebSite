<?php
/**
 *
 */
require_once("Interrogation.php");

class InsertInterrogation extends Interrogation {

  //-------------------------------campi dati-----------------------------------

  // tabella SQL Select
  private $insertArray =[];
  private $insertText = "";
  private $valuesArray =[];
  private $valuesText = "";
  private $interSQL = "";

  //-------------------------------funzioni-------------------------------------

  // costruttore

  function __construct($arrayTable,$arrayInsert,$arrayValues){
    parent::__construct($arrayTable);
    $errorInsert = $this->setInsert($arrayInsert);
    $this->setError($errorInsert);
    $errorValues = $this->setValues($arrayValues);
    $this->setError($errorValues);
    $this->concatenationText();
  }

  // funzioni set
  function setInsert($input){
    $errorIns="";
    if(!(is_array($input))){
      $errorIns = "Formato passaggio parametri non corretto";
      return $errorIns;
    }
    for ($i=0; $i < count($input)&& strlen($errorIns)==0 ; $i++) {
        if(!ctype_alpha(str_replace("_","",$input[$i]))){
          $errorIns = "Formato passaggio parametri non corretto";
        }
    }
    if(strlen($errorIns)==0){
      $this->insertArray = $input;
    }
    return $errorIns;
  }

  function setValues($input){
    $errorVl="";
    if(!(is_array($input))){
      $errorVl = "Formato passaggio parametri non corretto 1";
      return $errorVl;
    }
    for ($i=0; $i < count($input)&& strlen($errorVl)==0 ; $i++) {
        if(!is_array($input[$i]))
          $errorVl = "Formato passaggio parametri non corretto 2";
        else {
          foreach ($input[$i] as $key => $value) {
            if(!preg_match("([(\w)\.(\w)]|\[w])",$value))
              if(!is_numeric($value))
                $errorVl = "Formato passaggio parametri non corretto 3";
          }
        }
    }
    if(strlen($errorVl)==0){
      $this->valuesArray = $input;
    }
    return $errorVl;
  }

  // funzione per interrogazione al Database
  function interrogation(){
    try {
        $this->connDatabase();
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    $connessione = $this->getConnession();

    if(!$result = $connessione->query($this->interSQL)){

        if ($connessione->errno == 1062) {
          throw new Exception("<p>Errore di inserimento, l&#39;elemento &egrave; gi&agrave; presente.</p>");
        }
        else
          throw new Exception("<p>Ops, c&#39;&egrave;  stato un errore, siamo spiacenti, la preghiamo di ripovare pi&ugrave; tardi.</p>", 1);

      }
      else {
        $this->connectionClose();
      }
  }

  // funzione concatenazione stringhe
  function concatenationText(){
    parent::concatenationText();
    if(!empty($this->insertArray))
      $this->insertText = array_reduce($this->insertArray,function($a,$b){
        if($a)
          return $a." ,".$b;
        else
          return $b;
      });
      if(!empty($this->valuesArray))
        foreach ($this->valuesArray as $key => $value) {
          $this->valuesText = $this->valuesText."(".array_reduce($value,function($c,$d){
            if($c){
              if(is_string($c))
                if(is_string($d))
                  return $c.", "."'".$d."'";
                else
                  return $c.", ".$d;
              else
                if(is_string($d))
                  return $c." ,"."'".$d."'";
                else
                  return $c.", ".$d;
            }
            else{
              if(is_string($d))
                return "'".$d."'";
              else
                return $d;
            }
          }).")";
          if(count($this->valuesArray)>$key+1)
            $this->valuesText=$this->valuesText.",";
      }
        $this->interSQL = "INSERT INTO ".$this->getTable()."(".$this->insertText.")"." VALUES ".$this->valuesText;
    }


}

 ?>
