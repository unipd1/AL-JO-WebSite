<?php
/**
 *
 */
abstract class Interrogation {

  //-------------------------------campi dati-----------------------------------

  // connessione
  private $host = "localhost";
  private $user = "root";
  private $password = "";
  private $db = "al.jo.db";
  private $connessione;

  // tabella SQL
  private $tableArray =[];
  private $tableText = "";
  private $whereArray =[];
  private $whereText = "";
  private $error = "";

  //-------------------------------funzioni-------------------------------------

  // costruttore

  function __construct(){
    $a = func_get_args();
    $i = func_num_args();
    if (method_exists($this,$f='__construct'.$i)) {
           call_user_func_array(array($this,$f),$a);
       }
  }

  function __construct1($arrayTable){
    $errorTable = $this->setTable($arrayTable);
    $this->setError($errorTable);
  }

  function __construct2($arrayTable,$arrayWhere){
    $this->__construct1($arrayTable);
    $errorWhere = $this->setWhere($arrayWhere);
    $this->setError($errorWhere);
  }

  // funzioni set

  function setTable($input){
    $errorSet="";
    if(!(is_array($input))){
      $errorSet = "Formato passaggio parametri non corretto";
      return $errorSet;
    }
    for ($i=0; $i < count($input)&& strlen($errorSet)==0 ; $i++) {
      if(!(ctype_alpha($input[$i])))
        $errorSet = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorSet)==0){
      $this->tableArray = $input;
    }
    return $errorSet;
  }

  function setWhere($input){
    $errorWhere="";
    if(!(is_array($input))){
      $errorWhere = "Formato passaggio parametri non corretto";
      return $errorWhere;
    }
    for ($i=0; $i < count($input)&& strlen($errorWhere)==0 ; $i++) {
      if(!preg_match("([\w(\s)*\w])",$input[$i]))
        $errorWhere = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorWhere)==0){
      $this->whereArray = $input;
    }
    return $errorWhere;
  }

  function addTable($input){
    $errorSet="";
    if(!(is_array($input))){
      $errorSet = "Formato passaggio parametri non corretto";
      return $errorSet;
    }
    for ($i=0; $i < count($input)&& strlen($errorSet)==0 ; $i++) {
      if(!(ctype_alpha($input[$i])))
        $errorSet = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorSet)==0){
      array_push($this->tableArray,$input);
    }
    else{
      $this->setError($errorSet);
    }
  }

  function addWhere($input){
    $errorWhere="";
    if(!(is_array($input))){
      $errorWhere = "Formato passaggio parametri non corretto";
      return $errorWhere;
    }
    for ($i=0; $i < count($input)&& strlen($errorWhere)==0 ; $i++) {
      if(!preg_match("([\w(\s)*\w])",$input[$i]))
        $errorWhere = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorWhere)==0){
      array_push($this->wherArray,$input);
    }
    else{
      $this->setError($errorWhere);
    }
  }

  function setError($error){
    if(strlen($error)>0)
      $this->error = $this->error.",".$error;
  }

  // funzioni get

  function getConnession(){
    return $this->connessione;
  }

  function getTable(){
    return $this->tableText;
  }

  function getWhere(){
    return $this->whereText;
  }

  // funzione connessione al database
  function connDatabase(){
    $this->connessione = new mysqli($this->host,$this->user,$this->password,$this->db);
      if($this->connessione->connect_errno){
          throw new Exception ("<p>Connessione fallita: ".$connessione->connect_error. ".</p>");
      }
    }

  // funzione per interrogazione al Database
  abstract function interrogation();

  function connectionClose(){
    $this->connessione->close();
  }

  // funzione concatenazione stringhe
  function concatenationText(){
    if(!empty($this->tableArray))
      $this->tableText = array_reduce($this->tableArray,function($a,$b){
          if($a)
              return $a.",".$b;
          else
              return $b;
      });
    if(!empty($this->whereArray))
      $this->whereText = array_reduce($this->whereArray,function($a,$b){
          if($a)
            return $a." ".$b;
          else
            return $b;
        });
  }

  // funzione __toString ridefinita
  function __toString(){
    return $this->error;
  }

}
 ?>
