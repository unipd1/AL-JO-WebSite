<?php
/**
 *
 */
require_once("Interrogation.php");

class DeleteInterrogation extends Interrogation {

  //-------------------------------campi dati-----------------------------------

  // tabella SQL Select
  private $table = "";
  private $interSQL = "";

  //-------------------------------funzioni-------------------------------------

  // costruttore

  function __construct($textTable,$arrayWhere){
    $errorSelect = $this->setTableText($textTable);
    parent::__construct(array($this->table),$arrayWhere);
    $this->setError($errorSelect);
    $this->concatenationText();
  }

  // funzioni set
  function setTableText($input){
    $errorText = "";
    if(!(ctype_alpha($input)))
      $errorText = "Errore Passaggio Parametri";
    else {
      $this->table=$input;
    }
    return $errorText;
  }

  // funzione per interrogazione al Database
  function interrogation(){
    try {
        $this->connDatabase();
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    $connessione = $this->getConnession();

    if(!$result = $connessione->query($this->interSQL)){
        throw new Exception("<p>Ops, c&#39;&egrave;  stato un errore, siamo spiacenti, la preghiamo di ripovare pi&ugrave; tardi.</p>", 1);

      }
      else {
        $this->connectionClose();
      }
  }

  // funzione concatenazione stringhe
  function concatenationText(){
    parent::concatenationText();
       $this->interSQL = "DELETE FROM ".$this->getTable()." WHERE ".$this->getWhere();
    }
}
?>
