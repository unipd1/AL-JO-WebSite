<?php
/**
 *
 */
require_once("Interrogation.php");

class SelectInterrogation extends Interrogation {

  //-------------------------------campi dati-----------------------------------

  // tabella SQL Select
  private $selectArray =[];
  private $selectText = "";
  private $interSQL = "";

  //-------------------------------funzioni-------------------------------------

  // costruttore

  function __construct($arraySelect,$arrayTable,$arrayWhere){
    parent::__construct($arrayTable,$arrayWhere);
    $errorSelect = $this->setSelect($arraySelect);
    $this->setError($errorSelect);
    $this->concatenationText();
  }

  // funzioni set
  function setSelect($input){
    $errorSelect="";
    if(!(is_array($input))){
      $errorSelect = "Formato passaggio parametri non corretto";
      return $errorSelect;
    }
    for ($i=0; $i < count($input)&& strlen($errorSelect)==0 ; $i++) {
        if(!preg_match("([(\w)\.(\w)]|\[w])",$input[$i]))
          $errorSelect = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorSelect)==0){
      $this->selectArray = $input;
    }
    return $errorSelect;
  }

  function addSelect($input){
    $errorSelect="";
    if(!(is_array($input))){
      $errorSelect = "Formato passaggio parametri non corretto";
      return $errorSelect;
    }
    for ($i=0; $i < count($input)&& strlen($errorSet)==0 ; $i++) {
      if(!(ctype_alpha($input[$i])))
        $errorSelect = "Formato passaggio parametri non corretto";
    }
    if(strlen($errorSelect)==0){
      array_push($this->selectArray,$input);
    }
    else{
      $this->setError($errorSelect);
    }
  }



  // funzione per interrogazione al Database
  function interrogation(){
    try {
        $this->connDatabase();
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    $connessione = $this->getConnession();

    if(!$result = $connessione->query($this->interSQL)){
        throw new Exception("<p>Ops, c&#39;&egrave;  stato un errore, siamo spiacenti, la preghiamo di ripovare pi&ugrave; tardi.</p>", 1);
      }
      else {
        $this->connectionClose();
        return $result;
      }

  }

  // funzione concatenazione stringhe
  function concatenationText(){
    parent::concatenationText();
    if(empty($this->selectArray)){
    }else {
      $this->selectText = array_reduce($this->selectArray,function($a,$b){
          if($a)
              return $a.",".$b;
          else
              return $b;
      });
        $this->interSQL = "SELECT ".$this->selectText." FROM ".$this->getTable()." WHERE ".$this->getWhere();
    }
    }

}

 ?>
