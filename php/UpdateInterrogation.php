<?php
/**
 *
 */
require_once("Interrogation.php");

class UpdateInterrogation extends Interrogation {

  //-------------------------------campi dati-----------------------------------

  // tabella SQL Select
  private $setArray =[];
  private $setText = "";
  private $interSQL = "";

  //-------------------------------funzioni-------------------------------------

  // costruttore

  function __construct($arrayTable,$arraySet,$arrayWhere){
    parent::__construct($arrayTable,$arrayWhere);
    $errorSet = $this->setSetting($arraySet);
    $this->setError($errorSet);
    $this->concatenationText();
  }

  // funzioni set
  function setSetting($input){
    $erroreSetting="";
    if(!(is_array($input))){
      $erroreSetting = "Formato passaggio parametri non corretto";
      return $erroreSetting;
    }
    for ($i=0; $i < count($input)&& strlen($erroreSetting)==0 ; $i++) {
      if(!preg_match("[((\s*)\w(\s*))=[(\s*)'(\w)'(\s*)]]",$input[$i]))
        $erroreSetting = "Formato passaggio parametri non corretto";
    }
    if(strlen($erroreSetting)==0){
      $this->setArray = $input;
    }
    return $erroreSetting;
  }

  // funzione per interrogazione al Database
  function interrogation(){
    try {
        $this->connDatabase();
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    $connessione = $this->getConnession();

    if(!$result = $connessione->query($this->interSQL)){
        /*echo"Errore della query: ".$connessione->error.".";
        exit();*/
        throw new Exception("<p>Ops, c&#39;&egrave;  stato un errore, siamo spiacenti, la preghiamo di ripovare pi&ugrave; tardi.</p>", 1);

      }
      else {
        $this->connectionClose();
      }
  }

  // funzione concatenazione stringhe
  function concatenationText(){
    parent::concatenationText();
    if(!empty($this->setArray))
      $this->setText = array_reduce($this->setArray,function($a,$b){
          if($a)
              return $a.",".$b;
          else
              return $b;
      });
        $this->interSQL = "UPDATE ".$this->getTable()." SET ".$this->setText." WHERE ".$this->getWhere();
    }

}

 ?>
